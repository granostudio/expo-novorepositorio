'use strict';

/**
 * @ngdoc function
 * @name ingresseFreepassPickerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ingresseFreepassPickerApp
 */

angular.module('ingresseFreepassPickerApp')
  .controller('MainCtrl', function ($scope, ingresseAPI, IngresseApiEventService, calendarConfig, ipCookie, freepassService) {
    $scope.user = {};
    $scope.loading = true;

    $scope.userID = 443918;
    $scope.usertoken = '443918-c55f3a2514a6a039cbdb9893032f61e972035fe6';

    $scope.eventsToLoad = [];
    $scope.ticketTypesLoaded = [];
    $scope.history = ipCookie('limit');
    $scope.errorMessage = false;

    if (!$scope.history) {
        $scope.history = {};
    }

    calendarConfig.templates.calendarMonthCell = 'views/customMonthCell.html';
    $scope.freepassMail = '';
    $scope.hasError = false;

    $scope.validateEmail = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    $scope.scrollTo =  function(elm){
        $('html, body').animate({
            scrollTop: $(elm).offset().top
        })
    }

    $scope.modalOpen = function(ticket) {
        $scope.selectedTicket = ticket;
        $scope.email = [];
        $scope.cpf = null;

        $('#overlay').fadeIn(function(){
            $('.modal').fadeIn();
        });
    };

    $scope.modalClose = function(){
        $('.modal').fadeOut(function(){
            $('#overlay').fadeOut('fast');
        });

        $scope.email = null;
        $scope.cpf = null;
        $scope.selectedTicket = null;
        $scope.emailSent = false;
    };


    $scope.otherTicket = function(){
        $scope.emailSent = false;
        $scope.email = null;
    }

    $scope.vm = {
        events:  [],
        calendarView:  'month',
        viewChangeClicked:  function(nextView ){
            if(nextView  !== 'month') {
                return false;
            }
        }

    };

    $scope.loadUserEvents = function(page) {
        var filter = {
            usertoken: $scope.usertoken,
            pageSize: 100
        };

        if (page) {
            filter.page = page;
        }

        ingresseAPI.user.getEvents($scope.userID, filter)
            .then(function (response) {

                $scope.eventsToLoad = response.data;

                $scope.title = $scope.eventsToLoad[0].title;
                $scope.description = $scope.eventsToLoad[0].description;
                $scope.venue = $scope.eventsToLoad[0].venue;
                $scope.venue.mapUrl = encodeURI('http://maps.googleapis.com/maps/api/staticmap?center=&size=640x640&scale=2&zoom=13&markers=color:blue|' + $scope.venue.street + '-' + $scope.venue.city + ' - ' + $scope.venue.state);
                $scope.poster = $scope.eventsToLoad[0].poster;

                if (response.paginationInfo.currentPage < response.paginationInfo.lastPage) {
                    $scope.loadUserEvents(response.paginationInfo.currentPage + 1);
                } else {
                    $scope.loadTickets();
                }
            });
    };

    $scope.loadTickets = function() {
        var event = $scope.eventsToLoad.pop();

        ingresseAPI.event.getTicketTypes(event.id)
            .then(function(response) {
                for (var i = 0; i < response.length; i++) {
                    if (response[i].status === 'available') {
                        $scope.ticketTypesLoaded.push($scope.convert(event.id, response[i]));
                    }
                }
            })
            .catch(function (error) {
                console.warn(error);
            })
            .finally(function() {
                if ($scope.eventsToLoad.length > 0) {
                    $scope.loadTickets();
                } else {
                    if ($scope.ticketTypesLoaded.length === 0) {
                        $scope.loading = false;
                        $scope.errorMessage = true;
                        return;
                    }
                    $scope.vm.events = $scope.ticketTypesLoaded;
                    $scope.vm.viewDate = $scope.vm.events[0].startsAt;
                    $scope.loading = false;
                }
            });
            // $scope.vm.events = [{
            //     title: 'An event',
            //     type: 'warning',
            //     startsAt: new Date(2015, 1, 1),
            //     endsAt: new Date(2015, 1, 1),
            //     draggable: true,
            //     resizable: true
            // }];
            // $scope.vm.viewDate = new Date(2015, 1, 1);
    };

    $scope.convert = function(eventid, ticketType) {
        var calendarEvent = {
            eventId: eventid,
            ticketTypeId: ticketType.id,
            title: ticketType.name,
            type: 'info',
            startsAt: IngresseApiEventService._convertApiDateTimeToMoment(ticketType.validTo).toDate(),
            draggable: false,
            resizable: false
        };

        if (ticketType.type !== 'Inteira') {
            calendarEvent.title += ' ' + ticketType.type;
        }

        return calendarEvent;
    };

    $scope.sendFreepass = function(ticket, email, cpf) {
        if (!$scope.history[ticket.eventId])
            $scope.history[ticket.eventId] = {};

        if ($scope.history[ticket.eventId][cpf] > 4 || ($scope.history[ticket.eventId][$scope.cpf] + email.length) > 4 ) {
            $scope.limitExceded = true;
            $('.modal .modal-content').shake();
            return;
        }

        $scope.isSendingFreepass = true;
            
        if(!$scope.cpf
                || (!$scope.validateEmail(email[0]) || email[0] === undefined)
                || (!$scope.validateEmail(email[1]) && email[1] !== undefined)
                || (!$scope.validateEmail(email[2]) && email[2] !== undefined)
                || (!$scope.validateEmail(email[3]) && email[3] !== undefined)
            ){
            $('.modal .modal-content').shake();
            $scope.hasError = true;
            $scope.isSendingFreepass = false;
            return;
        }

        $scope.hasError = false;

        var filters = {
            verify: false
        };

        var postObject = {
            eventId: ticket.eventId,
            emails: email.join(),
            ticketTypeId: ticket.ticketTypeId
        };

        var usertoken = $scope.usertoken;

        ingresseAPI.freepass.send(filters, postObject, usertoken)
            .then(function(response) {
                $scope.emailSent = true;

                if (!$scope.history[ticket.eventId][$scope.cpf]) {
                    var objEmail = {};
                    objEmail[$scope.cpf] = 0;
                    $scope.history[ticket.eventId] = objEmail;
                }
                //console.log($scope.history[ticket.eventId], $scope.history[ticket.eventId][$scope.cpf])
                $scope.history[ticket.eventId][$scope.cpf] += email.length;
                $scope.limitExceded = false;
                ipCookie('limit', $scope.history, { expires: 21 });
            })
            .finally(function() {
                $scope.isSendingFreepass = false;
            });
    };

    $scope.init = function () {
        $scope.user = freepassService.getUser();
        
        if ($scope.user == null || $scope.user.freepass == false) {
            //window.location.href = 'http://localhost/freepass-control-panel';
        };
    };
    
    /**
     * On view content loaded
     */
    $scope.$on('$viewContentLoaded', function () {
        $scope.init();
        $scope.loadUserEvents();
    });
});

jQuery.fn.shake = function() {
    this.each(function() {
        $(this).css({ "position": "relative" });
        for (var x = 1; x <= 3; x++) {
            $(this).animate({ left: -25 }, 10).animate({ left: 0 }, 50).animate({ left: 25 }, 10).animate({ left: 0 }, 50);
        }
    });
    return this;
};
