'use strict';

/**
 * @ngdoc overview
 * @name ingresseFreepassPickerApp
 * @description
 * # ingresseFreepassPickerApp
 *
 * Main module of the application.
 */

// moment.locale('pt-br');

var app = angular
  .module('ingresseFreepassPickerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ingresseSDK',
    'mwl.calendar',
    'ipCookie',
    'ui.mask',
  ])
  .config(function ($routeProvider, ingresseApiPreferencesProvider, calendarConfig) {
    // Ingresse SDK
    ingresseApiPreferencesProvider.setHost('https://api.ingresse.com');

    ingresseApiPreferencesProvider.setPublicKey('172f24fd2a903fc0647b61d7112ee1b9814702be');
    ingresseApiPreferencesProvider.setPrivateKey('5883af339b287ec5235e79f48434247fa0c75633');


    calendarConfig.dateFormatter = 'moment'; // use moment to format dates

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/passCode/:passCode', {
        templateUrl: 'views/freepass.html',
        controller: 'FreepassCtrl'
      })
      .when('/calendar', {
        templateUrl: 'views/calendar.html',
        controller: 'calendarCtrl',
        controllerAs: 'calendar'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
