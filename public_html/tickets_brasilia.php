<div class="tickets-container wrapper tickets" id="ingressos">
	<div class="half-wrapper">
		<div class="row">
			<div class="col col-12">
				<h2 class="title"> Ingressos </h2>
			</div> <!-- .col .col-12 -->
		</div> <!-- .row -->

		<div class="row">
			<div class="col col-6 border-l">
				<p>
					<span class="bold subtitle">Inteira: </span> <span class="subtitle"> R$ 40,00 </span> <br/>
					<span class="bold subtitle">Meia: </span> <span class="subtitle"> R$ 20,00 </span> <br/>
					<small> (mediante apresentação de carteirinha) </small>
				</p>

				<!-- <p>
					Professores de Ensino Fundamental e Médio, de escolas públicas e particulares, têm entrada franca em todos os dias e horários, desde que comprovem atividade em 2016;
				</p> -->
				<p>
					<span class="bold"> Quem tem direito a meia entrada: </span> <br/>
					<!-- Aposentados,  Idosos,  Estudantes, Deficientes físicos (neste caso, os acompanhantes também tem direito a meia entrada); -->
					<ul class="info-list">
						<li>Estudantes regularmente matriculados nos níveis e modalidades de educação e ensino mediante comprovaçao através de documento com foto. São válidas apenas  Carteira de Identificação Estudantil (CIE), emitida por associações estudantis vinculadas à União Nacional dos Estudantes (UNE), à União Brasileira dos Estudantes Secundaristas (Ubes) e à Associação Nacional dos Pós-Graduandos (ANPG).</li>
						<li>Pessoas com deficiência, inclusive seu acompanhante quando necessário;</li>
						<li>Jovens de 15 a 29 anos de idade de baixa renda, inscritos no Cadastro Único para Programas Sociais do Governo Federal (CadÚnico);</li>
						<li>Aposentados (Apresentar documento de identidade oficial com foto e cartão de benefício do INSS que comprove a condição. (Lei Municipal SP nº 12.325/1997)</li>
						<li>Idosos, acima de 60 anos;</li>
						<li>
							Diretores, coordenadores pedagógicos, supervisores e titulares do quadro de apoio escolar municipal, estadual  e federal:
							<ul>
								<li>Vigilantes e Profissionais de Segurança . Para ter direito à meia-entrada, os vigilantes devem apresentar, “no momento do acesso ao evento ou ao local de sua realização, a Carteira Nacional do Vigilante (CNV), com prazo de validade em vigor”. A regra, semelhante à aplicada para estudantes, (Projeto Lei-2015-00735-RDI/GDF)</li>
							</ul>
						</li>
						<li>Obs.: A concessão do direito ao benefício da meia-entrada é assegurada em 40% (quarenta por cento) do total dos ingressos disponíveis para cada evento.</li>
					</ul>
				</p>
				<p>
					<span class="bold">Quem tem direito a gratuidade: </span> <br/>

					<ul class="info-list">
						<li>Crianças até 02 anos de idade; </li>
					</ul>
				</p>
				<br>
				<!-- <p>
					<span class="bold subtitle">ATENÇÃO</span>
					<ul class="info-list"> 
						<li>Compra de ingresso na Bilheteria: é necessária a comprovação do direito ao benefício da 1/2 entrada no ato da compra e no acesso ao evento.</li>
						<li>Compra de ingresso pela Internet: é necessária a comprovação do direito ao benefício da 1/2 entrada no acesso ao evento.</li>
						<li>Caso o benefício não seja comprovado, o portador deverá complementar o valor do ingresso adquirido para o valor do ingresso integral, caso contrário o acesso ao evento não será permitido.</li>
					</ul>
				</p> -->
			</div> <!-- .col .col-5 .text-center -->

			<div class="col col-6 border-l altura-box">
				<p class="precos">
					<span class="bold subtitle"> Horário: </span> <br/>
					<span class="subtitle">Segunda a Sábado, das 10h00 às 22h00;<br/>
					Domingos e Feriados das 11h00 às 23h00<br/><br/>
					</span>
				</p>

				<!-- <p class="precos">
					<span class="bold subtitle"> Vendas: </span> <br/>
					<strong>Entre  21 a 28 de março, vendas:<br/></strong>
					<ul class="info-list">
						<li>Internet e aplicativo para celular*;</li>
						<li>Ponto de venda extra, Iguatemi Brasilia, piso térreo;</li>
					</ul>
				</p> -->

				<p class="precos">
					<span class="bold">Horário das bilheterias no Iguatemi Brasilia:<br/></span>
					<ul class="info-list">
						<li>Segunda a Sábado, das 10h00 às 21h00; </li>
						<li>Domingos e Feriados, das 11h00 às 22h00</li>
					</ul>
					<p>Capacidade de público: limite máximo de 300 pessoas por hora</p>
				</p>

				<p class="precos">
					<span class="bold">Vendas:<br/></span>
					<ul class="info-list">
						<li>Internet e aplicativo para celular<span class="asterisco">*</span>;</li>
						<li>Bilheteria da exposição, Iguatemi Brasilia, 1º subsolo;</li>
						<li>Ponto de venda extra, Iguatemi Brasilia, piso térreo;</li>
					</ul>
				</p>

				<p class="precos">
					<span class="bold">Canais de vendas digitais:<span class="asterisco">*</span></span>
				</p>
				<ul class="info-list">
					<li>Site vendas: www.tudus.com</li>
					<li>Aplicativo para celular (sistemas IOS e Android): Tudus</li>
					<li>Site Brasil: expo-theartofthebrick.com.br </li>
					<li>Facebook: facebook/theartofthebrickbrasil</li>
					<li>Instagram: instagram/theartofthebrickbrasil</li>
				</ul>
				<p class="precos"><strong>*Sujeito a taxa de conveniência</strong></p>
				<p>
					Compra de ingresso pela Internet: é necessária a comprovação do direito ao benefício da 1/2 entrada no acesso ao evento. Caso o benefício não seja comprovado, o portador deverá complementar o valor do ingresso adquirido para o valor do ingresso integral, caso contrário o acesso ao evento não será permitido.
				</p>
				<p class="precos"><strong>Bilheteria da exposiçao, Iguatemi Brasilia;</strong></p>
				<p>Importante: Na compra de ingresso na Bilheteria: é necessária a comprovação do direito ao benefício da 1/2 entrada no ato da compra e no acesso ao evento.
				</p>
				<!-- <p>
					<span class="bold subtitle"> MHN - Museu Histórico Nacional </span> <br/>
					<span class="endereco">
						Praça Marechal Âncora, s/nº<br/>
					</span>
					<strong>Período: de 17 de novembro 2016 a 22 de janeiro de 2017</strong>
				</p>
				<div class="mapa">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.202426166454!2d-43.171701685079746!3d-22.90590334359806!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x997e50222e9573%3A0x54c54f452c889eb8!2sMuseu+Hist%C3%B3rico+Nacional!5e0!3m2!1spt-BR!2sbr!4v1478513501576" width="100%" height="225" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div> -->
				<p>
					 <strong>Ingressos estão sujeito a disponibilidades.</strong>
				</p>

			</div> <!-- .col .col-7 -->
		</div> <!-- .row -->

	</div> <!-- .half-wrapper -->
</div> <!-- .tickets-container .wrapper -->
