<!-- <div class="row">
	<div class="col col-12 text-center">
		<h2> A arte de criar com <span class="lego">lego®</span>, </h2>
		<p class="subtitle"> de NATHAN SAWAYA </p>
	</div> <!-- .col-12 .text-center --
</div> <!-- .row -->

<div class="divisao"></div> <!-- .divisao -->

<div class="row">
	<div class="col col-6">
		<img src="assets/images/img_01.jpg" alt="">
	</div> <!-- .col-6 -->

	<div class="col col-6">
		<h3> Bem-vindo à espetacular obra de Nathan Sawaya, feita inteiramente com peças <span class="lego">lego®</span>! </h3>

		<p> Você sonhou com isso e Nathan Sawaya transformou seu sonho em realidade. Uma mostra para todas as idades. Com um espaço de exposição de mais de 1.800 m² , A ARTE DE CRIAR COM <span class="lego">LEGO®</span> leva o público a uma imersão no mundo divertido e colorido, e por vezes desconcertante, de Nathan Sawaya. </p>

		<p> O artista, que só nesta exposição utilizou mais de um milhão de peças <span class="lego">Lego®</span>, é o criador de obras que instigam a mente e arrancam sorrisos de crianças e adultos. A mostra apresenta inúmeras criações espetaculares, como O Pensador, do escultor francês Auguste Rodin, o magnífico vitral da face norte da Catedral de Chartres, na França, e até um dinossauro de seis metros de comprimento, feito com 80.020 peças <span class="lego">Lego®</span>. </p>

		<p> Não espere mais para visitar uma das dez melhores exposições do mundo, de acordo com a CNN, e mergulhe nessa oportunidade única! No final do percurso, o visitante encontrará ainda uma área de lazer com milhares de peças <span class="lego">Lego®</span> e <i>video games</i> DC COMICS™ para a imaginação e a criatividade rolarem soltas! </p>
	</div> <!-- .col-6 -->
</div> <!-- .row -->

<div class="divisao"></div> <!-- .divisao -->