<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/novo/favicon.ico">

    <title> Art of The Brick - Exposição de Esculturas com LEGO® </title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

     <div class="row" style="max-width: 500px;margin: 5% auto;">
     	<img src="logo.53bc50fb.png" align="center" width="100%" />
     </div>
      <div class="row" style="max-width: 400px;margin: 0 auto;margin-top: 5%;">
        <form class="form-signin" name="userForm">



    <div class="alert alert-warning" style="display:none">

        <a href="#" class="close" data-dismiss="alert">&times;</a>

        <strong>Atenção!</strong> <span class="messageToShow"></span>

    </div>



          <div class="col-md-12">
            <h2 class="form-signin-heading"> Cortesia </h2>
          </div>
          <p> &nbsp; </p>
          <div class="col-md-12">
            <label for="inputEmail" class="sr-only">CPF</label>
            <input type="text" id="inputEmail" class="form-control document" placeholder="CPF (apenas números)" name="document" required autofocus>
          </div>
          <p> &nbsp; </p>
          <?php /*
          <div class="col-md-12">
            <label for="inputPassword" class="sr-only">Código de Acesso</label>
            <input type="text" id="inputPassword" class="form-control passcode" name="passcode" placeholder="Código de Acesso" required>
          </div>
          <p> &nbsp; </p>
          */ ?>
          <div class="col-md-12">
            <button class="btn btn-lg btn-primary btn-block check" type="submit"> Enviar </button>
          </div>
        </form>
       
      </div>

      <div class="row" style="max-width: 432px;margin: 5% auto;">
     	<img src="yellow.jpg" align="center" width="100%" />
     </div>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript">
        ;(function($, window){

            jQuery(function($) {

                var Freepass = {};

                Freepass.init = function() {

                    $('.check').on('click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();

                        var userDocument = $('input.document').val();
                        //var userPasscode = $('input.passcode').val();

                        //if (userDocument.length <= 0 || userPasscode.length <= 0) {
                        if (userDocument.length <= 0) {
                            return false;
                        };
                        //data: { "document": userDocument, "passcode": userPasscode }
                        $.ajax({
                            method: "POST",
                            url: "http://expo-theartofthebrick.com.br/freepass-control-panel/api/users/check.json",
                            data: { "document": userDocument}
                        })
                        .done(function( msg ) {
                            if (msg.result.result == true) {

                                if (msg.result.user.freepass == true) {
                                    window.location.href = 'http://expo-theartofthebrick.com.br/adquirir-cortesia/#/passCode/' + userDocument;
                                };
                               
                                if (msg.result.user.passkey == true) {
                                    window.location.href = 'http://expo-theartofthebrick.com.br/comprar-ingressos';
                                };
                            };

                            if (msg.result.result == false) {
                                $('.messageToShow').text('CPF não encontrado!');
                                $('.alert-warning').show('fast');
                            };
                        });
                    });

                };

                Freepass.init();
            });

        })(jQuery, this);
    </script>
  </body>
</html>