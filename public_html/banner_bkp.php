<div id="banner">
	<img src="assets/images/banner.jpg" alt="" />
	<div class="bt-compre-ingresso">
		<a href="http://expo-theartofthebrick.com.br/comprar-ingressos/" class="btn btn-ingressos">
			<img src="assets/images/bt-comprar.png" alt="Comprar Ingresso" class="comprar" />
		</a>
	</div> <!-- .btn-compre-ingressos -->
</div> <!-- #banner -->

<div id="banner-mobile">
	<img src="assets/images/banner-mobile.png" alt="" />
	<div class="bt-compre-ingresso">
		<a href="http://www.eventim.com.br/ingressos.html?affiliate=GG1&doc=artistPages%2Ftickets&fun=artist&action=tickets&erid=1973259&includeOnlybookable=false&x10=1&x11=the" target="_blank">
			<img src="assets/images/bt-comprar.png" alt="Comprar Ingresso" class="comprar" />
			<!-- <img src="assets/images/bt-prevenda.png" alt=""> -->
		</a>
	</div>
</div>
