<div class="row partners">
	<div class="half-wrapper container">
		<div class="col col-4 apresentado">
			<h3> Apresentado por: </h3>

			<ul>
				<li class="col col-6">
					<div class="table">
						<div class="v-align middle">
							<a href="http://www.cultura.gov.br/" target="_blank"><img src="assets/images/partners/lei_de_incentivo_a_cultura.png" alt="Ministério da Cultura" /></a>
						</div>
					</div>					
				</li>

				<li class="col col-6">
					<div class="table">
						<div class="v-align middle">
							<a href="http://www2.brasilprev.com.br/Paginas/Default.aspx" target="_blank"><img src="assets/images/partners/brasilprev.png" alt="Brasilprev" class="brasilprev" /></a>
						</div>
					</div>
				</li>
			</ul>
		</div> <!-- .col .col-4 -->

		<div class="col col-8 apoio">
			<h3>Apoio:</h3>

			<ul>
				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="https://www.pontosmultiplus.com.br/home/" target="_blank"><img src="assets/images/partners/multiplus.png" alt="Multiplus"/></a>
						</div>
					</div>
				</li>

				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://www.otima.com/Site/php/home.php" target="_blank"><img src="assets/images/partners/otima.png" alt="Otima" /></a>
						</div>
					</div>
				</li>


				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="#" target="_blank"><img src="assets/images/partners/rio_logoparceiro.png" alt="" /></a>
						</div>
					</div>
				</li>

	
				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://www.museuhistoriconacional.com.br/" target="_blank"><img src="assets/images/partners/rio_mhn.png" alt="Museu Histórico Nacional"  style="width:90px;" /></a>
						</div>
					</div>
				</li>


	
				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="#" target="_blank"><img src="assets/images/partners/rio_sbm.png" alt="Sistema Brasileiro de Museus" /></a>
						</div>
					</div>
				</li>

	
				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://www.museus.gov.br/" target="_blank"><img src="assets/images/partners/rio_ibram.png" alt="Instituto Brasileiro de Museus" /></a>
						</div>
					</div>
				</li>



	

			</ul>			
		</div> <!-- .col .col-8 -->

		<div class="col col-5 media">
			<h3>Media Partner:</h3>

<style>
	.partners .media ul li {
		width:calc(27.3333% - 35px);
	}
</style>

			<ul>
				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://www.destakjornal.com.br/" target="_blank"><img src="assets/images/partners/destak.png" alt="Destak" /></a>
						</div>
					</div>
				</li>

				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://www.mixfm.com.br/" target="_blank"><img style="width:88px" src="assets/images/partners/rio_mix.png" alt="Radio Mix" /></a>
						</div>
					</div>
				</li>

				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://vejario.abril.com.br/" target="_blank"><img src="assets/images/partners/rio_veja.png" alt="Veja Rio de Janeiro" /></a>
						</div>
					</div>
				</li>
                
                
                
				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://sulamericaparadiso.uol.com.br/" target="_blank"><img src="assets/images/partners/rio_sulamerica.png" alt="Rádio Sulamerica Paradiso" /></a>
						</div>
					</div>
				</li>
                
                
                
                
                
                
                
                
			</ul>
		</div> <!-- .col .col-6 -->

		<div class="col col-3 fornecedor">
			<h3>Fornecedor Oficial:</h3>

			<ul>
				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://www.qualicorp.com.br/" target="_blank"><img src="assets/images/partners/rio_qualicorp.png" alt="Qualicorp" /></a>
						</div>
					</div>
				</li>
			</ul>
		</div> <!-- .col .col-3 -->		

		<div class="col col-4 producao">
			<h3>Produção:</h3>

			<ul>
				<li>
					<div class="table">
						<div class="v-align middle">
							<img src="assets/images/partners/knight.png" alt="Knight" />
						</div>
					</div>
				</li>

				<li>
					<div class="table">
						<div class="v-align middle">
							<img src="assets/images/partners/terminal2.png" alt="Terminal2" />
						</div>
					</div>
				</li>
			</ul>			
		</div> <!-- .col .col-4 -->

		<div class="col col-5 realizacao">
			<h3>Realização:</h3>

			<ul>
				<li>
					<div class="table">
						<div class="v-align middle">
							<img src="assets/images/partners/ring.png" alt="Ring"  />
						</div>
					</div>
				</li>

				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="http://www.cultura.gov.br/" target="_blank"><img src="assets/images/partners/ministerio_da_cultura_3.png" alt="Ministério da Cultura" /></a>
						</div>
					</div>
				</li>
			</ul>
		</div> <!-- .col .col-5 -->

		<div class="col col-3 venda">
			<h3>Venda:</h3>

			<ul>
				<li>
					<div class="table">
						<div class="v-align middle">
							<a href="https://www.ingresse.com/" target="_blank"><img src="assets/images/partners/ingresse.png" alt="Ingresse" class="ingresse" /></a>
						</div>
					</div>
				</li>
			</ul>			
		</div> <!-- .col .col-2 -->
	</div> <!-- .half-wrapper -->
</div> <!-- .row partners --> 