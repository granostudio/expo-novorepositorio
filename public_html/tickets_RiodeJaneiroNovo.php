<div class="tickets-container wrapper tickets" id="ingressos">
	<div class="half-wrapper">
		<div class="row">
			<div class="col col-12">
				<h2 class="title"> Ingressos </h2>
			</div> <!-- .col .col-12 -->
		</div> <!-- .row -->

		<div class="row">
			<div class="col col-6 border-l">
				<p>
					<span class="bold subtitle">Inteira: </span> <span class="subtitle"> R$ 20,00 </span> <br/>
					<span class="bold subtitle">Meia: </span> <span class="subtitle"> R$ 10,00 </span> <br/>
            <small style="margin-bottom:0px;"> (mediante apresentação de carteirinha) </small>
          <span class="bold subtitle" style="margin-bottom:30px;">Início das vendas:</span> <span class="subtitle">27 de maio</span> <br/>
				</p>

				<!-- <p>
					Professores de Ensino Fundamental e Médio, de escolas públicas e particulares, têm entrada franca em todos os dias e horários, desde que comprovem atividade em 2016;
				</p> -->
				<p>
					<span class="bold"> Quem tem direito a meia entrada: </span> <br/>
					<!-- Aposentados,  Idosos,  Estudantes, Deficientes físicos (neste caso, os acompanhantes também tem direito a meia entrada); -->
					<ul class="info-list">
            <li>Idosos com idade superior a 60 (sessenta) anos têm direito a meia-entrada. Para comprovação, basta apresentar o documento de identidade;</li>
            <li>Pessoas com necessidades especiais e um acompanhante, tem direito a meia-entrada. O documento exigido no local de realização do evento para pessoas com necessidades especiais, será: <br>
               a) O cartão de Benefício de Prestação Continuada da Assistência Social da pessoa com deficiência, ou;<br>
               b) Documento emitido pelo Instituto Nacional do Seguro Social – INSS que ateste a aposentadoria de acordo com os critérios estabelecidos na Lei Complementar nº 142, de 8 de maio de 2013; <br>
               c) O documento do beneficiado, sempre deverá ser acompanhado do documento de identificação com foto  expedido por órgão público e válido em todo o território nacional.<br>
              Acompanhante: também tem direito ao benefício da meia-entrada (somente um acompanhante por pessoa com necessidade especial);</li>
            <li>Jovens com com idade entre 15 e 29 anos que pertencem à famílias de baixa renda (até dois salários mínimos/mês), inscritas no Cadastro Único para Programas Sociais do Governo Federal – CadÚnico;</li>
						<!-- <li>Estudantes regularmente matriculados nos níveis e modalidades de educação e ensino mediante comprovaçao através de documento com foto. São válidas apenas  Carteira de Identificação Estudantil (CIE), emitida por associações estudantis vinculadas à União Nacional dos Estudantes (UNE), à União Brasileira dos Estudantes Secundaristas (Ubes) e à Associação Nacional dos Pós-Graduandos (ANPG).</li> -->
            <li>Estudantes de escolas públicas ou privadas de todos os níveis, mediante apresentação de carteira de identidade com data de validade até o dia 31 de março do ano subsequente ao de sua expedição;</li>
            <li>Menores de 21 anos mediante apresentação de documento válido com foto;</li>
            <li>Professor da rede pública estadual e municipal e de escolas particulares  (apresentar documento de identidade oficial com foto e carteira funcional da Secretaria de Educação ou Holerite que comprove a condição.).</li>
            <!-- <li>Aposentados (Apresentar documento de identidade oficial com foto e cartão de benefício do INSS que comprove a condição. (Lei Municipal SP nº 12.325/1997)</li> -->
						<!-- <li>
							Diretores, coordenadores pedagógicos, supervisores e titulares do quadro de apoio escolar municipal, estadual  e federal:
							<ul>
								<li>Vigilantes e Profissionais de Segurança . Para ter direito à meia-entrada, os vigilantes devem apresentar, “no momento do acesso ao evento ou ao local de sua realização, a Carteira Nacional do Vigilante (CNV), com prazo de validade em vigor”. A regra, semelhante à aplicada para estudantes, (Projeto Lei-2015-00735-RDI/GDF)</li>
							</ul>
						</li> -->
						<!-- <li>Obs.: A concessão do direito ao benefício da meia-entrada é assegurada em 40% (quarenta por cento) do total dos ingressos disponíveis para cada evento.</li> -->
					</ul>
				</p>
				<p>
					<span class="bold">Quem tem direito a gratuidade: </span> <br/>

					<ul class="info-list">
						<li>Crianças até 02 anos de idade; </li>
					</ul>
				</p>
				<br>
				<!-- <p>
					<span class="bold subtitle">ATENÇÃO</span>
					<ul class="info-list"> 
						<li>Compra de ingresso na Bilheteria: é necessária a comprovação do direito ao benefício da 1/2 entrada no ato da compra e no acesso ao evento.</li>
						<li>Compra de ingresso pela Internet: é necessária a comprovação do direito ao benefício da 1/2 entrada no acesso ao evento.</li>
						<li>Caso o benefício não seja comprovado, o portador deverá complementar o valor do ingresso adquirido para o valor do ingresso integral, caso contrário o acesso ao evento não será permitido.</li>
					</ul>
				</p> -->
			</div> <!-- .col .col-5 .text-center -->

			<div class="col col-6 border-l altura-box">
				<p class="precos">
					<span class="bold subtitle"> Horário: </span> <br/>
					<span class="subtitle">Terça a Sábado, das 13h00 às 21h30;<br/>
					Domingos até às 20h00.<br/><br/>
					</span>
				</p>

				<!-- <p class="precos">
					<span class="bold subtitle"> Vendas: </span> <br/>
					<strong>Entre  21 a 28 de março, vendas:<br/></strong>
					<ul class="info-list">
						<li>Internet e aplicativo para celular*;</li>
						<li>Ponto de venda extra, Iguatemi Brasilia, piso térreo;</li>
					</ul>
				</p> -->

				<!-- <p class="precos">
					<span class="bold">Horário das bilheterias no Iguatemi Brasilia:<br/></span>
					<ul class="info-list">
						<li>Segunda a Sábado, das 10h00 às 21h00; </li>
						<li>Domingos e Feriados, das 11h00 às 22h00</li>
					</ul>
					<p>Capacidade de público: limite máximo de 300 pessoas por hora</p>
				</p> -->

				<p class="precos">
					<span class="bold">Canais de vendas físicas:<br/></span>
					<ul class="info-list">
						<!-- <li>Internet e aplicativo para celular<span class="asterisco">*</span>;</li>
						<li>Bilheteria da exposição, Iguatemi Brasilia, 1º subsolo;</li>
						<li>Ponto de venda extra, Iguatemi Brasilia, piso térreo;</li> -->
            <li>TICKET CENTER  –  Praia do Leme Quiosque QL12,  Av. Atlântica – Leme, Rio de Janeiro -RJ, 22010;</li>
            <li>BILHETERIA DA EXPOSIÇÃO: a partir de 21 de junho, VillageMall – Avenida das  Américas, 3.900,Barra da Tijuca – Rio de janeiro. Piso SS1.</li>
					</ul>
				</p>

				<p class="precos">
					<span class="bold">Canais de vendas digitais (sujeito a taxa de conveniência):<span class="asterisco">*</span></span>
				</p>
				<ul class="info-list">
					<li>Site vendas: <a href="http://www.tudus.com.br/" target="_blank">www.tudus.com.br</a></li>
          <li>Site Brasil: <a href="http://www.expo-theartofthebrick.com.br/" target="_blank">www.expo-theartofthebrick.com.br</a></li>
          <li>Facebook: <a href="https://www.facebook.com/theartofthebrickbrasil/" target="_blank">theartofthebrickbrasil</a></li>
					<!-- <li>Aplicativo para celular (sistemas IOS e Android): Tudus</li> -->
					<!-- <li>Instagram: instagram/theartofthebrickbrasil</li> -->
				</ul>

        <p class="precos">
          <span class="bold">Mais informações sobre o artista e a exposição:</span>
          <ul class="info-list">
            <li>Nathan Sawaya (<a href="http://www.nathansawaya.com/" target="_blank">www.nathansawaya.com</a>)</li>
            <li>Website: <a href="http://www.brickartist.com/" target="_blank">www.brickartist.com</a></li>
            <li>Twitter: <a href="https://twitter.com/NathanSawaya" target="_blank">/NathanSawaya</a></li>
            <li>Site Brasil: <a href="http://www.expo-theartofthebrick.com.br/" target="_blank">expo-theartofthebrick.com.br</a></li>
            <li>Facebook: <a href="https://www.facebook.com/theartofthebrickbrasil/" target="_blank">theartofthebrickbrasil/</a></li>
            <li>Instagram: <a href="https://www.instagram.com/theartofthebrickbrasil/" target="_blank">theartofthebrickbrasil/</a></li>
          </ul>
        </p>
				<!-- <p class="precos"><strong>*Sujeito a taxa de conveniência</strong></p> -->
				<!-- <p>
					Compra de ingresso pela Internet: é necessária a comprovação do direito ao benefício da 1/2 entrada no acesso ao evento. Caso o benefício não seja comprovado, o portador deverá complementar o valor do ingresso adquirido para o valor do ingresso integral, caso contrário o acesso ao evento não será permitido.
				</p> -->
				<p class="precos"><strong>Bilheteria da exposiçao: a partir de 21 de junho, VillageMall – Avenida das  Américas, 3.900,Barra da Tijuca – Rio de janeiro. Piso SS1.</strong></p><br>
				<!-- <p>Importante: Na compra de ingresso na Bilheteria: é necessária a comprovação do direito ao benefício da 1/2 entrada no ato da compra e no acesso ao evento.
				</p> -->
				<p>
					<span class="bold subtitle"> Local </span> <br/><br>
					<span class="endereco">
						VillageMall - Av. das Américas, 3900 - Piso SS1 - Barra da Tijuca, Rio de Janeiro - RJ<br/>
					</span>
					<strong>Duração: De 21/06 até dia 06/08</strong>
				</p>
				<p>
					 <strong>Ingressos estão sujeito a disponibilidades.</strong>
				</p>

			</div> <!-- .col .col-7 -->
		</div> <!-- .row -->

	</div> <!-- .half-wrapper -->
</div> <!-- .tickets-container .wrapper -->
