'use strict';

var ingresseWidget = {
    host               : 'https://dk57nqppwurwj.cloudfront.net',
    developmentHost    : 'https://local.ingresse.com:9000',
    homologationHost   : 'https://di7sot6oypu2g.cloudfront.net',
    maxIterationsTimes : 100,
    iterationsExectuted: 0,
    show: function (event) {
        if (/comprar/.test(window.location.hash) === false) {
            window.history.pushState({ embedstore: true }, null, window.location.href + '#comprar');
        }

        document.body.className += ' blockScrolling';

        window.onpopstate = function() {
            ingresseWidget.cleaningEmbedStore();
        };

        ingresseWidget.buildComponentForButton(event.target);
    },
    parseDataAttributes: function (attrs) {
        var list  = [].slice.call(attrs),
            data  = {},
            regex = /data-/;

            for (var i = 0; i < list.length; i++) {
                var attr = list[i],
                    name = attr.name.replace(regex, '');

                if (regex.test(attr.name)) {
                    data[name] = attr.value;
                }
            }

        return data;
    },
    buildComponentForButton: function (widgetButton) {
        //<div class='cloak'></div>
        ingresseWidget.cloakElement = document.createElement('div');
        ingresseWidget.cloakElement.setAttribute('id','cloak');
        ingresseWidget.cloakElement.setAttribute('style','background: rgba(0,0,0,0.8);height: 100%;width: 100%;position: fixed;top: 0px;left: 0px;z-index: 999999999999999;');

        // Don't apply the scroll block on facebook.
        if (window.location.hostname.indexOf('facebook.com') === -1) {
            ingresseWidget.cloakElement.onwheel = function (event) {
                event.preventDefault();
            };
        }

        var container = widgetButton.parentElement,
                data  = ingresseWidget.parseDataAttributes(container.attributes),
                src;

        switch (data.host) {
            case 'dev':
                src = this.developmentHost;
                data.host = 'apihml';
                break;

            case 'apihml':
                src = this.homologationHost;
                break;

            default:
                src = this.host;
        };

        src += '/#/session/' + window.location.host + '/event/' + data.eventid;

        var keys = Object.keys(data);

        for (var i = 0; i < keys.length; i++) {
            var key = keys[i],
                val = data[key];

            if (val) {
                src = ingresseWidget._updateQueryString(key, val, src);
            }
        }

        // Enable cross-domain tracking for iFrames
        try {
            var tracker = window.ga.getByName('ingresse'),
                linker = new window.gaplugins.Linker(tracker);

            src = linker.decorate(src, true);
        } catch (e) {}

        if (!data.isRedirect) {
            //<iframe>
            var iframeElement = document.createElement('iframe');
            iframeElement.id = 'ingresse-widget-frame';

            iframeElement.src = src;

            iframeElement.setAttribute('style', 'width:100%; height:100%; border:0px; position:fixed; top:0px; left:0px; z-index:99999999999999999 !important;');
            ingresseWidget.cloakElement.appendChild(iframeElement);

            //<button class='button hide' onclick='hideWidget()'>Hide</button>
            var hideButtonElement = document.createElement('button');

            hideButtonElement.setAttribute('class','ingresse-widget-hide');
            hideButtonElement.innerText = 'X';
            hideButtonElement.onclick = ingresseWidget.hide;
            ingresseWidget.cloakElement.appendChild(hideButtonElement);

            document.body.appendChild(ingresseWidget.cloakElement);

        } else {
            window.location.href = src;
        }
    },
    _updateQueryString: function (key, value, url) {
       var separator = (url.indexOf('?') === -1) ? '?' : '&';

       url += (separator + key + '=' + value);

       return url;
    },
    hide: function () {
        ingresseWidget.cleaningEmbedStore();
    },
    init: function () {
        var elements = document.getElementsByClassName('ingresse-widget');

        if (elements.length === 0) {
            if(ingresseWidget.iterationsExectuted >= ingresseWidget.maxIterationsTimes){
                return;
            }

            ingresseWidget.iterationsExectuted++;
            setTimeout(ingresseWidget.init, 100);
            return;
        }

        for (var i = elements.length - 1; i >= 0; i--) {
            var containerElement = elements[i];
            var showButtonElement;

            var comprar = document.createTextNode('Comprar ingressos');

            if (containerElement.children.length > 0) {
                showButtonElement = elements[i].children[0];
                showButtonElement.onclick = ingresseWidget.show;

            } else {
                showButtonElement = document.createElement('button');
                showButtonElement.setAttribute('class','venus orange button');
                showButtonElement.id = 'venus';
                showButtonElement.onclick = ingresseWidget.show;
                showButtonElement.appendChild(comprar);

                if (window.location.hostname.indexOf('ingresse') === -1) {
                    var powered = document.createElement('span');

                    powered.setAttribute('class','powered');
                    powered.innerText = 'Powered by Ingresse.com';
                    showButtonElement.appendChild(powered);
                }

                containerElement.appendChild(showButtonElement);
            }
        }
    },
    cleaningEmbedStore: function() {
        window.onbeforeunload = null;
        document.body.className = document.body.className.replace('blockScrolling', '');
        document.body.removeChild(ingresseWidget.cloakElement);
    }
};


if (document.addEventListener) {
    document.addEventListener("DOMContentLoaded", function(event) {
      ingresseWidget.init();
    });

} else {
    ingresseWidget.init();
}
