<div class="row audioguia">
	<div class="col col-12">
		<h2 id="audioguia"> Aplicativo e Audioguia da Exposição </h2>
		<p class="subtitle"> 
			Faça o Download gratuito do aplicativo do audioguia da exposição.  <br/>
			Nele você pode encontrar diversas informações sobre as obras de Nathan Sawaya.
		</p>

		<img src="http://expo-theartofthebrick.com.br/assets/images/banner_audio_guia.png" usemap="#Map" border="0" />
        <map name="Map" id="Map">
          <area shape="rect" coords="1289,37,1543,135" href="https://play.google.com/store/apps/details?id=com.Sined.lego.app&amp;hl=pt_BR" target="_blank" alt="Google Play" />
          <area shape="rect" coords="1288,171,1537,256" href="https://itunes.apple.com/br/app/the-art-of-the-brick/id1141137497?mt=8
" target="_blank" alt="App Store" />
        </map>
	</div> <!-- .col-12 -->
</div> <!-- .row -->

<div class="row">
	<div class="col col-6">
		<img src="assets/images/img_06.jpg" alt="" />
	</div> <!-- .col-6 -->

	<div class="col col-6">
		<h3> Grasp, de Nathan Sawaya </h3>
		<p> “Muitas pessoas me perguntam qual é minha escultura favorita entre todas que já construí. E respondo sempre: ‘A próxima’. Como poderia escolher somente uma entre todas? São como filhos; dediquei coração e alma à construção de cada uma. Mas estou sempre focado na próxima escultura, é lá que está meu coração. Mesmo assim, há algumas esculturas que se destacam. A obra Yellow [Amarelo] tornou- se uma escultura icônica. Muitas pessoas sentem uma forte conexão com ela e fazem perguntas a respeito dela. A escultura Grasp também se sobressai. Mostra uma figura tentando se desprender de uma parede enquanto braços tentam segurá-la. É uma obra importante para mim, pois fala sobre minha história. Quando tentei libertar-me e dedicar minha vida à arte, muitas pessoas me disseram: ‘ Você está louco, está cometendo um grande erro, não pode fazer isso’. Tive de me afastar delas, e essa foi a inspiração para a criação de Grasp.” </p>
	</div> <!-- .col-6 -->
	</div> <!-- .row -->

	<div class="row">
		<div class="col col-6">
			<h3> The Dinosarium, de Nathan Sawaya </h3>
			<p> “Bem-vindo ao Dinosaurium. Como pode ver, este recinto abriga um dos meus melhores trabalhos: um esqueleto de Tiranossauro. Levei um verão inteiro para fazer esta escultura e utilizei mais de 80.000 peças LEGO®. Agora você deve estar se perguntando: ‘O que este dinossauro tem a ver com todas as outras obras que vimos hoje?’ Esta escultura é muitíssimo importante para mim. Sabe por que criei este dinossauro? Eu o criei para você! Queria criar algo especialmente para você, para todas as crianças que visitam minha exposição. Então foi o que fiz: passei todo o verão construindo a escultura desse animal gigante. Foi um processo interessante, pois a obra era tão grande e exigia tanto tempo que às vezes o processo de construção ficava um pouco monótono. Para construir meu dinossauro, comprei um pequeno boneco de Tiranossauro Rex e o coloquei sobre a mesa. Isso ajudou-me a conhecer todos os ossos e formas de um Tiranossauro. Foi o que tornou possível produzir uma versão bem maior. Está vendo os cabos pendurados sobre a estátua? Eles fixam o esqueleto ao teto, para que fique preso no lugar. O esqueleto é tão grande que é composto por 14 partes, que podem ser desmontadas separadamente. Nós as transportamos ao redor do mundo e as remontamos onde quer que passemos. O esqueleto inteiro, com todas as suas partes em tamanho natural, da cabeça à ponta da cauda, tem exatamente as mesmas dimensões de um filhote de Tiranossauro, uma ‘criança’ ... assim como você!” </p>
		</div> <!-- .col-6 -->

		<div class="col col-6">
			<img src="assets/images/img_07.jpg" alt="" />
		</div> <!-- .col-6 -->
	</div> <!-- .row -->
</div> <!-- .row -->

<div class="divisao"></div> <!-- .divisao -->