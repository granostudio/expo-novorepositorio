<?php echo "<?php \$this->extend('/Common/search_form'); ?>\n"; ?>

<?php echo "<?php echo \$this->BootstrapForm->create(false, array('type' => 'get', 'novalidate' => true)); ?>\n"; ?>
<?php 
echo "<?php\n";
$tempModel = new Model(array('name' => $modelClass, 'table' => Inflector::tableize($modelClass), 'ds' => 'default'));
$fieldsSchema = $tempModel->schema(true);
foreach ($fieldsSchema as $key => $field) :
	if (array_key_exists('comment', $field) && $field['comment'] != 'upload') {
		$comment = json_decode($field['comment'], true);
		if (!empty($comment) && array_key_exists('search', $comment)):
			echo "\techo \$this->BootstrapForm->input('{$key}', array('label' => {$modelClass}::\$labels['{$key}']));\n";
		endif;
	}
endforeach;
echo "?>\n";
?>
<div>
    <?php echo "<?php echo \$this->Form->button('Limpar', array('class' => 'btn btn-default pull-left btn-clear-form', 'type' => 'button')); ?>\n"; ?>
    <?php echo "<?php echo \$this->Form->submit('Procurar', array('class' => 'btn btn-primary pull-right')); ?>\n"; ?>
</div>

<?php echo "<?php echo \$this->Form->end(); ?>"; ?>