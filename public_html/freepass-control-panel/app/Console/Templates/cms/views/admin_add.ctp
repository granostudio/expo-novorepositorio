<div class="row">
    <div class="col-md-12">
		<?php 
		echo "<?php
        echo \$this->Breadcrumb->drop(Inflector::pluralize(\$model::\$label), array('action' => 'index'))
                ->drop('Novo')
                ->create();
        ?>";
		?>
    </div>
</div>

<?php echo "<?php echo \$this->element(sprintf('%s/form', Inflector::pluralize(\$model)));?>" ?>