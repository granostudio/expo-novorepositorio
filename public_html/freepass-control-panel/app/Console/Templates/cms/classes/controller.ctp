<?php
/**
 * Controller bake template file
 *
 * Allows templating of Controllers generated from bake.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.classes
 * @since         CakePHP(tm) v 1.3
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

echo "<?php\n";
echo "App::uses('{$plugin}AppController', '{$pluginPath}Controller');\n";
?>
/**
 * <?php echo $controllerName; ?> Controller
 *
<?php
if (!$isScaffold) {
	$defaultModel = Inflector::singularize($controllerName);
	echo " * @property {$defaultModel} \${$defaultModel}\n";
	if (!empty($components)) {
		foreach ($components as $component) {
			echo " * @property {$component}Component \${$component}\n";
		}
	}
}
?>
 * @author Plastic
 */
class <?php echo $controllerName; ?>Controller extends <?php echo $plugin; ?>AppController {

<?php if ($isScaffold): ?>
/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;

<?php else:

	if (count($helpers)):
		echo "/**\n * Helpers\n *\n * @var array\n */\n";
		echo "\tpublic \$helpers = array(";
		for ($i = 0, $len = count($helpers); $i < $len; $i++):
			if ($i != $len - 1):
				echo "'" . Inflector::camelize($helpers[$i]) . "', ";
			else:
				echo "'" . Inflector::camelize($helpers[$i]) . "'";
			endif;
		endfor;
		echo ");\n\n";
	endif;
	
	if (count($components)):
		echo "/**\n * Components\n *\n * @var array\n */\n";
		echo "\tpublic \$components = array(";
		for ($i = 0, $len = count($components); $i < $len; $i++):
			if ($components[$i] == 'Paginator') {
				continue;
			}
			if ($i != $len - 1):
				echo "'" . Inflector::camelize($components[$i]) . "', ";
			else:
				echo "'" . Inflector::camelize($components[$i]) . "'";
			endif;
		endfor;
		echo ");\n\n";
	endif;

	echo "\tpublic \$paginate = array(\n";
		echo "\t\t'limit' => 20,\n";
		echo "\t\t'paramType' => 'querystring',\n";
		echo "\t\t'order' => array(\n";
			echo "\t\t\t'{$currentModelName}.created' => 'DESC'\n";
		echo "\t\t)\n";
	echo "\t);\n";
	?>
	
	<?php 
	$fields = $modelObj->schema(true);
	echo "public \$grid = array();\n";
	?>
	
	/**
	 * Callback beforeFilter
	 *
	 * @return void
	 */
	public function beforeFilter() 
	{
		parent::beforeFilter();
		ClassRegistry::init($this->model);
		
		$this->grid = array(
<?php
$compact = array();
foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
	foreach ($modelObj->{$assoc} as $associationName => $relation):
		if (!empty($associationName)):
			$otherModelName = $this->_modelName($associationName);
			$otherPluralName = $this->_pluralName($associationName);
			$displayField = $modelObj->{$associationName}->displayField;
			$compact[$relation['foreignKey']] = "\t\t\tarray('field' => '{$otherModelName}.{$displayField}', 'label' => '{$otherModelName}'),\n";
		endif;
	endforeach;
endforeach;

foreach ($fields as $key => $field) {
	if (array_key_exists('comment', $field) && $field['comment'] != 'upload') {
		$comment = json_decode($field['comment'], true);
		if (!empty($comment) && array_key_exists('grid', $comment)):
			if (isset($compact[$key])) {
				echo $compact[$key];
			} else {
				echo "\t\t\tarray('field' => '{$currentModelName}.{$key}'),\n";
			}
		endif;
	}
	if (array_key_exists('comment', $field) && $field['comment'] == 'upload') {
		echo "\t\t\tarray('field' => '{$currentModelName}.{$key}', 'format' => array(
			'_formatImage' => array(
                'width' => 80,
                'height' => 80,
                'aspect' => true,
                'crop' => false
            )
		)),\n";
	}
}
?>
        );
	}
	
<?php
	if (!empty($actions)) {
		echo "\t" . trim($actions) . "\n";
	}

endif; ?>
}