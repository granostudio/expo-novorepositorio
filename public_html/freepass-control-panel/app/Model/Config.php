<?php
App::uses('AppModel', 'Model');

class Config extends AppModel
{
    /**
     * Nome do modelo em português, usado nas views
     * @var string
     */
    public static $label = "Configurações";
    public static $labels = array(
        "name" => "Nome",
        "label" => "Label",
        "value" => "Valor",
        "editable" => "Editável",
        "type" => "Tipo"
    );
    public static $param_labels = array(
        "name" => "Nome",
        "value" => "Valor"
    );
    public $validate = array(
        'name' => array(
            'rule' => array('minLength', '3'),
            'message' => 'Este campo deve conter no minimo 3 caracteres'
        ),
        'label' => array(
            'rule' => array('minLength', '3'),
            'message' => 'Este campo deve conter no minimo 3 caracteres'
        )
    );
    public static $types = array(
        1 => "Email",
        2 => "Obrigatório"
    );

    /**
     * Validação para cada tipo (self::$types) de configuração
     */
    public static $validations = array(
        1 => array(
            'mustNotEmpty' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'message' => 'O e-mail deve ser preenchido!',
                'last' => true
            ),
            'mustBeEmail' => array(
                'rule' => array('email'),
                'message' => 'E-mail inválido',
                'last' => true
            )
        ),
        2 => array(
            'rule'    => 'notEmpty',
            'message' => 'Este campo é obrigatório'
        ),
    );

    /**
     * beforeValidate
     * @param type $options
     */
    public function beforeValidate($options = array())
    {
        parent::beforeValidate($options);

        // Aplica validação se for o caso
        if (!empty($this->data["Config"]["id"])) {
            $validation = $this->_getValidation($this->data["Config"]["id"]);
            if ($validation) {
                $this->validate["value"] = $validation;
            }
        }
    }

    /**
     * Retorna a validação para uma determinada configuração
     * @param type $id
     * @return boolean
     */
    public function _getValidation($id)
    {
        $result = ClassRegistry::init("Config")->findById($this->data["Config"]["id"], array('type'));

        if (!empty($result["Config"]["type"])) {
            return self::$validations[$result["Config"]["type"]];
        }

        return false;
    }

    public function load($prefix = "Site")
    {
        try {
            $result = ClassRegistry::init("Config")->find('all', array('fields' => array("name", "value")));

            foreach ($result as $item) {
                Configure::write($prefix . "." . $item["Config"]["name"], $item["Config"]["value"]);
            }
        } catch (Exception $e) {

        }
    }
}
