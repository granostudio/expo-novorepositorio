<?php
App::uses('AppModel', 'Model');
/**
 * Bar Model
 * @author Plastic
 */
class Bar extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = array(
	);

	public static $labels = array(
		'id' => 'ID',
		'name' => 'Name',
	);
}
