<?php
ini_set('max_execution_time', 1200);
ini_set('max_input_time', 1200);

App::uses('AppModel', 'Model');

class Upload extends AppModel
{
	public $messages = array();
	public $displayField = 'name';

	public $actsAs = array(
		'Upload.Upload' => array(
			'file' => array(
				'maxSize' => 2048000,
				'rootDir' => ROOT,
				'path' => '{ROOT}{DS}uploads{DS}{model}{DS}{field}{DS}',
				'fields' => array(
					'dir' => 'id',
					'type' => '_type',
					'size' => '_size'
				),
				'deleteOnUpdate' => true
			)
		)
	);

	public $validate = array(
		'file' => array(
			'photoExt' => array(
				'rule' => array('isValidExtension', array('jpeg', 'jpg', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pps', 'JPG', 'JPEG', 'PNG'), false),
				'message' => 'Extensão inválida!'
			),
			'file' => array(
				'rule' => array('isBelowMaxSize', 2048000),
				'message' => 'Arquivo deve ser menor que 2MB'
			)
		)
	);

	public $virtualFields = array(
		'path' => 'CONCAT("/uploads/upload/file/", Upload.id, "/", Upload.file)',
	);

	public static $label = 'Upload';

	/**
	 * Multiple Upload files
	 *
	 * @param array $data
	 * @return array|mixed
	 */
	public function multipleUpload($data = null)
	{
		$this->Behaviors->detach('Logable');
		if (isset($data[$this->alias]['file']) && !empty($data[$this->alias]['file'])) {
			return $this->_beforeSaveFile($data);
		}
		return false;
	}

	/**
	 * Prepare file data
	 *
	 * @param array $data
	 * @return array
	 */
	public function _beforeSaveFile($data)
	{
		foreach ($data[$this->alias]['file'] as $file) :
			$save = array(
				'file' => $file,
				'model' => $data[$this->alias]['model'],
				'foreign_key' => $data[$this->alias]['foreign_key']
			);

			$this->_saveFile($save);
		endforeach;

		return $this->messages;
	}

	/**
	 * Save file data
	 *
	 * @param array $data
	 * @return void
	 */
	public function _saveFile($data)
	{
		$this->create();
		try {
			if ($this->save($data)):
				$this->messages['success'][] = array("file" => $data['file']['name']);
				return;
			endif;
		} catch (Exception $e) {
			$this->messages['errors'][] = array("file" => $data['file']['name'], "message" => "Erro ao salvar o arquivo");
			return;
		}

		$error = $this->validationErrors;
		if (isset($error['file']) && !empty($error['file'][0])):
			$this->messages['errors'][] = array("file" => $data['file']['name'], "message" => array_shift($error['file']));
			return;
		endif;

		$this->messages['errors'][] = array("file" => $data['file']['name'], "message" => "Erro ao salvar o arquivo");
	}
}
