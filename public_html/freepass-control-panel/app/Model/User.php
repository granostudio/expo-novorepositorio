<?php
App::uses('AppModel', 'Model');

/**
 * User Model
 * @author André Andrade
 */
class User extends AppModel
{
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'ruleNameNotEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Preencha o nome',
			)
		),
		'email' => array(
			'ruleEmailNotEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Preencha o e-mail',
			),
			'ruleEmailEmail' => array(
				'rule' => array('email'),
				'message' => 'E-mail inválido',
			),
		),
		'street' => array(
			'rule' => array('notEmpty'),
			'message' => 'Preencha o endereço'
		),
		'city' => array(
			'rule' => array('notEmpty'),
			'message' => 'Preencha a cidade'
		),
		'state' => array(
			'rule' => array('notEmpty'),
			'message' => 'Preencha o estado'
		),
		'document' => array(
			'rule' => array('notEmpty'),
			'message' => 'Preencha o documento'
		),
		'birthday' => array(
			'rule' => array('date', 'dmy'),
			'message' => 'Preencha a data de nascimento'
		),
		'password' => array(
			'mustNotEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'Informe uma senha.',
				'on' => 'create',
				'last' => true
			),
			'mustBeLonger' => array(
				'rule' => array('minLength', 6),
				'message' => 'Senha deve conter mais de 5 caracteres.',
				'last' => true
			),
			'mustMatch' => array(
				'rule' => array('verifies'),
				'message' => 'Senha inválida, confirme a sua senha novamente.'
			)
		)
	);

	public static $label = 'User';

	public static $labels = array(
		'id' => 'ID',
		'name' => 'Nome',
		'email' => 'E-mail',
		'street' => 'Endereço',
		'city' => 'Cidade',
		'state' => 'Estado',
		'document' => 'Documento',
		'gender' => 'Sexo',
		'birthday' => 'Data de nascimento',
		'password' => 'Senha',
		'c_password' => 'Confirmar senha',
		'passcode' => 'Código',
		'passkey' => 'Cupom',
		'freepass' => 'Cortesia',
		'ingresse_id' => 'IngresseID',
		'ingresse_token' => 'IngresseToken',
		'created' => 'Data de Criação',
		'modified' => 'Data de Modificação',
	);

	public function beforeValidate($options = array())
	{
		parent::beforeValidate($options);

		if (isset($this->data[$this->alias]['id']) && empty($this->data[$this->alias]['password'])) {
			unset($this->data[$this->alias]['password']);
		}
	}

	public function verifies()
	{
		if (!isset($this->data[$this->alias]['c_password'])) {
			return false;
		}

		return ($this->data[$this->alias]['password'] === $this->data[$this->alias]['c_password']);
	}

	public function beforeSave($opt = array())
	{
		if (isset($this->data[$this->alias]['password']) && !empty($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}

		return true;
	}

	public function afterSave($created, $options = array())
	{
		if ($created && empty($this->data[$this->alias]['invite_count'])) {
			$this->saveField('invite_count', Configure::read('Site.invite_count'));
		}
	}

	/**
	 * Send notification
	 *
	 * @param array $data
	 * @return void
	 */
	public function sendNotification($user)
	{
		$this->_queueEmail(array(
			'to' => array($user['User']['email']),
			'subject' => 'Você recebeu mais convites',
			'template' => 'notification',
			'layout' => 'notification',
			'data' => $user['User']
		));
	}
	
	/**
	 * Import user with document
	 * only number - substr(uniqid(mt_rand()), 0, 5)
	 *
	 * @param array $user
	 * @return void
	 */
	public function importDocument($user)
	{
		if (empty($user) || empty($user['document'])) {
			return false;
		}
		
		$user['document'] = str_replace('-', '', $user['document']);
		$user['document'] = str_replace('.', '', $user['document']);
		/*
		if ($this->findByDocument($user['document'])) {
			return false;
		}
		*/
		$newUser = array(
			'document' => $user['document'],
			'passcode' => 'artbrick'
			//'passcode' => substr(uniqid(md5(microtime())), 0, 5)
		);
		
		$newUser['freepass'] = true;
		/*
		$newUser['passkey'] = true;
		if ($user['type'] == 'cortesia') {
			$newUser['freepass'] = true;
			$newUser['passkey'] = false;
		}
		*/
		$this->create();
		try {
			$this->save($newUser, false);
		} catch (Exception $e) {
			return false;
		}
	}
}