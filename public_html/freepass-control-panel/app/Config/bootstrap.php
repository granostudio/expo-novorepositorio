<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

Cache::config('default', array('engine' => 'File'));

CakePlugin::load('Queue');
CakePlugin::load('Upload');
CakePlugin::load('Sluggable');
CakePlugin::load('Migrations');
CakePlugin::load('MktMigrate', array('bootstrap' => true, 'routes' => false));

Configure::write('MktMigrate.login', 'migrate');
Configure::write('MktMigrate.pass', 'migrate');

Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/* MKT Plugins */
CakePlugin::load('Newsletter', array('routes' => true));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
if (file_exists(APP . 'Config' . DS . 'database.php')) {
	CakePlugin::load('DatabaseLogger');
	CakeLog::config('default', array(
		'engine' => 'DatabaseLogger.DatabaseLog'
	));
}
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

Inflector::rules('plural', array(
    'irregular' => array(
        'operador' => 'operadores',
    )
));

Configure::write("Application.title", "The Art of the Brick - Control Panel");
Configure::write("MKTCMS.version", "1.0");


function str_encode_utf8($string) { 
	if (mb_detect_encoding($string, 'UTF-8', true) === FALSE) { 
		$string = utf8_encode($string);
	}
	return $string;
}

function str_decode_utf8($string) { 
	if (mb_detect_encoding($string, 'UTF-8', true) === TRUE) { 
		$string = utf8_decode($string); 
	}
	return $string;
}

function str_encode_mac($string) {
	return mb_convert_encoding( $string, 'Windows-1252', 'UTF-8');
}

function xlsBOF() {
    echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
    return;
}

function xlsEOF() {
    echo pack("ss", 0x0A, 0x00);
    return;
}

function xlsWriteNumber($Row, $Col, $Value) {
    echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
    echo pack("d", $Value);
    return;
}

function xlsWriteLabel($Row, $Col, $Value ) {
    $L = strlen($Value);
    echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
    echo $Value;
    return;
}

// Load composer autoload.
# require APP . '/Vendor/autoload.php'; // or ROOT . '/vendors/autoload.php';
// Remove and re-prepend CakePHP's autoloader as composer thinks it is the most important.
# spl_autoload_unregister(array('App', 'load'));
# spl_autoload_register(array('App', 'load'), true, true);