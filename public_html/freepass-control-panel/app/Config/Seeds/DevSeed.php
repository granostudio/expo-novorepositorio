<?php

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class DevSeed extends SeedShell
{

    public $uses = array('Operator', 'Bar', 'Foo');

    public function up()
    {
        $this->_seedOperators();
        $this->_seedBars();
        $this->_seedFoos(50);
        self::_consoleWriteLine("############## Seed Completed XD ################");
    }

    public function down()
    {
        self::_consoleWriteLine("Deleting...");

        $db = ConnectionManager::getDataSource('default');
        $database = $db->config["database"];

        $db->rawQuery("SET foreign_key_checks = 0");

        foreach ($db->query("show tables") as $tables) {
            $table = $tables["TABLE_NAMES"]["Tables_in_$database"];
            $db->rawQuery("truncate $database.$table");
        }

        $uploadFolder = new Folder(ROOT . '/uploads');
        if ($uploadFolder->delete()) {
            self::_consoleWriteLine("uploads excluídos [OK]");
        }

        $db->rawQuery("SET foreign_key_checks = 1");

        self::_consoleWriteLine("[OK]");
    }

    public function _seedOperators()
    {
        $model = "Operator";
        ClassRegistry::init($model);

        self::_consoleWriteLine(__FUNCTION__ . "...");

        $this->_create($model, array(
            'name' => 'MKT Virtual',
            'email' => 'mktvirtual@mktvirtual.com.br',
            'password' => '123456',
            'cpassword' => '123456',
            'active' => true,
            'type' => Operator::TYPE_ADMINISTRATOR
        ));

        $this->_create($model, array(
            'name' => 'Operador',
            'email' => 'operador@mktvirtual.com.br',
            'password' => '123456',
            'cpassword' => '123456',
            'active' => true,
            'type' => Operator::TYPE_OPERATOR
        ));

        self::_consoleWriteLine("[OK]");
    }

    public function _seedBars()
    {
        $model = "Bar";

        self::_consoleWriteLine(__FUNCTION__ . "...");

        $this->_create($model, array(
            'name' => 'Bar 1',
        ));

        $this->_create($model, array(
            'name' => 'Bar 2',
        ));

        $this->_create($model, array(
            'name' => 'Bar 3',
        ));

        self::_consoleWriteLine("[OK]");
    }

    public function _seedFoos($amount)
    {
        $model = "Foo";

        self::_consoleWriteLine(__FUNCTION__ . "...");

        for ($count = 1; $count <= $amount; $count++) {
            $this->_create($model, array(
                'name' => "Foo $count",
                'bar_id' => rand(1, 3),
                'other_bar_id' => rand(1, 3)
            ));
        }

        self::_consoleWriteLine("[OK]");
    }

    public function _create($model, $data)
    {
        $this->$model->create();
        if (!$this->$model->save($data)) {
            var_dump($this->$model->validationErrors);
            self::_consoleWriteLine("FATAL ERRORRRRRRRRRRRRRRRRRRRR");
            die();
        }
    }

    private static function _consoleWriteLine($value)
    {
        echo ($value . "\n");
    }
    
    public static function _generateText()
    {
        return "Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralh";
    }

}