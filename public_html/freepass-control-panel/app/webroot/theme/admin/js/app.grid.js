/**
 * Init grid events
 * @returns {undefined}
 */
App.initGrid = function()
{
    var URL_PREFIX = "";
    
    if(PLUGIN)
    {
        URL_PREFIX = "{0}admin/{1}/{2}/".format(URL_BASE,PLUGIN,CONTROLLER);
    }
    else
    {
        URL_PREFIX = "{0}admin/{1}/".format(URL_BASE,CONTROLLER);
    }
    
    var items_list_key = "{0}_selected_items".format(CONTROLLER);
    var fields_list_key = "{0}_selected_fields".format(CONTROLLER);
    
    $(".action-delete").click(function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        App.showDeleteCancelDlg("Atenção", "Você deseja realmente excluir este item?", function(result) {
            if (result === "delete")
            {
                document.location = url;
            }
        });
    });


    $(".action-delete-selection").click(function(e) {
        e.preventDefault();
        var url = $(this).attr("href");

        var data = {data: {key: items_list_key}};
        $.post("{0}list_count".format(URL_PREFIX), data, function(response) {

            if (response.count <= 0)
            {
                App.showOkDlg("Atenção", "<div class='alert alert-warning'>Não existe nenhum item selecionado</div>");
                return;
            }

            App.showDeleteCancelDlg("Atenção", "Você deseja realmente excluir o(s) <b>{0} item(s)</b> selecionado(s)?".format(response.count), function(result) {

                if (result === "delete")
                {
                    document.location = url;
                }

            });

        }, 'json');


    });

    $(".action-export").click(function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        App.showExportGridDlg("Atenção", function(result) {
            if (result === "export")
            {
                document.location = url;
            }
        });
    });

    $(".grid-item").click(function(e) {


        if ($(this).is(":checked"))
        {
            var data = {data: {key: items_list_key, value: $(this).attr("data-id")}};
            $.post("{0}list_push".format(URL_PREFIX), data, function(response)
            {
                if (!response.result)
                {
                    App.showDlg("Atenção", "Falha ao adicionar item");
                }

            }, 'json');
        }
        else
        {
            var data = {data: {key: items_list_key, value: $(this).attr("data-id")}};
            $.post("{0}list_pop".format(URL_PREFIX), data, function(response)
            {
                if (!response.result)
                {
                    App.showDlg("Atenção", "Falha ao remover item");
                }

            }, 'json');
        }

    });

    $(document).on("click", ".exportable .field", function() {

        if ($(this).is(":checked"))
        {
            var data = {data: {key: fields_list_key, value: $(this).attr("data-field")}};
            $.post("{0}list_push".format(URL_PREFIX), data, function(response)
            {
                if (!response.result)
                {
                    App.showDlg("Atenção", "Falha ao adicionar item");
                }

            }, 'json');
        }
        else
        {
            var data = {data: {key: fields_list_key, value: $(this).attr("data-field")}};
            $.post("{0}list_pop".format(URL_PREFIX), data, function(response)
            {
                if (!response.result)
                {
                    App.showDlg("Atenção", "Falha ao remover item");
                }

            }, 'json');
        }


    });

};
