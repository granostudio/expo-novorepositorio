App.sort = function() {
	$("tbody").sortable({
		placeholder: "ui-state-highlight",
		handle: ".glyphicon-move",
		stop: function( event, ui ) {
			var model = $(this).find('.glyphicon-move:first').attr('data-model');
			var ids = Array();
			$("tbody tr").each( function() {
				ids.push($(this).find("input").attr("data-id"));
			});
			$.post(URL_BASE + "admin/app/sort", {ids: ids, model: model}, function() {});
		}
	});
};

;(function($) {
	if ($('table .glyphicon-move').length) {
		App.sort();
	};
})(jQuery);