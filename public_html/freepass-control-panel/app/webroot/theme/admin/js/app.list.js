//##################################################################################################
//# Listas                                                                                         #      
//##################################################################################################

App.list = new Array();

App.listItemAdd = function(key, value) {

    this.listLoad(key);

    if (App.list[key] === undefined)
        App.list[key] = [];

    if (App.list[key].indexOf(value) === -1)
        App.list[key].push(value);

    this.listSave(key);
};

App.listItemRemove = function(key, value) {

    this.listLoad(key);

    if (App.list[key] === undefined)
        App.list[key] = [];

    var idx = App.list[key].indexOf(value);

    if (idx > -1)
        App.list[key].splice(idx, 1);

    this.listSave(key);

};

App.listItemExist = function(key, value) {

    this.listLoad(key);

    if (App.list[key].indexOf(value) > -1)
        return true;
    else
        return false;

};

App.listRead = function(key) {

    this.listLoad(key);

    if (App.list[key] === undefined)
        App.list[key] = [];

    return App.list[key];
};

App.listWrite = function(key, itens) {

    if (App.list[key] === undefined)
        App.list[key] = [];

    App.list[key] = itens;
    this.listSave(key);

};

App.listSave = function(key) {

    $.cookie('app_{0}'.format(key), JSON.stringify(App.list[key]), {path: '/'});
    return true;
};

App.listLoad = function(key) {

    var data = $.cookie('app_{0}'.format(key));
    if (data !== undefined)
        App.list[key] = JSON.parse(data);
    else
        App.list[key] = new Array();

    return true;

};

