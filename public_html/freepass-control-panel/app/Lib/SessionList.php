<?php
/**
 * Classe para manipular lista em sessão
 *
 */
class SessionList
{
    public static function push($key, $value)
    {
        if (!CakeSession::check("lists")) {
            CakeSession::write("lists", array());
        }

        $lists =  CakeSession::read("lists");

        if (isset($lists[$key]) && in_array($value, $lists[$key])) {
            return true;
        }

        $lists[$key][] = $value;

        CakeSession::write("lists", $lists);
    }

    public static function pop($key, $value)
    {
        if (!CakeSession::check("lists")) {
            CakeSession::write("lists", array());
        }

        $lists =  CakeSession::read("lists");

        if (!is_array($value)) {
            $value = array($value);
        }

        $lists[$key] = array_diff($lists[$key], $value);

        CakeSession::write("lists", $lists);
    }

    public static function readAll()
    {
        if (!CakeSession::check("lists")) {
            CakeSession::write("lists", array());
        }

        return CakeSession::read("lists");
    }

    public static function read($key)
    {
        if (!CakeSession::check("lists")) {
            CakeSession::write("lists", array());
        }

        $lists = CakeSession::read("lists");

        if (isset($lists[$key])) {
            return $lists[$key];
        } else {
            return array();
        }
    }

    public static function count($key)
    {
        if (!CakeSession::check("lists")) {
            CakeSession::write("lists", array());
        }

        $lists = CakeSession::read("lists");

        if (isset($lists[$key])) {
            return count($lists[$key]);
        } else {
            return 0;
        }
    }
}
