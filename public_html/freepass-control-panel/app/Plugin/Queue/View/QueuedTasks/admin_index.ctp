<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Breadcrumb->drop(Inflector::pluralize($model::$label), array("action" => "index"))
                ->create($this->Html->link("Executar",array("action"=>"run")));
        ?>
    </div>
</div>

<div class="row">

    <div class="col-md-3">

        <?php echo $this->element(sprintf("%s/search_form", Inflector::pluralize($model))) ?>

    </div>

    <div class="col-md-9">

        <?php
        $options = array(
            "actions" => array(
                "edit" => false,
                "view" => array(
                    "title" => "Visualizar",
                    "icon" => "glyphicon-search",
                    "url" => array("action" => "view")
                )
            )
        );

        echo $this->Grid->create(Inflector::pluralize($model::$label), $grid, $result, $options);
        ?>

    </div>

</div>
