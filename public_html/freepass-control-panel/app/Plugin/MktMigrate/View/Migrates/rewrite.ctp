<?php $filePresent = false; ?>
<div class="col-md-4">
	<?php if (file_exists(ROOT . DS . '.htaccess')): ?>
		<div class="bs-callout bs-callout-info">
			<h4>Atenção!</h4>
			<p>
				<?php echo "[.htaccess] presente"; ?>
				<?php $filePresent = true; ?>
			</p>
		</div>
	<?php else: ?>
		<div class="bs-callout bs-callout-warning">
			<h4>Atenção!</h4>
			<p><?php echo "O arquivo [.htaccess] não esta presente!"; ?></p>
		</div>
	<?php endif; ?>
	
	<?php if ($filePresent) : ?>
		<p>
			<div class="alert heading heading-mkt">
				<strong class="alert-heading">.htaccess</strong>
			</div>
		
			<textarea data-role="editor" name="revision_files" cols="50"><?php echo trim(file_get_contents(ROOT . DS . '.htaccess')) ?></textarea>
			<button class="saveFile">Save</button>
			<?php echo $this->Html->scriptBlock("
				jQuery( function() {
					jQuery(window).bind('load', function() {
						var myCodeMirror = CodeMirror.fromTextArea(document.getElementsByTagName('textarea')[0], {
							mode: 'text/x-mysql',
							tabMode: 'indent',
							matchBrackets: true,
							autoClearEmptyLines: true,
							lineNumbers: true,
							theme: 'default'
						});
						jQuery('.saveFile').click(function(){
							//myCodeMirror.save();
							var content = myCodeMirror.getValue();
							var path = jQuery('#hiddenFilePath').text();
							var response = confirm('Deseja realmente salvar este arquivo?');
							if(response) {
								$.ajax({
									type: 'POST',
									url: 'save',
									data: {c:content,p:path},
									dataType: 'text',
									success: function(){
										alert('Arquivo salvo');
									}
								});
							} else {
								alert('File not saved!');
							}
						});
					});
				});
			", array('inline' => false)); ?>
		
		</p>
	<?php endif; ?>
</div>