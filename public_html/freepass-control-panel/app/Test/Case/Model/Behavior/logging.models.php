<?php 
if (!defined('CAKEPHP_UNIT_TEST_EXECUTION')) {
	define('CAKEPHP_UNIT_TEST_EXECUTION', 1);
}

class Logging extends CakeTestModel
{
	public $actsAs = array('Logable');
}
