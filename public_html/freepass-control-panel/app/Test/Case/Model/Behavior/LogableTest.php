<?php
require_once('logging.models.php');

class LogableTest extends CakeTestCase {

	public $fixtures = array('app.logging');
	
	public function startTest() {
		$this->Log = ClassRegistry::init('Logging');
		//$this->TestLog->Behaviors->attach('Logable', array());
	}
	
	public function testInstaceOf()
	{
		$this->assertIsA($this->Log, 'Logging');
	}
	
	public function testFind() 
	{
		$log = $this->Log->find('first');
		$expected = '1';
		$this->assertEqual($log['Logging']['id'], $expected);
	}
	
	public function testBehavior()
	{
		$saveLog = $this->Log->save(array('who' => '[Testing]', 'action' => 1, 'model' => 'Logging', 'model_id' => 2));
		$this->assertInternalType('array', $saveLog);
		
		$this->Log->disable_log = true;
		$saveLogWithoutLogging = $this->Log->save(array('who' => '[Testing]', 'action' => 2, 'model' => 'Logging', 'model_id' => 3));
		$this->assertInternalType('array', $saveLog);
		
		$this->assertEqual($this->Log->find('count'), 2);
	}
	
	public function endTest()
	{
		Classregistry::flush();
		unset($this->TestLog);
	}
	
	public function testDeleteLog()
	{
		$this->Log->delete(1);
		$this->assertEqual($this->Log->find('count'), 0);
	}
	
	public function testEditLog()
	{
		$this->Log->id = 1;
		$this->Log->save(array('who' => '[Testing]', 'action' => 3, 'model' => 'Logging', 'model_id' => 1));
	}
}