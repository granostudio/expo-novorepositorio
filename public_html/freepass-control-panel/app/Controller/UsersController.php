<?php
App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @author André Andrade
 */
class UsersController extends AppController
{
	public $paginate = array(
		'limit' => 20,
		'paramType' => 'querystring',
		'order' => array(
			'User.created' => 'DESC'
		)
	);

	public $grid = array();

	/**
	 * Callback beforeFilter
	 *
	 * @return void
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		ClassRegistry::init($this->model);

		$this->grid = array(
			array('field' => 'User.name'),
			array('field' => 'User.email'),
			array('field' => 'User.document'),
			array('field' => 'User.created'),
		);

		$this->Auth->allow('add', 'api_add', 'ingresse', 'auth', 'api_check', 'api_verify', 'api_paid', 'api_decrease');
	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add($action = 'index')
	{
		parent::admin_add();
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id|int
	 * @return mixed
	 */
	public function admin_edit($id = null)
	{
		$model = $this->model;
		$this->$model->id = $id;

		if (!$this->$model->exists()) {
			$this->showError(array("controller" => "home"));
		}

		if ($this->request->is('get')) {
			$this->request->data = $this->$model->read();
			unset($this->request->data['User']['password']);
		} else {
			if ($this->$model->save($this->request->data)) {
				$this->showSuccess(array("action" => "index"));
			} else {
				$this->showError();
			}
		}
	}

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index()
	{
		$this->set('grid', $this->grid);
		$options = array();

		if (isset($this->request->query) && !empty($this->request->query['name'])) {
			$this->paginate['conditions'] = array(
				'OR' => array(
					'User.name LIKE ' => '%' . $this->request->query['name'] . '%',
					'User.email LIKE ' => '%' . $this->request->query['name'] . '%'
				)
			);
		}

		$this->paginate['order'] = SearchComponent::createOrder($this->request, array('User.id' => 'asc'));

		if (!isset($this->params->named["export"])) {
			$this->set('result', $this->paginate($this->model));
		} else {
			$this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
			$this->render("/Common/admin_export");
		}
	}

	/**
	 * Show invites by user
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function admin_show($id)
	{
		$this->set('grid', $this->grid);

		$options = array();

		$this->paginate['conditions'] = array('id' => $id);
		$this->paginate['order'] = SearchComponent::createOrder($this->request, array('User.id' => 'asc'));

		if (!isset($this->params->named["export"])) {
			$this->set('result', $this->paginate($this->model));
		} else {
			$this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
			$this->render("/Common/admin_export");
		}
	}

	/**
	 * Import users
	 *
	 * @return void
	 */
	public function admin_import()
	{
		App::import('Vendor', array('file' => 'excel_reader'));
		
		if (empty($this->data)) {
			return false;
		}
		
		$reader = new Spreadsheet_Excel_Reader;
		$reader->read($this->request->data['User']['file']['tmp_name']);
		
		for ($row = 1; $row <= $reader->sheets[0]['numRows']; $row++) {
			$user = array();
			for ($column = 1; $column <= $reader->sheets[0]['numCols']; $column++) {
				if (isset($reader->sheets[0]['cells'][$row][$column]) && $column == 1) {
					$user['document'] = $reader->sheets[0]['cells'][$row][$column];
				}
				if (isset($reader->sheets[0]['cells'][$row][$column]) && $column == 2) {
					$user['type'] = $reader->sheets[0]['cells'][$row][$column];
				}
			}
			$this->User->importDocument($user);
		}
		
		$this->showSuccess(array("action" => "index"));
	}

	/**
	 * Generate URL to authorize
	 *
	 * @return string
	 */
	public function _ingresseLoginUrl()
	{
		$publickey = '172f24fd2a903fc0647b61d7112ee1b9814702be';
		$privatekey = '5883af339b287ec5235e79f48434247fa0c75633';

		$timestamp = gmdate('Y-m-d\TH:i:s\Z');
		$computedSignature = base64_encode(hash_hmac('sha1', $publickey . $timestamp, $privatekey, true));

		$params = array(
			'publickey' => $publickey,
			'signature' => $computedSignature,
			'timestamp' => $timestamp,
			'returnurl' => Router::url('/users/ingresse', true)
		);

		return http_build_query($params);
	}

	public function login()
	{
		if ($this->Auth->user()) {
			return $this->redirect($this->Auth->redirect());
		}

		$ingresseAuthLogin = $this->_ingresseLoginUrl();

		$this->redirect('https://api.ingresse.com/authorize/?'. $ingresseAuthLogin, null, true);

		/*
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirect());
			} else {
				$this->showError(null, "Usuário e(ou) senha inválido");
			}
		}
		*/
	}

	public function logout()
	{
		$this->redirect($this->Auth->logout());
	}

	/**
	 * API /add
	 *
	 * @todo implement fieldList
	 * @return mixed
	 */
	public function api_add()
	{
		$data = $this->request->data;
		
		$fieldList = array('name', 'email', 'document', 'street', 'city', 'birthday', 'gender');
		
		if (!empty($data['birthday'])) {
			$data['birthday'] = preg_replace('/(\d{2})(\d{2})(\d{4})/', '$1/$2/$3', $data['birthday']);
		}
		
		if (!empty($data['ingresse_id'])) {
			
			$userByIngresse = $this->User->find('first', array('conditions' => array('ingresse_id' => $data['ingresse_id'])));
			
			if (!empty($userByIngresse)) {
				$this->User->id = $userByIngresse['User']['id'];
				$data['id'] = $userByIngresse['User']['id'];
			} else {
				$this->User->create();
			}
		}
		
		$this->User->set($data);
		
		if ($this->User->save($data)) {
			return $this->set(array(
				'response' => array('result' => true, 'user_id' => $this->User->id),
				'_serialize' => 'response'
			));
		}

		return $this->set(array(
			'response' => array('result' => false, 'validationErrors' => $this->User->validationErrors),
			'_serialize' => 'response'
		));
	}
	
	/**
	 * Auth postback ingresse
	 *
	 * @return void
	 */
	public function ingresse()
	{
		$this->autoRender = false;
		
		if (isset($this->params->query['token']) && !empty($this->params->query['token'])) {
			$this->redirect('/users/auth/' . $this->params->query['token'], null, true);
		}
	}

	/**
	 * Auth with ingresse token
	 *
	 * @param string token
	 * @return void
	 */
	public function auth($token)
	{
		$this->autoRender = false;
		$user = $this->User->find('first', array('conditions' => array('ingresse_token' => $token)));
		
		if (!$user) {
			return $this->showMessage('/', 'warning', 'Para convidar um amigo é necessário comprar um ingresso!');
		}
		
		unset($user['User']['password']);
		
		$invite = $this->User->Invite->find('first', array('conditions' => array('buy' => true, 'owner' => $user['User']['id'])));
		
		if (!$invite) {
			return $this->showMessage('/', 'warning', 'Você deve pagar o ingresso para ter acesso!');
		}
		
		$this->request->data = $user['User'];
		$this->Auth->login($this->request->data);
		
		return $this->redirect('/');
	}
	
	/**
	 * Check user with document and passcode
	 *
	 * @param string passCode
	 * @return mixed
	 */
	public function api_check()
	{
		//$user = $this->User->findByDocumentAndPasscode($this->request->data['document'], $this->request->data['passcode']);
		$user = $this->User->findByDocument($this->request->data['document']);

		$result = array('result' => false);
		
		if (!$user) {
			return $this->set(array('result' => $result, '_serialize' => array('result')));
		}
		
		$result = array(
			'result' => true,
			'user' => $user['User']
		);
		
		$this->set(array('result' => $result, '_serialize' => array('result')));
	}

	/**
	 * Verify user
	 *
	 * @param string passCode
	 * @return mixed
	 */
	public function api_verify()
	{
		$user = $this->User->find('first', array('conditions' => array(
			'document' => $this->request->data['document'],
			'freepass' => true,
			'paid' => false
		)));
		
		$result = array('result' => false);
		
		if (!$user) {
			return $this->set(array('result' => $result, '_serialize' => array('result')));
		}
		
		$result = array(
			'result' => true,
			'user' => $user['User']
		);
		
		$this->set(array('result' => $result, '_serialize' => array('result')));
	}

	/**
	 * change flag paid
	 *
	 * @param string passCode
	 * @return mixed
	 */
	public function api_paid()
	{
		$user = $this->User->findById($this->request->data['id']);
		
		$result = array('result' => false);
		
		if (!$user) {
			return $this->set(array('result' => $result, '_serialize' => array('result')));
		}
		
		// update user
		$this->User->id = $user['User']['id'];
		$this->User->saveField('paid', true, false);

		$result = array(
			'result' => true,
			'user' => $user['User']
		);
		
		$this->set(array('result' => $result, '_serialize' => array('result')));
	}

	/**
	 * change flag invite_count
	 *
	 * @param string passCode
	 * @return mixed
	 */
	public function api_decrease()
	{
		$user = $this->User->findById($this->request->data['id']);
		
		$result = array('result' => false);
		
		if (!$user) {
			return $this->set(array('result' => $result, '_serialize' => array('result')));
		}
		
		$inviteCount = $user['User']['invite_count'];
		$newInviteCount = $inviteCount - 1;
		$user['User']['invite_count'] = $newInviteCount;
		
		$this->User->id = $user['User']['id'];
		$this->User->saveField('invite_count', $newInviteCount, false);
		
		if ($newInviteCount == 0) {
			$this->User->saveField('paid', true, false);
		}

		$result = array(
			'result' => true,
			'user' => $user['User']
		);
		
		$this->set(array('result' => $result, '_serialize' => array('result')));
	}

	public function admin_export()
	{
		$this->autoRender = false;
		
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=usuarios.xls");
		header("Content-Transfer-Encoding: binary");
		
		xlsBOF();
		
		xlsWriteLabel(0, 0, "CPF");
		xlsWriteLabel(0, 1, "Cortesias");
		//xlsWriteLabel(0, 2, str_encode_mac('Informação'));
		
		$users = $this->User->find('all', array('conditions' => array('modified >=' => '2016-09-06 00:00:00')));
		
		$line = 1;
		
		foreach ($users as $key => $user):
			# $cortesias = ($user['User']['paid'] == true) ? 4 : 0;
			xlsWriteLabel($line, 0, $user['User']['document']);
			xlsWriteLabel($line, 1, 4 - $user['User']['invite_count']);
			++$line;
		endforeach;
		
		xlsEOF();
		exit();
	}
}
