<?php
App::uses('AppController', 'Controller');

class ErrorLogsController extends AppController
{
    public $paginate = array(
        'limit' => 2,
    );
    public $grid = array();

    public function beforeFilter()
    {
        parent::beforeFilter();

        ClassRegistry::init($this->model);

        $this->grid = array(
            array("field" => "ErrorLog.type") ,
            array("field" => "ErrorLog.message", "format" => function($value, $item) {
                return "<div style='max-width: 400px; word-wrap: break-word'>" . $value . "</div>";
            }),
            array("field" => "ErrorLog.ip","label" => "Informações", "format" => function($value, $item) {
                $info = array();
                $info[] = $item["ErrorLog"]["hostname"];
                $info[] = $item["ErrorLog"]["uri"];
                $info[] = $item["ErrorLog"]["refer"];

                return implode("<br>", $info);
            }),
            array("field" => "ErrorLog.created"),
        );
    }

    public function admin_index()
    {
        $options = array(
            "name" => array("operation" => "contain"),
        );

        $this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
        $this->paginate['order'] = SearchComponent::createOrder($this->request, array('ErrorLog.id' => 'desc'));

        $this->set('grid', $this->grid);

        if (!isset($this->params->named["export"])) {
            $this->set('result', $this->paginate($this->model));
        } else {
            unset($this->paginate["limit"]);
            $this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
            $this->render("/Common/admin_export");
        }
    }
}
