<div class="container alert-message">
	<div class="row">
    	<div class="col-md-12">
	        <div class="alert alert-success alert-dismissable flash">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <span class="glyphicon glyphicon-thumbs-up"></span> <?php echo $message; ?>
	        </div>
    	</div>
	</div>
</div>