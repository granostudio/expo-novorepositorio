<div class="container alert-message">
	<div class="row">
    	<div class="col-md-12">
	        <div class="alert alert-warning alert-dismissable flash">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <span class="glyphicon glyphicon-info-sign"></span> <?php echo $message; ?>
	        </div>
    	</div>
	</div>
</div>