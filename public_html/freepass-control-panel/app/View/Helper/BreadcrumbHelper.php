<?php
App::uses('AppHelper', 'View/Helper');

class BreadcrumbHelper extends AppHelper
{
    private $items = array();

    public $helpers = array("Html");

    public function drop($label, $url = null)
    {
        $this->items[] = array("label" => $label, "url" => $url);

        return $this;
    }

    public function create($right = null)
    {
        $li = "";
        $last = array_pop($this->items);

        foreach ($this->items as $item) {
            $li .= $this->Html->tag("li", $this->Html->link($item['label'], $item["url"]));
        }

        $li .= $this->Html->tag("li", $last['label']);

        if ($right) {
            $li .= $this->Html->tag("li", $right, array("class" => "pull-right"));
        }

        return $this->Html->tag("ul", $li, array("class" => "breadcrumb"));
    }
}
