<?php
App::uses('AppHelper', 'View/Helper');

/**
 * LanguageHelper for suffix lang
 *
 */
class LanguageHelper extends AppHelper
{
	/**
	 * Return field_suffix
	 *
	 * @param string $field
	 * @return string
	 */
	public static function get($field)
	{
		$lang = Configure::read('Config.language');
		if ($lang == 'por') {
			return $field;
		}

		return $field . '_' . $lang;
	}
}
