<?php
App::uses('AppHelper', 'View/Helper');

class GridHelper extends AppHelper
{
    public $helpers = array("Html", "Paginator", "Image");
    public $model = null;
    public $selected_fields = array();
    public $selected_items = array();

    public function __construct(View $View, $settings = array())
    {
        parent::__construct($View, $settings);

        $this->selected_fields = SessionList::read($this->params->controller . "_selected_fields");
        $this->selected_items = SessionList::read($this->params->controller . "_selected_items");
    }

    /**
     * Create the Grid
     *
     * @param type $title
     * @param type $cols
     * @param type $result
     * @param array $options
     * @return type
     */
    public function create($title, $cols = array(), $result = array(), $options = array())
    {
        $default = array(
            'checkbox' => true,
            'header_buttons' => array(
                'export' => true,
                'delete' => true
            )
        );

        $options = array_merge($default, $options);

        $this->options = $options;

        $this->model = Inflector::classify($this->request->params["controller"]);

        if (isset($this->options['model'])) {
            $this->model = Inflector::classify($this->options['model']);
        }

        $thead = $this->_createTableHeader($cols);

        $rows = "";

        foreach ($result as $item) {

            $tds = "";

            $tds .= $this->Html->tag("td", $this->_createItemActions($item), array("class" => "actions"));

            foreach ($cols as $col) {
                if (isset($col["hidden"])) {
                    continue;
                }

                $options = array();
                if (isset($col["options"])) {
                    $options = $col["options"];
                }

                $tds .= $this->Html->tag("td", $this->_getFieldValue($item, $col), $options);
            }

            $rows .= $this->Html->tag("tr", $tds);
        }

        $tbody = $this->Html->tag("tbody", $rows);

        $content = $thead . $tbody;

        $table = $this->Html->tag("table", $content, array("class" => "table table-striped table-bordered"));

        // Head
        $grid_actions = $this->_createGridActions();

        $panel_heading = $this->Html->tag("div", $title . $this->_createFiltersInfo() . $grid_actions, array("class" => "panel-heading"));

        // Body
        $panel_body = $this->Html->tag("div", $table, array("class" => "panel-body table-responsive"));

        // Footer
        $info = $this->Html->tag("div", $this->_createPagingInfo(), array("class" => "pull-right hidden-xs"));
        $panel_footer = $this->Html->tag("div", $this->_createPagination() . $info, array("class" => "panel-footer"));

        $html = $this->Html->tag("div", $panel_heading . $panel_body . $panel_footer, array("class" => "grid panel panel-default"));

        return $html;
    }

    /**
     * Cria label que exibe a quantidade de filtros utilizados na pesquisa
     *
     * @return string
     */
    private function _createFiltersInfo()
    {
        $query = $this->request->query;

        $tmp = array_filter($query);
        unset($tmp["sidx"]);
        unset($tmp["sord"]);
        $count = count($tmp);

        if ($count) {
            return " - " . $this->Html->tag("span", $count . " filtro(s) selecionado(s)", array("class" => "label label-primary"));
        }

        return "";
    }

    /**
     * Gera url para a action de exportação
     *
     * @return string
     */
    private function _createExportUrl()
    {
        $url = Router::url("/admin/" . $this->params["controller"] . "/index/export:1", true);

        if ($this->params["plugin"]) {
            $url = Router::url("/admin/" . $this->params["plugin"] . "/" . $this->params["controller"] . "/index/export:1", true);
        }

        if (!empty($this->request->query)) {
            $url .= "?" . http_build_query($this->request->query);
        }

        return $url;
    }

    /**
     * Create paging information
     *
     * @return string
     */
    private function _createPagingInfo()
    {
        $params = $this->Paginator->params();

        return sprintf("Exibindo página %s de %s - total de registros: %s", $params["page"], $params["pageCount"], $params["count"]);
    }

    /**
     * Create table header
     *
     * @param array $cols
     * @return string
     */
    private function _createTableHeader($cols)
    {
        $rows = "";
        $rows .= $this->Html->tag("th", "");

        foreach ($cols as $data) {

            if (isset($data["hidden"])) {
                continue;
            }

            $query = $this->request->query;

            if (!isset($query["sord"])) {
                $query["sord"] = "asc";
            }

            $tip = $query["sord"] == "asc" ? "Ordenar (Z-A)" : "Ordenar (A-Z)";

            // Sort ASC
            $query["sidx"] = $data["field"];
            $query["sord"] = $query["sord"] == "asc" ? "desc" : "asc";

            $label = $this->getFieldLabel($data);

            $link = $this->Html->link($label, array("?" => $query), array("class" => "showtooltip", "title" => $tip));

            $options = array();

            if (isset($data["options"])) {
                $options = $data["options"];
            }

            $rows .= $this->Html->tag("th", $link, $options);
        }

        $thead_row = $this->Html->tag("tr", $rows);

        return $this->Html->tag("thead", $thead_row);
    }

    /**
     * Create pagination links
     *
     * @return type
     */
    private function _createPagination()
    {
        $paginator = "";
        $paginator .= $this->Paginator->prev("<<", array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        $paginator .= $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '...'));
        $paginator .= $this->Paginator->next(">>", array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));

        return $this->Html->tag("ul", $paginator, array("class" => "pagination"));
    }

    /**
     * Create item actions
     *
     * @param type $item
     * @return type
     */
    private function _createItemActions($item)
    {
        $actions = array();

        $actions["edit"] = array(
            "title" => "Editar",
            "icon" => "glyphicon-pencil",
            "url" => array("action" => "edit")
        );

        $actions["view"] = array(
            "title" => "Visualizar",
            "icon" => "glyphicon-search",
            "url" => array("action" => "view")
        );

        $actions["delete"] = array(
            "title" => "Excluir",
            "icon" => "glyphicon-trash",
            "url" => array("action" => "delete")
        );

        if (isset($this->options["actions"])) {
            $actions = array_merge($actions, $this->options["actions"]);
        }

        $itens = array();

        $itens[] = $this->Html->tag("input", "", array("data-id" => $item[$this->model]["id"], "class" => "grid-item", "type" => "checkbox", "checked" => $this->itemIsSelected($item[$this->model]["id"])));

        foreach ($actions as $key => $options) {
            $itens[] = $this->_createItemAction($key, $options, $item[$this->model]["id"], $item);
        }

        return implode(" ", $itens);
    }

    /**
     * Create a action
     *
     * @param type $action
     * @param type $id
     * @param type $title
     * @return type
     */
    private function _createItemAction($action, $options, $id, $item = array())
    {
        if (!$options) {
            return;
        }

		if (isset($options['format'])) {
			$fn = $options['format'];

			return $fn($id, $this->Html, $item);
		}

        $url = $options["url"];
        $url[] = $id;

        return $this->Html->link(
            $this->Html->tag("span", "", array("class" => "glyphicon " . $options["icon"])), $url, array("escape" => false, "class" => "showtooltip action-$action", "title" => $options["title"])
        );
    }

    /**
     * Create item actions
     *
     * @param type $item
     * @return type
     */
    private function _createGridActions()
    {
        $actions = array();

        if ($this->options['header_buttons']['export'] === true ) {
            $actions[] = $this->Html->link(
                $this->Html->tag("span", "", array("class" => "glyphicon glyphicon-cloud-download")), $this->_createExportUrl(), array("escape" => false, "class" => "showtooltip action-export pull-right", "title" => "Exportar")
            );
        }

        if ($this->options['header_buttons']['delete'] === true ) {
            $actions[] = $this->Html->link(
                $this->Html->tag("span", "", array("class" => "glyphicon glyphicon-trash")), array("action" => "delete_selection"), array("escape" => false, "class" => "showtooltip action-delete-selection pull-right", "title" => "Excluir items selecionados")
            );
        }

        return implode(" ", $actions);
    }

    /**
     * Get field label
     *
     * @param array $col
     * @return string
     */
    public function getFieldLabel($col)
    {
        if (isset($col["label"])) {
            return $col["label"];
        }

        $parts = explode(".", $col["field"]);

        $model = $parts[0];
        ClassRegistry::init($model);

        if (isset($model::$labels[$parts[1]])) {
            return $model::$labels[$parts[1]];
        } else {
            return $col["field"];
        }
    }

    /**
     * Get field value
     *
     * @param type $item
     * @param type $col
     * @return string
     */
    private function _getFieldValue($item, $col)
    {
        if (Hash::check($item, $col["field"])) {

            $value = Hash::extract($item, $col["field"]);

            if (isset($col["format"])) {
                return $this->_formatField($col, $value, $item);
            }

            if (isset($col["count"])) {
                return count($value);
            }

            return $value[0];
        }

        return false;
    }

    public function _formatField($col, $value, $item)
    {
        $format = $col["format"];

        if (array_key_exists('_formatImage', $format)) {
            $key = current(array_keys($format));
            return $this->$key(array_shift($format), $value[0], $item[$this->model]["id"], $col);
        }

		if (array_key_exists('_formatEmbed', $format)) {
            return $this->_formatEmbed($value[0]);
		}

        if (is_array($format)) {
            return $format[$value[0]];
        }

        if (is_callable($format)) {
            return $format($value[0], $item, $this->Html);
        }
    }

	public function _formatEmbed($value)
	{
		preg_match('/https\:\/\/(www\.)?youtube\.com\/watch\?v\=([^&]*)/', $value, $match);
		if (isset($match[2]) && !empty($match[2])) {
			return $this->Html->image('http://img.youtube.com/vi/' . $match[2] . '/default.jpg');
		}

		return;
	}

    /**
     * Format field to resize image
     *
     * @param array $options   array with (width, height, aspect and crop)
     * @param string $value    image name
     * @param int $id          image id
     * @param string $field    image field
     * @return string          img tag
     */
    public function _formatImage($options, $value, $id, $col)
    {
        $parts = explode(".", $col["field"]);
		$field = end($parts);

		# retorna tag media quando for video (POG mesmo, feito é melhor que perfeito :P)
		$ext = explode('.', $value);
		if (end($ext) == 'mp4') {
			return $this->Html->media('/files/' . strtolower($this->model) . DS . $field . DS . $id . DS . $value, array('type' => 'video', 'text' => 'video incompatível', 'controls' => true, 'width' => 200));
		}

		$options = array_merge(array(
			'width' => 100,
			'height' => 100,
			'aspect' => true,
			'crop' => true
		), $options);

		if (file_exists(ROOT . '/uploads/' . strtolower($this->model) . DS . $field . DS . $id . DS . $value)) {
			return $this->Html->image($this->Image->resize(ROOT . '/uploads/' . strtolower($this->model) . DS . $field . DS . $id . DS . $value, $options['width'], $options['height'], $options['aspect'], $options['crop']));
		}

        return $this->Html->image($this->Image->resize(WWW_ROOT . '/files/' . strtolower($this->model) . DS . $field . DS . $id . DS . $value, $options['width'], $options['height'], $options['aspect'], $options['crop']));
    }

    public function fieldIsSelected($field)
    {
        if (is_null($this->selected_fields)) {
            $this->selected_fields = array();
        }

        return in_array($field, $this->selected_fields);
    }

    public function itemIsSelected($field)
    {
        if (is_null($this->selected_items)) {
            $this->selected_items = array();
        }

        return in_array($field, $this->selected_items);
    }

    public function export($title, $cols = array(), $result = array())
    {
        $tmpfile = tmpfile();
        $line = array();

        foreach ($cols as $col) {
            if (in_array($col["field"], $this->selected_fields)) {
                $line[] = $this->getFieldLabel($col);
            }
        }

        fputcsv($tmpfile, $line);

        // Rows
        foreach ($result as $item) {


            $line = array();
            foreach ($cols as $col) {

                if (in_array($col["field"], $this->selected_fields)) {
                    $value = $this->_getFieldValue($item, $col);
                    $line[] = strip_tags($value);
                }
            }

            fputcsv($tmpfile, $line);
        }

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header(sprintf("Content-Disposition: attachment;filename=%s.csv", strtolower($title)));
        header("Content-Transfer-Encoding: binary");

        fseek($tmpfile, 0);
        $fstats = fstat($tmpfile);

        return fread($tmpfile, $fstats["size"]);
    }
}
