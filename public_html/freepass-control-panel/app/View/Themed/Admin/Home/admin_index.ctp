<div class="row">
    <div class="col-md-12">
        <?php echo $this->Breadcrumb->drop("Home", array("controller" => "home"))->create(); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">Resumo</div>
            <div class="panel-body">
                <?php
                ClassRegistry::init("Foo");
                echo $this->Counter->create(array(
                    array("label" => "Usuários", "model" => "User")
                ));
                ?>
            </div>
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">Cortesias  - <small>após 06/09/2016</small></div>
            <ul class="nav nav-pills nav-stacked">
                <li><?php echo $this->Html->link('Exportar', array('controller' => 'users', 'action' => 'export')); ?></li>
            </ul>
        </div>
    </div>
    
</div>