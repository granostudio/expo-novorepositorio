<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true)); ?>
<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">Importação</div>
            <div class="panel-body">
				<?php
					echo $this->BootstrapForm->input('file', array('type' => 'file', 'label' => 'Arquivo .XLS'));
				?>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('../Common/form_footer'); ?>
<?php echo $this->Form->end(); ?>
