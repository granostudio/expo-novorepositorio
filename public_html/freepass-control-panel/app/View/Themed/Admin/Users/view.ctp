<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<table class="table table-bordered">
		<thead>
	        <th>Campo</th>
	        <th>Valor</th>
        </thead>
		<tbody>
<tr>		<td><?php echo User::$labels['id']; ?></td>
		<td>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['name']; ?></td>
		<td>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['email']; ?></td>
		<td>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['street']; ?></td>
		<td>
			<?php echo h($user['User']['street']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['city']; ?></td>
		<td>
			<?php echo h($user['User']['city']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['state']; ?></td>
		<td>
			<?php echo h($user['User']['state']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['document']; ?></td>
		<td>
			<?php echo h($user['User']['document']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['gender']; ?></td>
		<td>
			<?php echo h($user['User']['gender']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['instagram']; ?></td>
		<td>
			<?php echo h($user['User']['instagram']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['birthday']; ?></td>
		<td>
			<?php echo h($user['User']['birthday']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['password']; ?></td>
		<td>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['active']; ?></td>
		<td>
			<?php echo h($user['User']['active']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['type']; ?></td>
		<td>
			<?php echo h($user['User']['type']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['hash']; ?></td>
		<td>
			<?php echo h($user['User']['hash']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['ingresse_id']; ?></td>
		<td>
			<?php echo h($user['User']['ingresse_id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['ingresse_token']; ?></td>
		<td>
			<?php echo h($user['User']['ingresse_token']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['created']; ?></td>
		<td>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><?php echo User::$labels['modified']; ?></td>
		<td>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</td>
</tr>		</tbody>
	</table>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
	</ul>
</div>
