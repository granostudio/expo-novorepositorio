<div class="row">
    <div class="col-md-12">
        <?php echo $this->Breadcrumb->drop(Inflector::pluralize($model::$label), array('action' => 'index'))
                 ->create($this->Html->link('Adicionar Novo', array('action' => 'add'))); ?>
	</div>
</div>

<div class="row">
    <div class="col-md-3">
        <?php echo $this->element(sprintf('%s/search_form', Inflector::pluralize($model))) ?>
	</div>

    <div class="col-md-9">
		<?php
		$options = array(
			'actions' => array(
				"Users" => array(
					"format" => function($id, $html) {
						return $html->link( $html->tag("span", "", array("class" => "glyphicon glyphicon-log-in")), array("controller" => "invites", "action" => "index", "?" => array('user_id' => $id)), array("escape" => false, "class" => "showtooltip action-gate", "title" => "Ver convidados"));
					}
				),
				"Invites" => array(
					"format" => function($id, $html) {
						return $html->link( $html->tag("span", "", array("class" => "glyphicon glyphicon-plus")), array("controller" => "users", "action" => "increment", $id), array("escape" => false, "class" => "showtooltip action-plus", "title" => "Adicionar convites"));
					}
				)
			)
		);
		$options = [];
		?>
		<?php echo $this->Grid->create(Inflector::pluralize($model::$label), $grid, $result, $options); ?>
	</div>
</div>