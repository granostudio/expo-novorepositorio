<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="panel panel-default animated <?php if(!$this->request->isPost()) echo "bounceInUp" ?>">
            <div class="panel-heading"><?php echo Configure::read("Application.title"); ?> - Login</div>
            <div class="panel-body">
                
                <?php echo $this->BootstrapForm->create($model, array('type' => 'post', 'novalidate' => true)); ?>

                <?php echo $this->BootstrapForm->input('email'); ?>
                <?php echo $this->BootstrapForm->input('password'); ?>

                <?php echo $this->Form->button('<span class="glyphicon glyphicon-ok"></span>  Entrar', array('class' => 'btn btn-success pull-left navbar-btn')); ?>

                <?php echo $this->Html->link('Esqueci minha senha',array("action"=>"remember"),array("class"=>"pull-right"));?>
                
                <?php echo $this->Form->end(); ?>

            </div>

        </div>

        <?php echo $this->Session->flash(); ?>

    </div>
    <div class="col-md-4"></div>

</div>