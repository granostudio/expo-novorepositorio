<?php $this->extend('/Common/search_form'); ?>

<?php echo $this->BootstrapForm->create(false, array('type' => 'get', 'novalidate' => true)); ?>

<?php echo $this->BootstrapForm->input('who'); ?>

<?php echo $this->BootstrapForm->input('action', array("type" => "select", "options" => Log::$actions, "empty" => "Selecione")); ?>

<div>
    <?php echo $this->Form->button('Limpar', array('class' => 'btn btn-default pull-left btn-clear-form', 'type' => 'button')); ?>
    <?php echo $this->Form->submit('Procurar', array('class' => 'btn btn-primary pull-right')); ?>
</div>

<?php echo $this->Form->end(); ?>
