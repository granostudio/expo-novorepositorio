<?php $this->extend('/Common/search_form'); ?>

<?php echo $this->BootstrapForm->create(false, array('type' => 'get', 'novalidate' => true)); ?>

<?php echo $this->BootstrapForm->input('name'); ?>

<?php echo $this->BootstrapForm->input('type', array("type" => "select", "options" => $model::getTypes(), "empty" => "Selecione")); ?>

<div>
    <?php echo $this->Form->button('Limpar', array('class' => 'btn btn-default pull-left btn-clear-form', 'type' => 'button')); ?>
    <?php echo $this->Form->submit('Procurar', array('class' => 'btn btn-primary pull-right')); ?>
</div>

<?php echo $this->Form->end(); ?>
