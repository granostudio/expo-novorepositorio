<?php echo $this->Html->charset(); ?>
<title><?php echo Configure::read("Application.title"); ?> | Painel de Controle</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<?php
if ($this->params['action'] != 'display'):
    define('CURRENT_VIEW', $this->params['controller'] . '/' . $this->params['action']);
else:
    define('CURRENT_VIEW', $this->params['controller'] . '/' . $this->params['pass'][0]);
endif;

$cssList = array(
    'bootstrap.min',
    'layout',
    'animate.min',
    //'mindmap.css',
    'smoothness/jquery-ui-1.10.3.custom.min',
    'bootstrap3-wysihtml5.min',
	'select2.min'
);

if (isset($this->viewVars['requestCss'])):
    $cssList[] = $this->viewVars['requestCss'];
endif;

if (file_exists(CSS . CURRENT_VIEW . '.css')):
    $cssList[] = CURRENT_VIEW . '.css';
endif;

echo $this->Html->meta('icon');
echo $this->fetch('meta');
echo $this->fetch('css');

# echo $this->ScriptCombiner->css($cssList);
echo $this->Html->css($cssList);
?>
