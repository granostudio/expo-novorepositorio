<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <span class="glyphicon glyphicon-thumbs-up"></span> <?php echo $message; ?>
        </div>
    </div>
</div>