<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true)); ?>
<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

<div class="row">
    <div class="col-md-3">


        <div class="panel panel-default">
            <div class="panel-heading">Geral</div>

            <div class="panel-body">

                <?php echo $this->BootstrapForm->input('upload', array('type' => 'file')); ?>
                <?php echo $this->BootstrapForm->input('name'); ?>

                <?php echo $this->BootstrapForm->input('type', array("type" => "select", "options" => $model::$types, "empty" => "Selecione")); ?>
                
                <?php echo $this->BootstrapForm->input('obs', array("maxlength" => "255","class"=>"hide-when","data-field"=>"#FooType","data-equals"=>"")); ?> 
                
                <?php echo $this->BootstrapForm->input('html', array("class"=>"editor")); ?> 

                <?php echo $this->BootstrapForm->input('bar_id', array("type" => "select", "options" => $bars, "empty" => "Selecione")); ?> 
                
            </div>

        </div>

    </div>

    <div class="col-md-3">

        <?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

        <div class="panel panel-default">
            <div class="panel-heading">Especiais</div>

            <div class="panel-body">

                <?php echo $this->BootstrapForm->autocompleteBelongsTo('other_bar_id', 'Bar', 'name', '', array('label' => 'id do Bar')); ?>

                <?php echo $this->BootstrapForm->autocomplete('autocomplete', 'Bar', 'name', array('Bar.id >=' => 1), array('label' => 'nome do Bar')); ?>

                <?php echo $this->BootstrapForm->input('datepicker', array("class" => "format-datepicker", 'type' => 'text')); ?>
                
                <?php echo $this->BootstrapForm->input('Bar', array("type" => "select", "options" => $bars,'multiple'=>true,'label'=>'Bars')); ?> 
                
            </div>

        </div>

    </div>

    <div class="col-md-3">

        <div class="panel panel-default">
            <div class="panel-heading">Formatação</div>

            <div class="panel-body">

                <?php echo $this->BootstrapForm->input('zipcode', array("class" => "format-zipcode")); ?>
                <?php echo $this->BootstrapForm->input('date', array("class" => "format-date", 'type' => 'text')); ?>
                <?php echo $this->BootstrapForm->input('time', array("class" => "format-time", 'type' => 'text')); ?>
                <?php echo $this->BootstrapForm->input('money', array("class" => "format-money", "type" => "text")); ?>
                <?php echo $this->BootstrapForm->input('phone', array("class" => "format-phone")); ?>
                <?php echo $this->BootstrapForm->input('cpf', array("class" => "format-cpf")); ?>
                <?php echo $this->BootstrapForm->input('cnpj', array("class" => "format-cnpj")); ?>
                <?php echo $this->BootstrapForm->input('rate', array("class" => "format-rate", "type" => "text")); ?>
                
                <!-- 
                data-min-value: default 0
                data-max-value: default 999999999.99
                -->
                <?php echo $this->BootstrapForm->input('integer', array("class" => "format-integer", "type" => "text","data-min-value"=>"0","data-max-value"=>"500")); ?>

            </div>

        </div>


    </div>


</div>

<?php echo $this->element('../Common/form_footer'); ?>

<?php echo $this->Form->end(); ?>
