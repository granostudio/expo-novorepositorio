<div class="row">
    <div class="col-md-12">
        <?php
        $add_link = "";
        if (AuthComponent::user("type") == Operator::TYPE_ADMINISTRATOR) {
            $add_link = $this->Html->link("Novo", array("action" => "add"));
        }

        echo $this->Breadcrumb->drop(Inflector::pluralize($model::$label), array("action" => "index"))
                ->create($add_link);
        ?>

    </div>
</div>

<div class="row">

    <div class="col-md-3">

        <?php echo $this->element(sprintf("%s/search_form", Inflector::pluralize($model))) ?>

    </div>

    <div class="col-md-9">

        <?php
        
        $options = array(
            "actions" => array(
                "delete" => false,
                "view" => false
            )
        );

        if (AuthComponent::user("type") == Operator::TYPE_ADMINISTRATOR) {

            $options = array(
                "actions" => array(
                    "view" => false
                )
            );
        }

        echo $this->Grid->create(Inflector::pluralize($model::$label), $grid, $result, $options);
        ?>

    </div>

</div>
