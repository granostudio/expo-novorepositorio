<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Breadcrumb->drop(Inflector::pluralize($model::$label), array("action" => "index"))
                ->drop("Visualizar")
                ->create("ID: " . $this->data[$model]["id"]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12 table-responsive">

        <table class="table table-bordered">
            <thead>
            <th>Campo</th>
            <th>Valor</th>
            </thead>
            <tbody>
                <?php foreach ($this->data[$model] as $field => $value): ?>

                    <tr>

                        <?php if (isset($model::$labels[$field])): ?>

                            <td><?php echo($model::$labels[$field]); ?></td>

                        <?php else: ?>

                            <td><?php echo($field); ?></td>

                        <?php endif; ?>

                        <td><?php echo($value); ?></td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>