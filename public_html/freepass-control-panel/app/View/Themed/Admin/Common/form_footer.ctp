<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    <div class="container-fluid">

        <?php echo $this->Form->button('<span class="glyphicon glyphicon-ok"></span>  Salvar', array('class' => 'btn btn-success pull-right navbar-btn')); ?>

        <?php if (!empty($this->data[$model]["id"])): ?>

            <?php echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span> Excluir', array("action" => "delete", $this->data[$model]["id"]), array('class' => 'btn btn-danger pull-left navbar-btn action-delete', 'escape' => false)); ?>

        <?php endif ?>

    </div>
</nav>