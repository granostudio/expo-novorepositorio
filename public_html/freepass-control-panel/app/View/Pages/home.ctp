<div class="hero-unit container">

	<div class="col-md-8 col-md-offset-2">
		<h1 class="title">
			The art of the brick
		</h1>

		<div>
			<div class="row">
			    <div class="column eight centered">
					<div class="user-info">
						<form name="userForm" novalidate>
					
							<h3 class="title">Identifique-se</h3>
					
							<div class="field">
								<label>CPF</label>
								<input class="input document"
										name="document" 
										ui-mask="999.999.999-99" 
										type="text" />
							</div>
					
							<div class="field">
								<label>Código</label>
								<input class="input passcode" 
										name="passcode" 
										type="text" />
							</div>
					
							<div class="submit-button">
								<button class="button check">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
</div>