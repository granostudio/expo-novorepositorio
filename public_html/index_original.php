<?php include('header.php'); ?>

	<?php include ("banner.php"); ?>

	<div class="content">
		<div class="container">
			<?php include('intro.php'); ?>

			<?php include('about.php'); ?>

			<?php include('author.php'); ?>

			<?php include('audioguia.php'); ?>

			<?php include('tickets_brasilia.php'); ?>
		</div> <!-- .wrapper -->

		<?php include('partners.php'); ?>

		<!--?php include('infos.php'); ?-->

		<?php include('social.php'); ?>
	</div> <!-- .content -->

<?php include("footer.php"); ?>
