module.exports = function(grunt) {
  "use strict";
  // var pkgFTP = grunt.file.readJSON('.ftppass' );
  grunt.initConfig({
    //server local -------------------------------------------------------------
    php: {
        local: {
            options: {
                port: 9001,
                hostname: 'localhost',
                // base: 'wordpress',
                keepalive: false,
                open: true,
                silent: true,
            },
        },
    },
    // DEPLOY ----------------------------------------------------------------
    // ftpush: {
    //   dev: {
    //     auth: {
    //       host: pkgJson.ftphost,
    //       port: pkgJson.ftpport,
    //       authKey: 'key1'
    //     },
    //     src: 'wordpress',
    //     dest: '/public_html/dev',
    //     simple: true
    //     // exclusions: ['path/to/source/folder/**/.DS_Store', 'path/to/source/folder/**/Thumbs.db', 'dist/tmp'],
    //     // keep: ['/important/images/at/server/*.jpg']
    //   },
    // },
    // COMPASS ----------------------------------------------------------------
    compass: { // Task
      admin: { // tema mãe
        options: {  // Target options
          sassDir: 'assets/scss',
          cssDir: 'assets/css',
          environment: 'production',
          outputStyle: 'expanded',
          // require: 'bootstrap-sass'
        }
      },
    },
    // WATCH ----------------------------------------------------------------
    watch: {
      live: {
        files: ['assets/scss/*.scss'],
        tasks: ['compass:admin'],
        options: {
          livereload: true,
        },
      },
    },
  });

  grunt.loadNpmTasks('grunt-php');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-ftpush');




  // --------------------------------------------------------------------------------------
  // TASKs
  // --------------------------------------------------------------------------------------
  // instalação inicial do wp
  grunt.registerTask('grano-local', ['php:local','watch:live']);

  // --------------------------------------------------------------------------------------

};
