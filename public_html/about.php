<div class="row">
	<div class="col col-12"> 
		<h2 id="a-exposicao"> A Exposição </h2>
	</div> <!-- .col-12 -->

	<div class="col col-12">
		<img src="assets/images/img_02.jpg" alt="" />
	</div> <!-- .col-12 -->

	<div class="col col-6">
		<p> As peças LEGO®, presentes tanto nos quartos das crianças quanto no universo dos adultos, são ao mesmo tempo modernas e clássicas, nunca saem de moda, e ainda são imunes a preocupações de adultos, como o cenário político e econômico e a recessão. Os blocos de montar são simples e, no entanto, graças à sua ampla variedade, abrem uma gama quase infinita de possibilidades, garantindo que qualquer pessoa que os tenha em mãos será levada pelo desejo irresistível de brincar, inventar e sonhar. </p>
	</div> <!-- .col-6 -->

	<div class="col col-6">
		<p> Para estimular a obsessão do público por essas pequenas peças de montar, a mostra A ARTE DE CRIAR COM LEGO® foi criada em torno dos conceitos de criatividade e de brincar, o que certamente envolve a criatividade do artista, mas também a do visitante. As obras da exposição são apenas exemplos de tudo o que se pode fazer com peças LEGO®. As obras expostas em A ARTE DE CRIAR COM LEGO® encantam por sua estética, sem dúvida. Mas, acima de tudo, é o surpreendente potencial criativo deste meio que fascina o público de todas as idades. </p>
	</div> <!-- .col-6 -->

</div> <!-- .row -->

<div class="divisao"></div> <!-- .divisao -->