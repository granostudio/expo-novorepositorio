<div class="row infos" id="informacoes">
	<div class="container">
		<h2> A Exposição no Rio de Janeiro </h2>
		<p>
			<!-- <img style="width:5%;" src="assets/images/icons/pin.png" /> -->
			MHN - Museu Histórico Nacional -  Praça Marechal Âncora, s/nº<br />
            De 17 de novembro de 2016 a 15 de janeiro de 2017
		</p>
		<br/><br/>
		<p style="font-size:18px; color:#000000; letter-spacing:1px;">
			Agendamentos para escolas: <a href="mailto:agendamento@expo-theartofthebrick.com.br?Subject=Agendamento" target="_top" style="color:#000000;">agendamento@expo-theartofthebrick.com.br</a>
		</p>
		<!-- <p> MHN - Museu Histórico Nacional - Praça Marechal Âncora, s/n˚ </p> -->
	</div>
</div> <!-- .row .infos -->
