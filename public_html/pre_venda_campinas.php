<head>
  <link rel="stylesheet" href="assets/css/pre_venda_campinas.css">
<!-- Latest compiled and minified CSS -->
</head>

<div class="col-sm-12 titulo" id="ingressos">
    <h1>INGRESSOS CAMPINAS</h1>
  </div>
  <div class="container">
    <!-- <div class="row">
      <div class="col-md-10 col-sm-offset-1 linha1">
        <p>TODOS: Os ingressos da exposição terão um preço especial do dia 01/08 até a abertura da exposição. Deve ser dito como: PRIMEIRO LOTE A VENDA: Garanta seu ingressos a preços especiais antes da abertura da exposição. Aproveite!</p>
      </div>
    </div> -->

    <div class="row">
      <div class="col-md-5half col-sm-offset-1half coluna1">
        <p><strong>LOCAL:</strong> Shopping Iguatemi - Avenida Iguatemi, 777, Vila Brandini, 1º Piso.</p>
        <p><strong>HORÁRIO DA EXPOSIÇÃO:</strong> Terça a Sábado, das 13h às 22h (Fechado às 2ª feiras), Domingos e feriados, das 13h até as 20h.</p>
        <p><strong>ABERTURA:</strong> 23 de agosto.</p>
        <p>
          <strong>ENCERRAMENTO:</strong> 22 de outubro.<br>
          <strong>VENDAS:</strong> a partir de 25 de julho.
        </p>
        <p>
          <strong>Horário de Vendas:</strong><br>
          <strong>Segunda-feira</strong> - Apenas vendas on-line.<br>
          <strong>Terça e Sábado</strong> - 13:00 às 21:00.<br>
          <strong>Domingos e Feriados</strong> - 13:00 às 19:00.
        </p>
        <p><strong>CANAIS DIGITAIS:</strong> (sujeito a taxa de conveniência)</p>
        <p>
            <strong>Site vendas:</strong> <a href="http://www.eventim.com.br/" target="_blank">www.eventim.com.br</a><br>
            <strong>Site Brasil:</strong> <a href="http://expo-theartofthebrick.com.br/" target="_blank">expo-theartofthebrick.com.br</a><br>
            <strong>Facebook:</strong> <a href="https://pt-br.facebook.com/theartofthebrickbrasil/" target="_blank">theartofthebrickbrasil</a><br>
        </p>
        <p><strong>CANAL DE VENDA FÍSICA:</strong> Bilheteria Oficial sem taxa de conveniência em frente ao Teatro do shopping Iguatemi Campinas.</p>
        <p><strong>INGRESSOS:</strong></p>
        <p>
          Até dia 22 de Agosto:<br>
          <strong>- R$ 25,00</strong> (inteira)<br>
          <strong>- R$ 12,50</strong> (meia-entrada)<br><br>
          A partir do dia 23/08:<br>
          <strong>- R$ 30,00</strong> (inteira)<br>
          <strong>- R$ 15,00</strong> (meia-entrada)<br>
        </p>
        <p><strong>QUEM TEM DIREITO À MEIA ENTRADA: Idosos com idade superior a 60 (sessenta) anos</strong> têm direito a meia-entrada. Para comprovação, basta apresentar o documento de identidade;</p>
        <p><strong>Pessoas com necessidades especiais e um acompanhante</strong>, tem direito a meia-entrada. O documento exigido no local de realização do evento para pessoas com necessidades especiais,
          será:<br> a) O cartão de Benefício de Prestação Continuada da Assistência Social da pessoa com deficiência, ou;<br> b) Documento emitido pelo Instituto Nacional do Seguro Social - INSS que ateste a
          aposentadoria de acordo com os critérios estabelecidos na Lei Complementar nº 142, de 8 de maio de 2013.<br>	c) O documento do beneficiado, sempre deverá ser acompanhado do documento de identificação
          com foto  expedido por órgão público e válido em todo o território nacional.<br><strong>Acompanhante:</strong> também tem direito ao benefício da meia-entrada (somente um acompanhante por pessoa com necessidade especial);</p>
      </div>
      <div class="col-md-5half coluna1">
        <p><strong>Jovens com idade entre 15 e 29 anos que pertencem à famílias de baixa renda</strong> (até dois salários mínimos/mes) , inscritas no Cadastro Único para Programas Sociais do Governo Federal - CadÚnico;</p>
        <p><strong>Estudantes de escolas públicas ou privadas</strong> de todos os níveis, mediante apresentação de  carteira de identidade com data de validade até o dia 31 de março do ano subsequente ao de sua expedição.</p>
        <p><strong>Menores de 12 anos</strong> mediante apresentação de documento válido com foto.</p>
        <p><strong>Professor da rede pública estadual e municipal e de escolas particulares</strong> (apresentar documento de identidade oficial com foto e carteira funcional da Secretaria de Educação ou Holerite que comprove a condição).</p>
        <p><strong>QUEM TEM DIREITO A GRATUIDADE: </strong></p>
        <p><strong>Crianças até 2 anos de idade;</strong></p>
      </div>
      <div class="col-md-5half coluna2" id="informacoes">
        <p><strong>Mais informações sobre o artista e a exposição:</strong></p>
        <p><strong>Nathan Sawaya</strong> (<a href="http://www.nathansawaya.com/" target="_blank">www.nathansawaya.com</a>)</p>
        <p><strong>Website:</strong> <a href="http://www.brickartist.com/" target="_blank">www.brickartist.com</a></p>
        <p><strong>Twitter:</strong> <a href="https://twitter.com/nathansawaya" target="_blank">NathanSawaya</a></p>
        <p><strong>Site Brasil:</strong> <a href="http://expo-theartofthebrick.com.br/" target="_blank">expo-theartofthebrick.com.br</a></p>
        <p><strong>Facebook:</strong> <a href="https://pt-br.facebook.com/theartofthebrickbrasil/" target="_blank">theartofthebrickbrasil</a></p>
        <p><strong>Instagram:</strong> <a href="https://www.instagram.com/theartofthebrickbrasil/?hl=pt-br" target="_blank">theartofthebrickbrasil</a></p>
      </div>
      <div class="col-md-5half" style="position: relative;height: 389px;">
        <a href="http://www.eventim.com.br/the-art-of-the-brick-ingressos.html?affiliate=BR1&doc=artistPages%2Ftickets&fun=artist&action=tickets&erid=1973259&includeOnlybookable=true" target="_blank"><img src="assets/images/bt-comprar.png" alt="" class="btn-ingresso"></a>
      </div>
    </div>

  </div>
