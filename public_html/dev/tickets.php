<div class="tickets-container wrapper tickets" id="ingressos">
	<div class="half-wrapper">
		<div class="row">
			<div class="col col-12">
				<h2 class="title"> Ingressos </h2>
			</div> <!-- .col .col-12 -->
		</div> <!-- .row -->

		<div class="row">
			<div class="col col-6 border-l">
				<p>
					<span class="bold subtitle">Inteira: </span> <span class="subtitle"> R$ 20,00 </span> <br/>
					<span class="bold subtitle">Meia: </span> <span class="subtitle"> R$ 10,00 </span> <br/>
					<small> (mediante apresentação de carteirinha) </small>
				</p>

				<!-- <p>
					Professores de Ensino Fundamental e Médio, de escolas públicas e particulares, têm entrada franca em todos os dias e horários, desde que comprovem atividade em 2016;
				</p> -->

				<p>
					<b> Quem tem direito a meia entrada: </b> <br/>
					<!-- Aposentados,  Idosos,  Estudantes, Deficientes físicos (neste caso, os acompanhantes também tem direito a meia entrada); -->
					<ul class="info-list">
						<li>Idosos com idade superior a 60 (sessenta) anos têm direito a meia-entrada. Para comprovação, basta apresentar o documento de identidade</li>

						<li>Pessoas com necessidades especiais e um acompanhante, tem direito a meia-entrada. O documento exigido no local de realização do evento para pessoas com necessidades especiais, será:<br/><br/>
<ul>
<li>a) O cartão de Benefício de Prestação Continuada da Assistência Social da pessoa com deficiência, ou;</li>
<li>b) Documento emitido pelo Instituto Nacional do Seguro Social - INSS que ateste a aposentadoria de acordo com os  critérios estabelecidos na Lei Complementar nº 142, de 8 de maio de 2013.</li>
<li>c) O documento do beneficiado, sempre deverá ser acompanhado do documento de identificação com foto  	expedido por órgão público e válido em todo o território nacional.</li>
<li>d) Acompanhante: também tem direito ao benefício da meia-entrada (somente um acompanhante por pessoa com necessidade especial)</li>
</ul>
						</li>

						<li>Jovens com com idade entre 15 e 29 anos que pertencem à famílias de baixa renda (ate 3 sal. Mínimos/mês), inscritas no Cadastro Único para Programas Sociais do Governo Federal - CadÚnico;</li>
						<li>Estudantes de escolas públicas ou privadas de todos os níveis, mediante apresentação de  carteira de identidade com data de validade até o dia 31 de março do ano subsequente ao de sua expedição</li>
						<li>Menores de 21 anos mediante apresentação de documento válido com foto.</li>
					</ul>
				</p>

				<p>
					<b> Quem tem direito a gratuidade: </b> <br/>

					<ul class="info-list">
						<li>Crianças até 02 anos de idade; </li>

						<li>Guia de Turismo*;</li>

						<li>Professor da rede pública estadual e municipal e de escolas  particulares * (apresentar documento de identidade oficial com foto e carteira funcional da Secretaria de Educação ou Holerite que comprove a condição.).
Obs.: *Têm entrada franca em todos os dias e horários respeitando-se o limite de visitantes na exposição no    momento. </li>
					</ul>
				</p>
			</div> <!-- .col .col-5 .text-center -->

			<div class="col col-6 border-l">
				<p class="precos">
					<span class="bold subtitle"> Horário de Funcionamento/Visitação: </span> <br/>
					Terça a Sexta feira , das 10h00 às 17h30 (Fechado às  2ª feiras)<br/>
					Sábados, Domingos e Feriados, das 10h00 às 17h00	<br/><br/>
					<span class="bold subtitle">A Exposição estará fechada nos dias:</span><br/>
					24, 25 e 26 dezembro, reabrindo dia 27 às 10h;<br/>
					31 dezembro, 1º e 2 de janeiro 2017, reabrindo dia 3 às 10h.
				</p>

				<p class="precos">
					<span class="bold">Pela internet:</span>
				</p>
				<ul class="info-list">
					<li>Site vendas: www.ingresse.com</li>
					<li>Site Brasil: expo-theartofthebrick.com.br</li>
					<li>Facebook: facebook/theartofthebrickbrasil</li>
					<li>Instagram: instagram/theartofthebrickbrasil</li>
				</ul>
				<a class="comprar" href="http://expo-theartofthebrick.com.br/comprar-ingressos/">
					<img src="assets/images/bt-comprar-carimbado.png" alt="Comprar Ingresso" />
				</a>

				<p><span class="bold">Bilheteria física:</span></p>
				<ul class="info-list"><li>No Museu - MHN; 3ªf  a Domingo das 10h00 às 17h00. / Sábados, Domingos e Feriados, das 10h00 às 16h30</li></ul>


				<div class="atencao">
					<br/><span class="bold subtitle" style="font-size: 28px; font-family: 'StagBold'"> ATENÇÃO </span> <br/>

					<p> Para Pontos de Venda e Bilheterias é necessária a comprovação do direito ao benefício da 1/2 entrada no ato da compra e no acesso ao evento. </p>
					<p> Para vendas pela Internet e Telefone é necessária a comprovação do direito ao benefício da 1/2 entrada no acesso ao evento. </p>
					<p> Caso o benefício não seja comprovado, o portador deverá complementar o valor do ingresso adquirido para o valor do ingresso integral, caso contrário o acesso ao evento não será permitido. </p>
				</div>
				<p>
					<span class="bold subtitle"> MHN - Museu Histórico Nacional </span> <br/>
					<span class="endereco">
						Praça Marechal Âncora, s/nº<br/>
					</span>
					<strong>Período: de 17 de novembro 2016 a 22 de janeiro de 2017</strong>
				</p>
				<div class="mapa">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.202426166454!2d-43.171701685079746!3d-22.90590334359806!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x997e50222e9573%3A0x54c54f452c889eb8!2sMuseu+Hist%C3%B3rico+Nacional!5e0!3m2!1spt-BR!2sbr!4v1478513501576" width="100%" height="225" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<p>
					 <strong>Ingressos estão sujeito a disponibilidades.</strong>
				</p>

			</div> <!-- .col .col-7 -->
		</div> <!-- .row -->

	</div> <!-- .half-wrapper -->
</div> <!-- .tickets-container .wrapper -->
