<div class="row social">
	<div class="container">
		<div class="col col-12">
			<h2 class="title"> Não deixe de nos acompanhar em nossas redes sociais </h2>
		</div>
		<div class="col col-2">
			<div class="media-hide" style="width: 302px; height: 87px;">

			</div>
		</div>
		<div class="col col-4">
			<div class="btn-facebook">
				<div class="btn-lego">
					<div class="wrap">
						<img src="assets/images/icons/facebook.png" alt="Facebook" />

						<div class="fb-like" data-href="https://www.facebook.com/theartofthebrickbrasil" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
					</div> <!-- .wrap -->
				</div> <!-- .btn-lego -->

				<p style="text-align:center;">
					<a style="color:#fff; margin-left:20%; font-size:16px; letter-spacing:1px;" href="http://facebook.com/theartofthebrickbrasil">
						facebook.com/theartofthebrickbrasil
					</a>
				</p>
			</div> <!-- .btn-facebook -->
		</div>
		<div class="col col-4">
			<div class="btn-instagram">
				<div class="btn-lego no-bg">
					<a href="https://www.instagram.com/theartofthebrickbrasil/" target="_blank">
						<img src="assets/images/bt-instagram.png" />
					</a>

					<p style="text-align:center;">
						<a style="color:#fff; font-size:16px; letter-spacing:1px;" href="http://instagram.com/theartofthebrickbrasil">
							instagram.com/theartofthebrickbrasil
						</a>
					 </p>
				</div> <!-- .btn-lego -->
			</div> <!-- .btn-instagram -->
		</div>
	</div>
</div>
