<footer class="footer">
	<div class="container">
		<div class="row contato">
			<div class="col col-12">
				<p style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">
					<span class="icons-contact"></span>
					<span class="text subtitle" style="text-transform: uppercase; font-size: 18px;">Contato: </span>
					<a href="mailto:contato@ring.art.br">contato@ingresse.com</a>
				</p>
			</div>
		</div> <!-- .row -->
		
		<div class="row">
			
			<div class="col col-4">
				<h3>
					<span class="icons-onibus"></span>
					<span class="text subtitle" style="text-transform: uppercase; font-size: 18px;">Ônibus</span>
				</h3>

				<p style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">Linhas de ônibus que passam perto do parque Ibirapuera:</p>

				<ul>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Linha 5154 Terminal Sto. Amaro/Estação da Luz.</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Linha 677A Jd. Ângela/Metrô Ana Rosa.</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Linha 5630-10 Terminal Grajaú/ Metrô Brás.</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Linha 775C Jd. Maria Sampaio/Metrô Santa Cruz.</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Linha 675N Terminal Sto. Amaro/Metrô Ana Rosa.</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Linha 775A Jd. Adalgiza/Metrô Vila Mariana.</li>
				</ul>

				<p class="mini" style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">Para mais detalhes sobre as linhas visite o <i>site</i> da SPTRANS: <a href="http://www.sptrans.com.br" target="_blank">www.sptrans.com.br</a></p>
			</div> <!-- .col .col-4 -->
			
			<div class="col col-4 metro">
				<h3>
					<span class="icons-metro"></span>
					<span class="text subtitle" style="text-transform: uppercase; font-size: 18px;">Metrô</span>
				</h3>

				<p style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">As estações de Metrô próximas são:</p>

				<ul>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Santa Cruz</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Paraíso</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Vila Mariana</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Brigadeiro</li>
					<li style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">• Ana Rosa</li>
				</ul>

				<p class="mini" style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">Segunda a Domingo, 06h – 00h.</p>
			</div> <!-- .col .col-4 -->
			
			<div class="col col-4">
				<h3>
					<span class="icons-estacionamento"></span>
					<span class="text subtitle" style="text-transform: uppercase; font-size: 18px;">Estacionamento</span>
				</h3>

				<p style="font-family: 'Source Sans Pro', sans-serif; font-weight: 300">Zona Azul - R$5,00 / 2h<br />
					Seg – sex, 10h – 20h<br />
					Sáb, dom e feriados, 08h – 18h</p>
			</div> <!-- .col .col-4 -->

		</div> <!-- .row -->
	</div> <!-- .container -->
</footer>

<?php 

	/*
	<footer class="footer">
		<div class="container">
			<div class="top">
				<div class="left">
					<h3>
						<span class="icons-contact"></span>
						<span class="text">Contato</span>
					</h3>

					<p><a href="mailto:contato@ring.art.br">contato@ingresse.com</a></p>
				</div>

				<div class="right">
					<h3>
						<span class="icons-estacionamento"></span>
						<span class="text">Estacionamento</span>
					</h3>

					<p>Zona Azul - R$5,00 / 2h<br />
						Seg – sex, 10h – 20h<br />
						Sáb, dom e feriados, 08h – 18h</p>
				</div>
			</div>

			<div class="bottom">
				<div class="left">
					<h3>
						<span class="icons-onibus"></span>
						<span class="text">Ônibus</span>
					</h3>

					<p>Linhas de ônibus que passam perto do parque Ibirapuera:</p>

					<ul>
						<li>• Linha 5154 Terminal Sto. Amaro/Estação da Luz.</li>
						<li>• Linha 677A Jd. Ângela/Metrô Ana Rosa.</li>
						<li>• Linha 5630-10 Terminal Grajaú/ Metrô Brás.</li>
						<li>• Linha 775C Jd. Maria Sampaio/Metrô Santa Cruz.</li>
						<li>• Linha 675N Terminal Sto. Amaro/Metrô Ana Rosa.</li>
						<li>• Linha 775A Jd. Adalgiza/Metrô Vila Mariana.</li>
					</ul>

					<p class="mini">Para mais detalhes sobre as linhas visite o <i>site</i> da SPTRANS: <a href="http://www.sptrans.com.br" target="_blank">www.sptrans.com.br</a></p>
				</div>

				<div class="right">
					<h3>
						<span class="icons-metro"></span>
						<span class="text">Metrô</span>
					</h3>

					<p>As estações de Metrô próximas são:</p>

					<ul>
						<li>• Santa Cruz</li>
						<li>• Paraíso</li>
						<li>• Vila Mariana</li>
						<li>• Brigadeiro</li>
						<li>• Ana Rosa</li>
					</ul>

					<p class="mini">Segunda a Domingo, 06h – 00h.</p>
				</div>
			</div>
		</div>
	</footer> */ ?>
</body>
</html>