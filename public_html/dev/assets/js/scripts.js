$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top-150
        }, 1000);
        return false;
      }
    }
  });
});

$(document).ready(function() {
    $('*:not(nav#menu)').click(function(e){
        if( $('nav#menu').hasClass('active') ){
            if( $(e.target).closest("nav#menu").length != 1 && $('div:not(.menu-anchor)') && $(e.target).closest(".menu-anchor").length != 1){
                $(".menu-anchor").removeClass("active");
                $('nav#menu').removeClass('active');
                $("body").removeClass("active");
                $("header.header").removeClass("active");
            }
        }
    });

    $(".menu-anchor").click(function(e){
        e.preventDefault();
        if( $('nav#menu').hasClass('active') ){            
            $("nav#menu").stop().fadeOut();
            $("nav#menu").removeClass("active");
        }else{
            setTimeout(function(){
                $("nav#menu").stop().fadeIn();
                $("nav#menu").addClass("active");
            }, 100);
        }
    });

    $("nav#menu ul li a").click(function(){
        if($("#menu").hasClass('active')){
            $("nav#menu").stop().fadeOut();
            $("nav#menu").removeClass("active");
        }
    });

});