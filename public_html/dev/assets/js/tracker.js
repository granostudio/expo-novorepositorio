$(function(){

	$("nav#menu li a").click(function(){
		var isso = $(this);
		var id = isso.attr('href');

		switch(id){
			case '#a-exposicao':
				ga('send', 'event', 'Art Of The Brick', 'Clicou no Menu', 'Exposição');
			break;

			case '#nathan-sawaya':
				ga('send', 'event', 'Art Of The Brick', 'Clicou no Menu', 'Nathan Sawaya');
			break;

			case '#audioguia':
				ga('send', 'event', 'Art Of The Brick', 'Clicou no Menu', 'Audioguia');
			break;

			case '#ingressos':
				ga('send', 'event', 'Art Of The Brick', 'Clicou no Menu', 'Ingressos');
			break;

			case '#informacoes':
				ga('send', 'event', 'Art Of The Brick', 'Clicou no Menu', 'Informações');
			break;
		}
	});

	$('#banner .btn-ingressos').click(function(){ 
		ga('send', 'event', 'Art Of The Brick', 'Clicou em Comprar Ingresso', 'Banner Topo');
		ga('send', 'event', 'Art Of The Brick', 'Clicou em Comprar Ingresso', 'App Ingresse'); 
	});
	$('#banner-mobile .btn-ingressos').click(function(){ 
		ga('send', 'event', 'Art Of The Brick', 'Clicou em Comprar Ingresso', 'Fim da Página');
		ga('send', 'event', 'Art Of The Brick', 'Clicou em Comprar Ingresso', 'App Ingresse');
	});
});