<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title> Art of The Brick - Exposição de Esculturas com LEGO® </title>
	<meta name="googlebot" content="noindex">

	<meta name="description" content="Confira essa incrível exposição com esculturas feitas de LEGO®, veja aqui os detalhes, datas e compre seu ingresso." />
	<meta name="keywords" content="lego, exposição lego, exposição, criar, Nathan Sawaya,"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">

	<link rel="stylesheet" type="text/css" href="assets/css/grid.css?v=1" />
	<link rel="stylesheet" type="text/css" href="assets/css/landpage.css?v=1" />
	<link rel="stylesheet" type="text/css" href="assets/css/fausto.css?v=1" />

	<script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
	<script src="assets/js/scripts.js?v=1"></script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-79382633-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<script src="assets/js/tracker.js?v=1"></script>

</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

	<header class="header">
		<div class="container">

			<h1 id="logo">
				<a href="/"> 
					<img src="assets/images/logo.png" alt="A arte de criar com LEGO®">
				</a> 
			</h1> <!-- #logo -->

			<a href="#" id="toggle" class="menu-anchor">
				<ul><li></li><li></li><li></li></ul>
			</a>

			<nav id="menu">
				<ul>
					<li> <a href="#a-exposicao"> A exposição </a> </li>
					<li> <a href="#nathan-sawaya"> Nathan Sawaya </a> </li>
					<li> <a href="#audioguia"> Audioguia </a> </li>
					<li> <a href="#ingressos"> Ingressos </a> </li>
					<li> <a href="#informacoes"> Informações </a> </li>
				</ul>
			</nav> <!-- #menu -->

		</div> <!-- .wrapper -->
	</header> <!-- .header -->