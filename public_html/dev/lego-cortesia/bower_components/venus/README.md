Venus
=====

This project is an AngularJS based UI kit. Inside of it you will find the directives and classes for use components as Ingresse uses. Visit: http://bissu.github.io/venus

# Getting Started

Installation is easy as easy as any other bower package installation. You just need to run:

    bower install -S venus
