'use strict';

/**
 * @ngdoc function
 * @name ingresseFreepassPickerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ingresseFreepassPickerApp
 */
angular.module('ingresseFreepassPickerApp')
  .controller('calendarCtrl', function ($scope) {
    $scope.vm = { 
    	'events':  [],
    	'calendarView':  'month',
    	'viewDate': moment().startOf('month').toDate()
    }
  });
