'use strict';

angular.module('ingresseFreepassPickerApp')
  .controller('FreepassCtrl', function ($scope, $route, freepassService, $location) {
    
    $scope.user = {};
    /**
     * Init controller
     */
    $scope.init = function () {
        freepassService.verify($route.current.params.passCode).then(function(){
            $scope.user = freepassService.getUser();
            
            if ($scope.user.freepass == true) {
                $location.path('/');
            } else {
                window.location.href = 'http://localhost/freepass-control-panel';
            }
        });
    };
    
    /**
     * On view content loaded
     */
    $scope.$on('$viewContentLoaded', function () {
        $scope.init();
    });
});