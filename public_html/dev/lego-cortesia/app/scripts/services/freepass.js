'use strict';

angular.module('ingresseFreepassPickerApp')
.factory('freepassService', function ($http, $q) {
    var factory = {};
	
	/**
	 * Define user
	 */
	factory.user = null;
	
	/**
     * Define the default env
     */
    factory.environment = 'hml';

    /**
     * Default envs values
     */
    factory.environments = {
        dev  : 'https://secure.localhost/freepass-control-panel',
        hml  : 'http://localhost/freepass-control-panel',
        prod : 'http://www.theartofthebrick.com'
    };

	factory.getUser = function() {
		return factory.user;
	};

	/**
	 * verify if the invitation is valid
	 *
	 * @param {string} passCode
	 */
	factory.verify = function(passCode) {
		return factory.request('/api/users/verify.json', 'POST', {'document' : passCode}).then( function (result) {
			if (result.data.result.result == true) {
				factory.user = result.data.result.user;
			}
		});
	};

	/**
	 * verify if the invitation is valid
	 *
	 * @param {string} passCode
	 */
	factory.paid = function() {
		return factory.request('/api/users/paid.json', 'POST', {'id' : factory.user.id}).then( function (result) {
			console.log(result);
		});
	};

	/**
	 * Make requests
	 *
	 * @param {string} url
	 * @param {string} type
	 * @param {object} data
	 */
	factory.request = function(url, type, data) {
		var deferred = $q.defer(),
			request = {
				method: type,
				url: factory.environments[factory.environment] + url,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
		};
		
		if (data) {
			request.data = data;
		}
		
		$http(request)
			.then(function(results) {
				deferred.resolve(results);
			})
			.catch(function (error) {
				deferred.reject(error);
			});
			
		return deferred.promise;
	};
	
	return factory;
});