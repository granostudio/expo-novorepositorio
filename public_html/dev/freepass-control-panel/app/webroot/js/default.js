;(function($, window){

	jQuery(function($) {
		
		var Freepass = {};

		Freepass.init = function() {

			$('.check').on('click', function(e) {
				e.preventDefault();
				e.stopPropagation();
				
				var userDocument = $('input.document').val();
				var userPasscode = $('input.passcode').val();

				if (userDocument.length <= 0 || userPasscode.length <= 0) {
					return false;
				};

				$.ajax({
					method: "POST",
					url: URL_BASE + "api/users/check.json",
					data: { "document": userDocument, "passcode": userPasscode }
				})
				.done(function( msg ) {
					if (msg.result.result == true) {
						if (msg.result.user.freepass == true) {
							window.location.href = 'http://localhost/freepass/app/#/';
						};

						if (msg.result.user.passkey == true) {
							window.location.href = 'http://localhost/lego/app/#/session/localhost/event/17010/passCode/' + userDocument;	
						};
					};
					
				});
			});
			
		};

		Freepass.init();
	});

})(jQuery, this);