<?php
App::uses('AppModel', 'Model');
require_once('convert.models.php');

class ConvertableTest extends CakeTestCase {

	public $fixtures = array('app.convert');
	
	function startTest() {
		$this->Convert = ClassRegistry::init('Convert');
	}
	
	public function testFormatToDecimal() {
		$convert = $this->Convert->find('first');
		$expected = '100.000,00';
		$this->assertEqual($convert['Convert']['value'], $expected);
	}
	
	public function testFormatToSave() {
		$convert = array(
			'value' => '2.200.479,00',
			'date' => '10/10/2014',
			'datetime' => '10/10/2014 12:42:54'
		);
		$save = $this->Convert->save($convert);
		
		$this->assertTrue($this->Convert->find('count') == 2);
		
		$expected = '2.200.479,00';
		$newConvert = $this->Convert->findById(2);
		$this->assertEqual($newConvert['Convert']['value'], $expected);
	}
	
	public function testValidateData()
	{
		$convert = array(
			'value' => '2.200.479,129',
			'date' => '10/13/2014',
			'datetime' => '1/12/14 12:42:54'
		);
		$save = $this->Convert->save($convert);
		$this->assertTrue($this->Convert->find('count') == 2);
	}
}