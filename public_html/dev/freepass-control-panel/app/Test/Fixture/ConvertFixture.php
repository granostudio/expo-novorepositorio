<?php
class ConvertFixture extends CakeTestFixture {

	public $fields = array(
		'id' => array('type' => 'integer', 'key' => 'primary'),
		'value' => array('type' => 'float', 'length' => '10,2', 'null' => false),
		'date' => array('type' => 'date', 'null' => true, 'default' => null),
		'datetime' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	public function init() {
		$this->records = array(
			array(
				'id' => 1,
				'value' => '100000.00',
				'date' => '2014-06-09',
				'datetime' => '2014-06-09 18:03:27'
			)
		);
		parent::init();
	}
}