<?php

App::uses('NewsletterAppController', 'Newsletter.Controller');

class NewslettersController extends NewsletterAppController
{

    public $paginate = array(
        'limit' => 2,
    );

    public function beforeFilter()
    {
        parent::beforeFilter();

        ClassRegistry::init($this->plugin . "." . $this->model);

        $this->grid = array(
            array("field" => "Newsletter.name"),
            array("field" => "Newsletter.surname"),
            array("field" => "Newsletter.email"),
            array("field" => "Newsletter.phone")
        );
    }

    public function admin_index()
    {

        $options = array(
            "name" => array("operation" => "contain"),
            "phone" => array("operation" => "contain"),
        );

        $this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
        $this->paginate['order'] = SearchComponent::createOrder($this->request, array('Newsletter.email' => 'asc'));

        $this->set('grid', $this->grid);

        if (!isset($this->params->named["export"])) {
            $this->set('result', $this->paginate($this->model));
        } else {
            $this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
            $this->render("/Common/admin_export");
        }
    }

}
