<?php

App::uses('NewsletterAppModel', 'Newsletter.Model');

class Newsletter extends NewsletterAppModel
{

    public $validate = array(
        'name' => array(
            'rule' => array('minLength', '3'),
            'message' => 'Este campo deve conter no minimo 3 caracteres'
        ),
        'email' => array(
            'mustUnique' => array(
                'rule' => 'isUnique',
                'required' => true,
                'message' => 'E-mail já cadastrado!',
                'last' => true
            ),
            'mustNotEmpty' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'message' => 'O e-mail deve ser preenchido!',
                'last' => true
            ),
            'mustBeEmail' => array(
                'rule' => array('email'),
                'message' => 'E-mail inválido',
                'last' => true
            )
        ),      
        
    );
    
    /**
     * Nome do modelo em português, usado nas views
     * @var string
     */    
    public static $label = "Newsletter";
    
    public static $labels = array(
        "name" => "Nome",
        "surname" => "Sobrenome",       
        "email" => "Email",
        "phone" => "Telefone",
    );

}