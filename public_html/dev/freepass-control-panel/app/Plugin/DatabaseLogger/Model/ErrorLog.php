<?php

class ErrorLog extends DatabaseLoggerAppModel
{

    public $name = 'ErrorLog';
    public $displayField = 'type';
    public $searchFields = array('ErrorLog.type');
    public $useTable = 'error_logs';
    public $actsAs = array();

    /**
     * Nome do modelo em português, usado nas views
     * @var string
     */
    public static $label = "Log de Erro";

    /**
     * Nome do modelo em português
     * @var type 
     */
    public static $labels = array(
        "type" => "Tipo",
        "message" => "Mensagem",
        "ip" => "Endereço IP",
        "hostname" => "Data",
        "uri" => "uri",
        "refer" => "Url",
        "created" => "Data"
    );

    public function beforeSave($options = array())
    {
        $this->Behaviors->detach('Logable');
        $this->data[$this->alias]['ip'] = env('REMOTE_ADDR');
        $this->data[$this->alias]['hostname'] = env('HTTP_HOST');
        $this->data[$this->alias]['uri'] = env('REQUEST_URI');
        $this->data[$this->alias]['refer'] = env('HTTP_REFERER');
        return true;
    }

    /**
     * Return a text search on message
     */
    public function textSearch($query = null)
    {
        if ($query) {
            if (strpos($query, 'type@') === 0) {
                $query = str_replace('type@', '', $query);
                if (strpos($query, '|')) {
                    $types = explode('|', $query);
                    $retval = array();
                    foreach ($types as $type) {
                        $retval['OR'][] = array('ErrorLog.type' => $type);
                    }
                    return $retval;
                } else {
                    return array('ErrorLog.type' => $query);
                }
            } else {
                $escapedQuery = $this->getDataSource()->value($query);
                return array("MATCH ({$this->alias}.message) AGAINST ($escapedQuery)");
            }
        }
        return array();
    }

    /**
     * Return all the unique types
     */
    public function getTypes()
    {
        $cache_key = 'database_log_types';
        if ($retval = Cache::read($cache_key)) {
            return $retval;
        }
        $retval = $this->find('all', array(
            'fields' => array('DISTINCT ErrorLog.type'),
            'order' => array('ErrorLog.type ASC')
        ));
        $retval = Hash::extract($retval, '{n}.ErrorLog.type');
        Cache::write($cache_key, $retval);
        return $retval;
    }

    public function search($params = array(), $options = array())
    {
        $conditions = parent::search($params);
        if (isset($params['start_date']) && !empty($params['start_date'])) {
            $params['start_date'] = str_replace('-', '/', $params['start_date']);
            $conditions['AND']["ErrorLog.created >="] = $this->str2datetime($params['start_date']);
        }
        if (isset($params['end_date']) && !empty($params['end_date'])) {
            $params['end_date'] = str_replace('-', '/', $params['end_date']);
            $conditions['AND']["ErrorLog.created <="] = $this->str2datetime($params['end_date'] . " 23:59:59");
        }
        return $conditions;
    }

}