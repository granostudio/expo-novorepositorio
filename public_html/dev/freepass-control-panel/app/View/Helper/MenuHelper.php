<?php
App::uses('AppHelper', 'View/Helper');

class MenuHelper extends AppHelper
{
    public $helpers = array("Html");

    /**
     * Cria um item do menu
     * @param string $label Nome que será exibido no link
     * @param array $url Array usado no $this->Html->link
     * @param array $active Array com os nomes dos controllers em que a class active será acionada
     * @param array $visible Array com os tipos de usuário que verão este item
     * @return Conteudo do item em HTML
     */
    public function item($label, $url, $active = null,$visible = array())
    {
        if (!empty($visible) & !in_array(AuthComponent::user("type"),$visible)) {
            return;
        }

        $params = array();

        if ($this->params['controller'] == $active) {
            $params["class"] = "active";
        }

        if (!isset($url["plugin"])) {
            $url["plugin"] = false;
        }

        $link = $this->Html->link($label, $url);

        return $this->Html->tag("li", $link, $params);
    }

    /**
     * Cria um divisor no menu
     * @return Conteudo do divisor em HTML
     */
    public function divider()
    {
        return $this->Html->tag("li","",array("class"=>"divider"));
    }

    /**
     * Cria um dropdown do para ser usado no menu
     * @param string $label Nome que será exibido no dropdown
     * @param array $active Array com os nomes dos controllers em que a class active será acionada
     * @param callback $content Função de callback para renderizar os items do dropdown
     * @param array $visible Array com os tipos de usuário que verão este item
     * @return string Conteudo do dropdown em HTML
     */
    public function dropdown($label,$active,$content,$visible = array())
    {
        if (!empty($visible) & !in_array(AuthComponent::user("type"),$visible)) {
            return;
        }

        $options["class"] = "dropdown";

        if (in_array($this->params['controller'],$active)) {
            $options["class"] .= " active";
        }

        $a = $this->Html->link($label . " " . $this->Html->tag("b", "", array("class" => "caret")), "#", array("class" => "dropdown-toggle", "data-toggle" => "dropdown", "escape" => false));

        $ul = $this->Html->tag("ul", $content(), array('class' => 'dropdown-menu', 'role' => 'menu'));

        return $this->Html->tag("li", $a . $ul, $options);
    }

    /**
     * Cria os dados no menu baseado em anotations feitas nos controllers
     */
    public function autoGenerate()
    {
        $classes = array();

        foreach(App::objects('controllers') as $ctlr) {
            $classes["App"][$ctlr] = array();
        }

        foreach(App::objects('plugins') as $plugin) {
             foreach(App::objects("$plugin.controllers") as $ctlr) {
                $classes[$plugin][$ctlr] = array();
             }
        }

        App::import('Vendor', 'Annotations/AnnotationParser');

        foreach($classes as $plugin => $controllers) {
            foreach($controllers as $ctrl => $data) {
                if ($plugin=="App") {
                    App::uses($ctrl,'Controller');
                } else {
                    App::uses("$ctrl","$plugin.Controller");
                }

                $class= new ReflectionClass($ctrl);
                $class->getDocComment();
                $annotations = AnnotationParser::getAnnotations($class->getDocComment());

                if(isset($annotations["menu"])) {
                    $classes[$plugin][$ctrl]["menu"] = $annotations["menu"];
                }
            }
        }
    }
}
