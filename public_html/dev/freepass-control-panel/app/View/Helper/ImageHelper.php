<?php
App::uses('AppHelper', 'View/Helper');
App::uses('Image', 'Lib');

class ImageHelper extends AppHelper
{
    public $helpers = array('Html');
    public $cacheDir = 'imagecache';
    public $expires = '+1 year';
    public $Image = null;

    public function __construct(\View $View, $settings = array())
    {
        parent::__construct($View, $settings);
        $this->Image = new Image();
    }
    
    /**
     * Resize and crop images
     *
     * @param string $path
     * @param int $width
     * @param int $height
     * @param bool $aspect
     * @param bool $cut
     * @param bool $returnPath
     *
     * @return bool|string
     */
    public function resize($path, $width, $height, $aspect = true, $cut = false, $returnPath = false)
    {
        return $this->Image->resize($path, $width, $height, $aspect, $cut, $returnPath);
    }

    public function clearCache($force = null)
    {
        return $this->Image->clearCache($force);
    }

    public function delTree($dir)
    {
        return $this->Image->delTree($dir);
    }

    public function fullPath()
    {
        return $this->Image->fullPath();
    }

    public function relFile($path, $hash)
    {
        return $this->Image->relFile($path, $hash);
    }

    public function cacheFile($path, $hash)
    {
        return $this->Image->cacheFile($path, $hash);
    }

    public function _resize($width, $height, $toWidth, $toHeight, $aspect = true, $cut = false)
    {
        return $this->Image->_resize($width, $height, $toWidth, $toHeight, $aspect, $cut);
    }
}