<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true)); ?>
<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">Geral</div>
            <div class="panel-body">
				<?php
					echo $this->BootstrapForm->input('name', array('label' => User::$labels['name']));
					echo $this->BootstrapForm->input('email', array('label' => User::$labels['email']));
					echo $this->BootstrapForm->input('street', array('label' => User::$labels['street']));
					echo $this->BootstrapForm->input('city', array('label' => User::$labels['city']));
					echo $this->BootstrapForm->input('state', array('label' => User::$labels['state']));
					echo $this->BootstrapForm->input('document', array('label' => User::$labels['document']));
					echo $this->BootstrapForm->input('gender', array('label' => User::$labels['gender']));
					echo $this->BootstrapForm->input('instagram', array('label' => User::$labels['instagram']));
					echo $this->BootstrapForm->input('birthday', array('type'=> 'text', 'class' => 'format-date', 'label' => User::$labels['birthday']));

					echo $this->BootstrapForm->input('password', array('label' => User::$labels['password']));
					echo $this->BootstrapForm->input('c_password', array('label' => User::$labels['c_password'], 'type' => 'password'));

					// echo $this->BootstrapForm->input('active', array('label' => User::$labels['active']));
					// echo $this->BootstrapForm->input('type', array('label' => User::$labels['type']));
					// echo $this->BootstrapForm->input('hash', array('label' => User::$labels['hash']));

					// echo $this->BootstrapForm->input('ingresse_id', array('label' => User::$labels['ingresse_id']));
					// echo $this->BootstrapForm->input('ingresse_token', array('label' => User::$labels['ingresse_token']));
				?>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('../Common/form_footer'); ?>
<?php echo $this->Form->end(); ?>
