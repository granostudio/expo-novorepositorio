<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true)); ?>
<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

<div class="row">
	<div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">Quantidade de convites que este usuário pode enviar</div>
            <div class="panel-body">
				<?php echo $this->BootstrapForm->input('invite_count', array('label' => User::$labels['invite_count'])); ?>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('../Common/form_footer'); ?>
<?php echo $this->Form->end(); ?>