<?php
$vars = sprintf("var URL_BASE = '%s';", Router::url("/", true));
$vars .= sprintf("var CONTROLLER = '%s';", $this->params->controller);


echo $this->Html->scriptBlock($vars);

$jsList = array(
	'/theme/admin/js/vendor/jquery-1.11.1.min',
	'/theme/admin/js/vendor/bootstrap.min'
);

if (isset($this->viewVars['requestJs']))
	$jsList[] = $this->viewVars['requestJs'];

// Script para a view aual
if (file_exists(WWW_ROOT . "theme/admin/js/". CURRENT_VIEW . '.js'))
    $jsList[] = "/theme/admin/js/". CURRENT_VIEW . '.js';


echo $this->fetch('script');
echo $this->Html->script($jsList);
?>
