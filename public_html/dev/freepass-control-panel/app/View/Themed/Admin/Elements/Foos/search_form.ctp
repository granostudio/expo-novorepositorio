<?php $this->extend('/Common/search_form'); ?>

<?php echo $this->BootstrapForm->create(false, array('type' => 'get', 'novalidate' => true)); ?>

<?php echo $this->BootstrapForm->input('name'); ?>
<?php echo $this->BootstrapForm->input('type', array("type" => "select", "options" => $model::$types, "empty" => "Selecione")); ?>

<!-- Pesquisa por campo relacionado, obrigatório passar um label e prefixar com o nome do model--> 
<?php echo $this->BootstrapForm->input('bar_name', array("label" => Bar::$labels["name"])); ?>

<!-- Pesquisa por chave estrangeira -->
<?php echo $this->BootstrapForm->input('bar_id', array("type" => "select", "options" => $bars, "empty" => "Selecione", "label" => "Bar ( Foo.bar_id )")); ?>

<div>
    <?php echo $this->Form->button('Limpar', array('class' => 'btn btn-default pull-left btn-clear-form', 'type' => 'button')); ?>
    <?php echo $this->Form->submit('Procurar', array('class' => 'btn btn-primary pull-right')); ?>
</div>

<?php echo $this->Form->end(); ?>
