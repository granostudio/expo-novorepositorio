<!DOCTYPE html>
<html>
    <head>
        
        <?php echo $this->element('Layout/login/head'); ?>
        
    </head>
    
    <body>
        
        <?php echo $this->element('Layout/login/version'); ?>                                                                                                                                                                                                                                                               
        <?php echo $this->element('Layout/login/js'); ?>
        
        <div class="container-fluid">
            
            
            <?php echo $this->fetch('content'); ?>

            
        </div>
        
        <?php echo $this->element('Layout/login/js'); ?>
        <?php echo $this->element('sql_dump'); ?>
        
    </body>
    
</html>
