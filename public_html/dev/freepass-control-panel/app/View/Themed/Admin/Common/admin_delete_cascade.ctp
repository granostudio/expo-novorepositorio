<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Breadcrumb->drop(Inflector::pluralize($model::$label), array("action" => "index"))
                ->drop("Excluir Tudo")
                ->create();
        ?>
    </div>
</div>

<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true,"url"=>array("action" => "delete_cascade", $id))); ?>
<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden','value'=>$id)); ?>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            <p>
                <strong>Atenção!</strong>
            </p>
            <p>
                Não foi possível excluir este item pois existem itens atrelados a ele.
            </p>
            <p>
                A exclusão irá excluir também os seguintes items:
            </p>
            <?php echo $this->Html->nestedList($deltree);?>
        </div>
    </div>
</div>

<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    <div class="container-fluid">

        <?php echo $this->Form->button('<span class="glyphicon glyphicon-trash"></span> Excluir tudo', array('class' => 'btn btn-danger pull-left navbar-btn action-delete-cascade', 'escape' => false)); ?>

    </div>
</nav>


<?php echo $this->Form->end(); ?>