<html>
<head>
<style type="text/css">
  #container {
    max-width: 800px;
    height: 800px;
    margin: auto;
	border: 1px solid #333;
  }
</style>
</head>
<body>
<div id="container"></div>
<?php echo $this->Html->script(array('vendor/sigma.min', 'vendor/plugins/sigma.parsers.json.min')); ?>
<script>
  sigma.parsers.json("<?php echo Router::url('/js/flare.json', true); ?>", {
    container: 'container',
    settings: {
      defaultNodeColor: '#ec5148'
    }
  });
</script>
</body>
</html>