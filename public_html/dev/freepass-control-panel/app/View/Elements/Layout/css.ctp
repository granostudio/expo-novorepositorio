<?php
if ($this->params['action'] != 'display'):
	define('CURRENT_VIEW', $this->params['controller'] . '/' . $this->params['action']);
else:
	define('CURRENT_VIEW', $this->params['controller'] . '/' . $this->params['pass'][0]);
endif;

$cssList = array('default', 'bootstrap.min', 'animate.min');

if (isset($this->viewVars['requestCss'])):
	$cssList[] = $this->viewVars['requestCss'];
endif;

if (file_exists(CSS . CURRENT_VIEW. '.css')):
	$cssList[] = CURRENT_VIEW . '.css';
endif;

echo $this->ScriptCombiner->css($cssList);
echo $this->fetch('meta');
echo $this->fetch('css');
?>