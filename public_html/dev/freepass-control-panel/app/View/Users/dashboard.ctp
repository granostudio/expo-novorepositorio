<div class="row">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<h1 class="title">Convidados</h1>

			<p class="convites-restantes">Convites restantes: <?php echo $remaining; ?></p>
		
			<?php if ($remaining): ?>
				<p class="adicionar"><?php echo $this->Html->link('Adicionar', array('controller' => 'invites', 'action' => 'add'), array('title' => 'Adicionar convite', 'class' => 'btn btn-oscar')); ?></p>
			<?php endif; ?>

			<table class="table table-bordered">
				<?php if (empty($user['Invite'])): ?>
					<tr>
						<td colspan=2>Convide seus amigos!</td>
					</tr>
				<?php endif; ?>
				<?php foreach($user['Invite'] as $invite): ?>
					<tr>
						<td class="table-row"><?php echo $invite['email']; ?></td>
						<td class="table-row">
							<?php if ($invite['register'] == false): ?>
								<?php echo $this->Html->link('<i class="glyphicon glyphicon-refresh"></i>', array('controller' => 'invites', 'action' => 'refresh', $invite['id']), array('title' => 'Reenviar convite', 'class' => 'label label-primary btn-sm', 'escape' => false)); ?>
								<?php //echo $this->Html->link('x', array('controller' => 'invites', 'action' => 'cancel', $invite['id']), array('title' => 'Cancelar convite', 'class' => 'label label-danger btn-sm')); ?>
							<?php else: ?>
								<i class="label label-primary">Convite aceito</i>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>