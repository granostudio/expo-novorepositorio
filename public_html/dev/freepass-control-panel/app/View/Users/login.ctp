<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="panel panel-default animated <?php if(!$this->request->isPost()) echo "bounceInUp" ?>">
            <div class="panel-heading">Login</div>
            <div class="panel-body">
                <?php echo $this->BootstrapForm->create($model, array('type' => 'post', 'novalidate' => true)); ?>

                <?php echo $this->BootstrapForm->input('email'); ?>
                <?php echo $this->BootstrapForm->input('password'); ?>

                <?php echo $this->Form->button('<span class="glyphicon glyphicon-ok"></span>  Entrar', array('class' => 'btn btn-success pull-left navbar-btn')); ?>

                <?php echo $this->Form->end(); ?>

				<?php if (CakeSession::check('Auth.User.id') == false): ?>
					<p></p>
				<?php endif; ?>
            </div>
        </div>
        <?php echo $this->Session->flash(); ?>
    </div>
    <div class="col-md-4"></div>
</div>