<?php echo $this->BootstrapForm->create($model, array('type' => 'post', 'novalidate' => false)); ?>
<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

<div class="row">
    <div class="container">
    	<div class="col-md-10 col-md-offset-1">
    		<h1 class="title">Adicionar convite</h1>	

    		<div class="input-group">
			<?php echo $this->Form->input('email', array('type' => 'email', 'label' => false, 'div' => false, 'class' => 'form-control', 'aria-describedby' => 'basic-addon2', 'placeholder' => 'Preencha seu e-mail')); ?>
				<span class="input-group-btn">
					<?php echo $this->Form->button('<span class="glyphicon glyphicon-ok"></span>  Salvar', array('class' => 'btn btn-success pull-right')); ?>
				</span>
			</div>
	    </div>
    </div>	
</div>

<?php echo $this->Form->end(); ?>