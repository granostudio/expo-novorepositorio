<?php
$config['ScriptCombiner']['combineCss'] = true;
$config['ScriptCombiner']['combineJs'] = true;
$config['ScriptCombiner']['compressCss'] = true;
$config['ScriptCombiner']['compressJs'] = true;
$config['ScriptCombiner']['cacheLength'] = 3600*24;
$config['ScriptCombiner']['cssCachePath'] = WWW_ROOT . 'cache' . DS . 'css' . DS;
$config['ScriptCombiner']['jsCachePath'] = WWW_ROOT . 'cache' . DS . 'js' . DS;
$config['ScriptCombiner']['fileSeparator'] = "\n\n/** FILE SEPARATOR **/\n\n";
$config['ScriptCombiner']['key'] = '39DSHFJK89U23IUEF893';
