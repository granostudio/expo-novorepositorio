<?php
App::uses('Controller', 'Controller');
App::uses('SessionList', 'Lib');
App::uses('Analytics', 'Lib');

class AppController extends Controller
{
    public $model = null;
    public $helpers = array('Html', 'Form', 'Session', 'Image', 'Text', 'Breadcrumb', 'BootstrapForm', 'ScriptCombiner', 'Language');
    public $components = array(
        'RequestHandler',
        'Session',
        'Search',
        'Tools',
        'Auth' => array(
            'autoRedirect' => true,
            'loginRedirect' => array('admin' => false, 'controller' => 'users', 'action' => 'dashboard'),
            'logoutRedirect' => array('admin' => false, 'controller' => 'pages', 'action' => 'display', 'home'),
            'fields' => array(
                'username' => 'email',
                'password' => 'password'
            ),
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'User',
                    'fields' => array(
                        'username' => 'email',
                        'password' => 'password'
                    ),
                    //'scope' => array('inactive' => false)
                ),
            ),
        )
    );

    /**
     * Grid field array used on GridHelper
     *
     * @var array
     */
    public $grid = array();

    public function beforeFilter()
    {
        $this->_setAuth();

        ClassRegistry::init("Config")->load();

        $this->Tools->run($this->request);

        $this->model = Inflector::classify($this->params['controller']);
        $this->plugin = Inflector::classify($this->params['plugin']);
        $this->set("model", $this->model);
        $this->set("plugin", $this->plugin);

        if ($this->request->is('ajax')) {
            $this->layout = "ajax";
        }
    }

    private function _setAuth()
    {
        if ($this->_isAdmin()) {

            AuthComponent::$sessionKey = 'Auth.Operator';

            $this->Auth->autoRedirect = false;
            $this->Auth->loginAction = array('admin' => true, 'controller' => 'operators', 'action' => 'login');
            $this->Auth->loginRedirect = array('admin' => true, 'controller' => 'home', 'action' => 'index');
            $this->Auth->logoutRedirect = array('admin' => true, 'controller' => 'operators', 'action' => 'login');

            $this->Auth->authenticate = array(
                'Form' => array(
                    'userModel' => 'Operator',
                    'fields' => array(
                        'username' => 'email',
                        'password' => 'password'
                    ),
                    'scope' => array('active' => true)
                )
            );

            $this->theme = 'admin';
        } else {
            AuthComponent::$sessionKey = 'Auth.User';
            $this->Auth->allow('login', 'logout', 'display', 'api', 'revision');
        }
    }

    protected function _isAdmin()
    {
        return isset($this->params['admin']) && $this->params['admin'];
    }

    /**
     * Show message
     *
     * @param array $redirect
     * @param string $element
     * @param string $msg
     */
    public function showMessage($redirect = null, $element = '', $msg = '')
    {
        $this->Session->setFlash($msg, $element);
        if ($redirect) {
            $this->redirect($redirect);
        }
    }

    /**
     * Show a error flash message
     *
     * @param array $redirect
     * @param string $msg
     */
    public function showError($redirect = null, $msg = "Falha ao realizar a ultima operação")
    {
        $this->showMessage($redirect, 'error_message', $msg);
    }

    /**
     * Show a success flash message
     *
     * @param array $redirect
     * @param string $msg
     */
    public function showSuccess($redirect, $msg = "Operação realizada com sucesso")
    {
        $this->showMessage($redirect, 'success_message', $msg);
    }

    //##############################
    //# CRUD                       #
    //##############################

    /**
     * Exclui o modelo do banco de dados e remove todos os outros modelos relacionados
     *
     * @param int $id
     */
    public function admin_delete_cascade($id)
    {
        $model = $this->model;
        $this->$model->id = $id;

        $this->set("deltree", $this->$model->getDependents());

        $this->set("id", $id);

        if (!$this->$model->exists()) {
            $this->showError(array('action' => 'index'), 'O item solicitado nao foi encontrado!');
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            $this->$model->detachModels($id);

            if ($this->$model->delete($id, true)) {
                $this->showSuccess(array('action' => 'index'));
            }
        }

        $this->render("/Common/admin_delete_cascade");
    }

    /**
     * Exclui o modelo do banco caso de erro (Geralmente erro de relacionamento) a action é
     * redirecionada para o delete cascade.
     *
     * @param int $id
     */
    public function admin_delete($id = null)
    {
        $model = $this->model;
        $this->$model->id = $id;

        if (!$this->$model->exists()) {
            $this->showError(array('action' => 'index'), 'O item solicitado nao foi encontrado!');
        }

        try {
            if ($this->$model->delete($id, false)) {
                $this->showSuccess(array('action' => 'index'));
            }
        } catch (Exception $e) {

            ClassRegistry::init("Operator");

            if ($this->Auth->user("type") == Operator::TYPE_ADMINISTRATOR) {
                $this->redirect(array('action' => 'delete_cascade', $id));
            } else {
                $this->showError(array('action' => 'index'), 'Falha ao excluir o item!');
            }
        }
    }

    /**
     * Exclui o modelo via ajax
     *
     * @param type $id
     */
    public function admin_ajax_delete($id = null)
    {
        $model = $this->model;
        $this->$model->id = $id;

        if (!$this->$model->exists()) {
            $response = array("result" => false, "error" => 'O item solicitado nao foi encontrado!');
        }

        if ($this->$model->delete($id, false)) {
            $response = array("result" => true);
        } else {
            $response = array("result" => false, "error" => 'Não foi possível excluir este item!');
        }

        return new CakeResponse(array('body' => json_encode($response), 'type' => 'json', 'status' => 200));
    }

    /**
     * Exclui JOE POGUEIRO COLOCA ESSA POHA DIREITO
     */
    public function admin_delete_selection()
    {
        $model = $this->model;
        $ids = SessionList::read($this->params->controller . "_selected_items");

        try {
            if ($this->$model->deleteAll(array($this->model . ".id" => $ids), false, true)) {
                SessionList::pop($this->params->controller . "_selected_items", $ids);
                $this->showSuccess(array('action' => 'index'));
            }
        } catch (Exception $e) {
            ClassRegistry::init("Operator");
            if ($this->Auth->user("type") == Operator::TYPE_ADMINISTRATOR) {
                $this->redirect(array('action' => 'delete_selection_cascade'));
            } else {
                $this->showError(array('action' => 'index'), 'O item solicitado nao foi encontrado!');
            }
        }
    }

	/**
	 * Delete cascade with selections
	 *
	 * @return void
	 */
	public function admin_delete_selection_cascade()
	{
		$model = $this->model;

        $ids = SessionList::read($this->params->controller . "_selected_items");
		$this->$model->id = current($ids);
        $this->set("deltree", $this->$model->getDependents());

        $this->set("id", current($ids));
        if ($this->request->is('post') || $this->request->is('put')) {
			$db = ConnectionManager::getDataSource('default');
			$db->rawQuery("SET foreign_key_checks = 0");

			try {
				if ($this->$model->deleteAll(array($this->model . ".id" => $ids), true)) {
					$db->rawQuery("SET foreign_key_checks = 1");
					$this->showSuccess(array('action' => 'index'));
				}
			} catch (Exception $e) {
				$db->rawQuery("SET foreign_key_checks = 1");
			}
        }
        $this->render("/Common/admin_delete_selection_cascade");
	}

    /**
     * Default add action
     */
    public function admin_add($action = 'index')
    {
        if ($this->request->is('post')) {

            $model = $this->model;

            if ($this->$model->save($this->request->data)) {
                $id = null;
                if ($action == 'edit') {
                    $id = $this->$model->id;
                }
                $this->showSuccess(array("action" => $action, $id));
            } else {
                $this->showError();
            }
        }
    }

    /**
     * Default ajax add action
     */
    public function admin_ajax_add($parent_id)
    {
        $this->set("parent_id", $parent_id);

        if ($this->request->is('post')) {

            $model = $this->model;

            if ($this->$model->save($this->request->data)) {
                $response = array("result" => true);
            } else {
                $response = array("result" => false, "errors" => $this->$model->validationErrors);
            }

            return new CakeResponse(array('body' => json_encode($response), 'type' => 'json', 'status' => 200));
        }
    }

    /**
     * Default edit action
     *
     * @param int $id
     */
    public function admin_edit($id = null)
    {
        $model = $this->model;
        $this->$model->id = $id;

        if (!$this->$model->exists()) {
            $this->showError(array("action" => "index"));
        }

        if ($this->request->is('get')) {
            $this->request->data = $this->$model->read();
        } else {

            if ($this->$model->save($this->request->data)) {
                $this->showSuccess(array("action" => "index"));
            } else {
                $this->showError();
            }
        }
    }

    /**
     * Default view action
     *
     * @param int $id
     */
    public function admin_view($id)
    {
        $model = $this->model;

        $this->$model->id = $id;

        if (!$this->$model->exists()) {
            $this->showError(array("controller" => "home"));
        }

        $this->request->data = $this->$model->read();

        $this->render("/Common/admin_view");
    }

    /**
     * Exibi view para seleção dos campos qque podem ser exportados
     */
    public function admin_export_fields()
    {
        $this->set('grid', $this->grid);
        $this->render("/Common/admin_export_fields");
    }

    public function admin_list_push()
    {
        $data = $this->request->data;
        SessionList::push($data["key"], $data["value"]);
        $result = array("result" => true);

        return new CakeResponse(array('body' => json_encode($result), 'type' => 'json', 'status' => 200));
    }

    public function admin_list_pop()
    {
        $data = $this->request->data;
        SessionList::pop($data["key"], $data["value"]);
        $result = array("result" => true);

        return new CakeResponse(array('body' => json_encode($result), 'type' => 'json', 'status' => 200));
    }

    public function admin_list_count()
    {
        $data = $this->request->data;
        $result = array("result" => true, "count" => SessionList::count($data["key"]));

        return new CakeResponse(array('body' => json_encode($result), 'type' => 'json', 'status' => 200));
    }

    /**
     * Retorna dados em ajax para o autocompleteBelongsTo do BootstrapFormHelper
     *
     * @return \CakeResponse
     */
    public function admin_autocompleteBelongsTo($getFieldText = false)
    {
        if ($getFieldText) {
            return $this->_autocompleteGetFieldText();
        }

        $result = $this->_autocompleteGetList($this->request->data);

        return new CakeResponse(array('body' => json_encode(self::_convertCakeList($result)), 'type' => 'json', 'status' => 200));
    }

    /**
     * Rotina auxiliar do metodo admin_autocompleteBelongsTo
     *
     * @return \CakeResponse
     */
    private function _autocompleteGetFieldText()
    {
        $data = $this->request->data;

        $model = $data["model"];
        $field = $data["field"];
        $id = $data["id"];

        $result = ClassRegistry::init($model)->findById($id, array($field));

        $response = array("result" => true, "value" => $result[$model][$field]);

        return new CakeResponse(array('body' => json_encode($response), 'type' => 'json', 'status' => 200));
    }

    /**
     * Retorna dados em ajax para o autocomplete do BootstrapFormHelper
     *
     * @return \CakeResponse
     */
    public function admin_autocomplete()
    {
        $result = $this->_autocompleteGetList($this->request->data);

        return new CakeResponse(array('body' => json_encode(self::_convertCakeList($result)), 'type' => 'json', 'status' => 200));
    }

	/**
	 * Retorna a lista para o autocomplete
	 *
	 * @param array $data - $this->request->data
	 * @return array
	 */
	public function _autocompleteGetList($data)
	{
        $model = $data["model"];
        $field = $data["field"];

        $default_conditions = array(
            $model . "." . $field . " LIKE" => "%" . $data["value"] . "%"
        );

        $conditions = unserialize(base64_decode($data["conditions"]));

        if (!is_array($conditions)) {
            $conditions = array();
        }

        $conditions = array_merge($default_conditions, $conditions);

        $options = array(
            'limit' => 20,
            'conditions' => $conditions
        );


        $obj = ClassRegistry::init($model);

        if (isset($data["displayField"])) {
            $obj->displayField = $data["displayField"];
        }

        return $obj->find('list', $options);
	}

    /**
     * Conta Registros
     *
     * @return \CakeResponse
     */
    public function admin_count()
    {
        $data = $this->request->data;

        $model = $data["model"];

        $conditions = unserialize(base64_decode($data["conditions"]));

        if (!is_array($conditions)) {
            $conditions = array();
        }

        $options = array(
            'conditions' => $conditions
        );

        $count = ClassRegistry::init($model)->find('count', $options);

        $response = array("result" => true, "count" => $count);

        return new CakeResponse(array('body' => json_encode($response), 'type' => 'json', 'status' => 200));
    }

    /**
     * Converte um array $key => $value para id => key , name => value
     *
     * @param array $items
     * @return type
     */
    public static function _convertCakeList($items)
    {
        $result = array();
        foreach ($items as $key => $value) {
            $result[] = array("id" => $key, "name" => $value);
        }

        return $result;
    }

    public function revision()
    {
		if ($this->request->is('post')) {
			$revision = file_get_contents(ROOT . '/.revision');
	        $revision = trim($revision);

	        App::uses('File', 'Utility');

	        $scriptCombiner = new File(ROOT . DS . 'app' . DS . 'Config' . DS . 'script_combiner.php', true);
	        $content = $scriptCombiner->read();

	        $content = preg_replace('/(\$config\[\'ScriptCombiner\'\]\[\'key\'] \= \'([^\']*)\'\;)/', "\$config['ScriptCombiner']['key'] = '{$revision}';", $content);

	        $scriptCombiner->write($content);
	        $scriptCombiner->close();
		}

        $this->autoRender = false;
    }
	
	/**
	 * Reordena os registros
	 *
	 * @return void
	 */
	public function admin_sort()
	{
		$this->autoRender = false;
		
		$ids = $this->request->data['ids'];
		$modelClass = $this->request->data['model'];
		
		$this->modelClass = ClassRegistry::init($modelClass);
		
		foreach ($ids as $order => $id):
			$this->modelClass->id = $id;
			$this->modelClass->saveField("order", $order + 1);
		endforeach;
		
		return true;
	}

    /**
     * Renew user session
     *
     * @param int $id
     * @return void
     */
    public function _renewUserSession($id=false)
	{
        if ($id) {
            $userId = $id;
        } else {
            $userId = CakeSession::read('Auth.User.id');
        }
        
        return $this->Session->write('Auth', ClassRegistry::init('User')->getUser($userId));
    }

    /**
     * Get user by token
     * 
     * @return void
     */
    public function _restTokenAPI()
	{
        if (isset($this->request->params['ext']) && $this->request->params['ext'] == 'json') {
            if (!$this->request->header('token') || is_null($this->request->header('token'))) {
                $this->response->type('json');
                $this->response->statusCode(400);
                $this->response->body(json_encode(array('status' => 'error', 'message' => 'Unauthorized!')));
                $this->response->send();
                $this->_stop();
            }
            
            $token = $this->request->header('token');
            $user = ClassRegistry::init('User')->findByToken($this->request->header('token'));
            if ($user && !is_null($user['User']['token'])) {
                $this->_renewUserSession($user['User']['id']);
            }
        }
    }
}