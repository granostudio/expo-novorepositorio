<?php

class ToolsComponent extends Component
{

    private static $commands = array(
        "tools_help" => "Exibe lista de commandos",
        "tools_set_debug" => "Seta o debug do cake",
        "tools_get_revision" => "Mostra a revisao do git (Obs: Arquivo .revision gerado pelo beanstalk)",
        "tools_get_file_perm" => "Mostra as permissoes para um determinado arquivo",
        "tools_list_cache" => "Lista os arquivos de cache",
        "tools_clear_cache" => "Excluir os arquivos de cache",
        "tools_get_db_config" => "Exibe configuração do banco de dados (Obs: require password)"
    );

    public static function run(CakeRequest $request)
    {

        foreach (self::$commands as $cmd => $desc) {
            if (isset($request->query[$cmd])) {
                header('Content-Type: text/html; charset=utf-8');
                self::$cmd($request->query[$cmd]);
            }
        }
    }

    private static function tools_help($value)
    {
        foreach (self::$commands as $cmd => $desc) {
            echo "<p><strong>$cmd</strong> - $desc<br>";
        }

        return;
    }

    private static function tools_set_debug($value)
    {
        Configure::write("debug", $value);
    }

    private static function tools_get_revision($value)
    {
        echo file_get_contents("../../.revision");

        return;
    }

    private static function tools_get_file_perm($value)
    {
        echo substr(sprintf('%o', fileperms($value)), -4);

        return;
    }

    private static function tools_list_cache($value)
    {
        $paths = array('../tmp/cache/models/*', '../tmp/cache/persistent/*', '../tmp/cache/views/*');

        foreach ($paths as $path) {
            $files = glob($path);

            foreach ($files as $file) {
                if (is_file($file)) {
                    echo $file . "</br>";
                }
            }
        }

        return;
    }

    private static function tools_clear_cache($value)
    {
        $paths = array('../tmp/cache/models/*', '../tmp/cache/persistent/*', '../tmp/cache/views/*');

        foreach ($paths as $path) {
            $files = glob($path);
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        }

        echo "Arquivos de cache limpos com sucesso";

        return;
    }

    private static function tools_get_db_config($value)
    {
        if (isset($_GET["password"]) && $_GET["password"] == "[mkt123]") {
            App::uses('ConnectionManager', 'Model');
            $dataSource = ConnectionManager::getDataSource('default');
            var_dump($dataSource->config);
        } else {
            echo "Acesso negado";
        }

        return;
    }
}
