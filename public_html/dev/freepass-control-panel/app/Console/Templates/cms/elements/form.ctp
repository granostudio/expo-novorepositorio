<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$tempModel = new Model(array('name' => $modelClass, 'table' => Inflector::tableize($modelClass), 'ds' => 'default'));
$fieldsSchema = $tempModel->schema(true);
$fileField = array();
foreach ($fieldsSchema as $key => $fil) :
	if (array_key_exists('comment', $fil) && $fil['comment'] == 'upload') {
		$fileField[] = $key;
	}
endforeach;
?>
<?php echo "<?php echo \$this->BootstrapForm->create(\$model, array('type' => 'file', 'novalidate' => true)); ?>\n"; ?>
<?php echo "<?php echo \$this->BootstrapForm->input('id', array('type' => 'hidden')); ?>\n"; ?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">Geral</div>
            <div class="panel-body">
				<?php 
				echo "<?php\n";
				foreach ($fields as $field) :
					if ($field == 'id') {
						continue;
					}
					if (strpos($action, 'add') !== false && $field === $primaryKey) {
						continue;
					} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
						if (in_array($field, $fileField)) {
							echo "\t\t\t\t\techo \$this->BootstrapForm->input('{$field}', array('type' => 'file'));\n";
						} else {
							echo "\t\t\t\t\techo \$this->BootstrapForm->input('{$field}', array('label' => {$modelClass}::\$labels['{$field}']));\n";
						}
					}
				endforeach; 
				?>
				<?php
				/*
				if (!empty($associations['hasAndBelongsToMany'])) {
					foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
						echo "\t\techo \$this->Form->input('{$assocName}');\n";
					}
				}
				*/
				echo "?>\n";
				?>
            </div>
        </div>
    </div>
</div>

<?php echo "<?php echo \$this->element('../Common/form_footer'); ?>"; ?>
<?php echo "<?php echo \$this->Form->end(); ?>"; ?>