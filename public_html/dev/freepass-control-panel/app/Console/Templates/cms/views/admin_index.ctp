<div class="row">
    <div class="col-md-12">
        <?php
        echo "<?php echo \$this->Breadcrumb->drop(Inflector::pluralize(\$model::\$label), array('action' => 'index'))
                 ->create(\$this->Html->link('Adicionar Novo', array('action' => 'add'))); ?>";
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <?php echo "<?php echo \$this->element(sprintf('%s/search_form', Inflector::pluralize(\$model))) ?>" ?>
    </div>

    <div class="col-md-9">
        <?php echo "<?php echo \$this->Grid->create(Inflector::pluralize(\$model::\$label), \$grid, \$result); ?>"; ?>
    </div>
</div>