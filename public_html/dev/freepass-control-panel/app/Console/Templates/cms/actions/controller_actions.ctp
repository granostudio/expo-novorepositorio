<?php
/**
 * Bake Template for Controller action generation.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.actions
 * @since         CakePHP(tm) v 1.3
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<?php $compact = array(); ?>
	/**
	 * <?php echo $admin ?>add method
	 *
	 * @return void
	 */
	public function admin_add($action = 'index') 
	{
		parent::admin_add();
<?php
	foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
		foreach ($modelObj->{$assoc} as $associationName => $relation):
			if (!empty($associationName)):
				$otherModelName = $this->_modelName($associationName);
				$otherPluralName = $this->_pluralName($associationName);
				echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
				$compact[] = "'{$otherPluralName}'";
			endif;
		endforeach;
	endforeach;
	if (!empty($compact)):
		echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
	endif;
?>
	}

<?php $compact = array(); ?>
	/**
	 * <?php echo $admin ?>edit method
	 *
	 * @throws NotFoundException
	 * @param string $id|int
	 * @return mixed
	 */
	public function admin_edit($id = null) 
	{
		parent::admin_edit($id);
<?php
		foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
			foreach ($modelObj->{$assoc} as $associationName => $relation):
				if (!empty($associationName)):
					$otherModelName = $this->_modelName($associationName);
					$otherPluralName = $this->_pluralName($associationName);
					echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
					$compact[] = "'{$otherPluralName}'";
				endif;
			endforeach;
		endforeach;
		if (!empty($compact)):
			echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
		endif;
	?>
	}
	
	/**
	 * <?php echo $admin ?>index method
	 *
	 * @return void
	 */
	public function admin_index() 
	{
		$this->set('grid', $this->grid);

		$options = array(
<?php
$fields = $modelObj->schema(true);
foreach ($fields as $key => $field) {
	if (array_key_exists('comment', $field) && $field['comment'] != 'upload') {
		$comment = json_decode($field['comment'], true);
		if (!empty($comment) && array_key_exists('search', $comment)):
			echo "\t\t\t'{$key}' => array('operation' => 'contain'),\n";
		endif;
	}
}
?>
		);
		
<?php 
$compact = array();
foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
	foreach ($modelObj->{$assoc} as $associationName => $relation):
		if (!empty($associationName)):
			$otherModelName = $this->_modelName($associationName);
			$otherPluralName = $this->_pluralName($associationName);
			$displayField = $modelObj->{$associationName}->displayField;
			$compact[] = "\t\t\t'{$otherModelName}' => array('fields' => '{$otherModelName}.{$displayField}')\n";
		endif;
	endforeach;
endforeach;
if (!empty($compact)):
	echo "\t\t\$this->paginate['contain'] = array(\n" . join(', ', $compact) . "\t\t);\n";
endif;
?>
		
		$this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
		$this->paginate['order'] = SearchComponent::createOrder($this->request, array('<?php echo $currentModelName; ?>.id' => 'asc'));

		if (!isset($this->params->named["export"])) {
			$this->set('result', $this->paginate($this->model));
		} else {
			$this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
			$this->render("/Common/admin_export");
		}
<?php
$compact = array();
		foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
			foreach ($modelObj->{$assoc} as $associationName => $relation):
				if (!empty($associationName)):
					$otherModelName = $this->_modelName($associationName);
					$otherPluralName = $this->_pluralName($associationName);
					echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
					$compact[] = "'{$otherPluralName}'";
				endif;
			endforeach;
		endforeach;
		if (!empty($compact)):
			echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
		endif;
	?>
	}