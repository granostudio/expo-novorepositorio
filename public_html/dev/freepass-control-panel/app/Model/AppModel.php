<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');
App::uses('AuthComponent', 'Controller/Component');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model
{
    public $recursive = -1;
    public $actsAs = array('Logable', 'Convertable', 'Containable');

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);

        // Evita problemas com alias no virtual field
        // Caso nao esteja usando alias nos models
        // pode desebilitar essa rotina
        foreach ($this->virtualFields as $key => $value) {
            $this->virtualFields[$key] = str_replace($this->name . ".", $this->alias . ".", $value);
        }
    }

    /**
     * Send email via queue cron
     *
     * @param array $settings
     * @return void
     */
    public function _queueEmail($settings)
    {
        $default = array(
            'to' => '',
            'subject' => '',
            'template' => '',
            'data' => array()
        );
        $settings = Set::merge($default, $settings);
        ClassRegistry::init('Queue.QueuedTask')->createJob('AdvancedEmail', $settings);
    }
    
    /**
     * Validação condicional
     *
     * @param type $data
     * @param type $to_check_field
     * @param type $condition
     * @param type $value
     * @return boolean
     */
    public function notEmptyIfEx($data, $to_check_field, $condition, $value)
    {

        $keys = array_keys($data);
        $field = $keys[0];

        $other = $this->data[$this->alias][$to_check_field];

        switch ($condition) {
            case "<":
                $validate = $other < $value;
                break;
            case ">":
                $validate = $other > $value;
                break;
            case ">=":
                $validate = $other >= $value;
                break;
            case "<=":
                $validate = $other <= $value;
                break;
            case "!=":
                $validate = $other != $value;
                break;
            default:
                $validate = $other == $value;
        }

        if ($validate) {
            if (empty($data[$field])) {
				return false;
			}
        }

        return true;
    }
    
    /**
     * Retorna arvore dos models que serão excluidos
     *
     * @return type
     */
    public function getDependents()
    {
        $deltree = array();
        foreach ($this->hasMany as $rel => $options) {
            if ($options["dependent"] === true) {
                $cls = $options["className"];
                $obj = ClassRegistry::init($cls);
                $deltree[$cls::$label] = $obj->getDependents();
            }
        }

        return $deltree;
    }

    /**
     * Utilizada no delete cascade para desatrelar modelos antes do delete
     * Cada model deve implementar o seu
     *
     * @param type $id
     * @return boolean
     */
    public static function detachModels($id)
    {
        // Exemplo
        // ClassRegistry::init("Foo")->updateAll(array("Foo.bar_id" => null), array("Foo.bar_id" => $id));
        return true;
    }
	
	/**
	 * Ajuda a mockar o metodo nos testes
	 *
	 * @return array
	 */
	public static function _getAuthUser($key = null)
	{
	    return AuthComponent::user($key);
	}
}