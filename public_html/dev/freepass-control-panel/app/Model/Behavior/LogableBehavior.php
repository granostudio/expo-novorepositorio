<?php
App::uses('Log', 'Model');
App::import('Component', 'Auth');

class SuperNaturalException extends CakeException
{
    
}

class LogableBehavior extends ModelBehavior
{
    public function setup(Model $Model, $settings = array())
    {
        if (!isset($this->settings[$Model->alias])) {
            $this->settings[$Model->alias] = array();
        }

        $this->settings[$Model->alias] = array_merge($this->settings[$Model->alias], $settings);
    }

    public function beforeSave(Model $Model, $options = array())
    {
        $Model->old_data = $this->_getOld($Model);

        return true;
    }

    public function afterSave(Model $Model, $created, $options = array())
    {
        if (isset($Model->disable_log)) {
            return;
        }

        if ($created) {
			$this->_log(Log::ACTION_ADD, $Model->plugin, $Model->name, $Model->id, null);
		} else {
			$this->_log(Log::ACTION_EDIT, $Model->plugin, $Model->name, $Model->id, $this->_getDiff($Model));
		}
		
        return true;
    }

    public function beforeDelete(Model $Model, $cascade = true)
    {
        $Model->old_data = $this->_getOld($Model);

        return true;
    }

    public function afterDelete(Model $Model)
    {
        $this->_log(Log::ACTION_DELETE, $Model->plugin, $Model->name, $Model->id, $Model->old_data[$Model->alias]);

        return true;
    }

    private function _log($action, $plugin,$model, $model_id, $data)
    {
        if (!AuthComponent::user()) {
            return;
        } 
        
        $who = AuthComponent::user("name") . " - " . AuthComponent::user("email");
        
        $_data = array(
            'action' => $action,
            'who' => $who,
            'plugin' => $plugin,
            'model' => $model,
            'model_id' => $model_id,
            'data' => json_encode($data),
            'user_id' => AuthComponent::user("id")
        );

        $log = ClassRegistry::init("Log");
        $log->create();
        $log->actsAs = array();
        $log->Behaviors->detach('Logable');
        
        if (!$log->save($_data, false)) {
            throw new SuperNaturalException("Ocorreu uma falha ao tentar gerar o log");
        }
    }

    public function _getOld(Model $Model)
    {
        $Model->Behaviors->detach("Convertable");
        $return = $Model->findById($Model->id);
        $Model->Behaviors->attach("Convertable");

        return $return;
    }

    private function _getDiff(Model $Model)
    {
        $fields = $Model->data[$Model->alias];

        if (empty($Model->old_data)) {
            return array();
        }

        $old_fields = $Model->old_data[$Model->alias];
        if (is_null($old_fields)) {
            return array();
        }

        // Remove campos que não estão no schema (Ex: cpassword, _size)
        $schema = $Model->schema();
        foreach ($fields as $key => $value) {
            if (!array_key_exists($key, $schema)) {
                unset($fields[$key]);
            }
        }
        
        $diff = array_diff_assoc($fields, $old_fields);

        return $diff;
    }
}