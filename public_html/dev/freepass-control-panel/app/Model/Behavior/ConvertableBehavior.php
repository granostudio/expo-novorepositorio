<?php
App::uses('ModelBehavior', 'Model');

class ConvertableBehavior extends ModelBehavior
{
    public function setup(Model $Model, $settings = array())
    {
        if (!isset($this->settings[$Model->alias])) {
            $this->settings[$Model->alias] = array(
                'afterFind' => false,
            );
        }

        $this->settings[$Model->alias] = array_merge($this->settings[$Model->alias], $settings);
    }

    public function beforeSave(Model $Model, $options = array())
    {
        foreach ($Model->schema() as $field => $data) {

            $except = isset($this->settings[$Model->alias]["except"]) ? $this->settings[$Model->alias]["except"] : array();
            if (in_array($field, $except)) {
				continue;
			}

            switch ($data["type"]) {
                case 'decimal':
                case 'float':
                    if ($data) {
						$this->_convertFloatToMysql($Model, $field);
					}
                    break;
                case 'date':
                    $this->_convertDateToMysql($Model, $field);
                    break;
                case 'datetime':
                    $this->_convertDateTimeToMysql($Model, $field);
                    break;
            }
        }

        return true;
    }

    public function afterFind(Model $Model, $results = array(), $primary = false)
    {
        foreach ($results as $key => $row) {

            foreach ($Model->schema() as $field => $data) {

                $except = isset($this->settings[$Model->alias]["except"]) ? $this->settings[$Model->alias]["except"] : array();
                if (in_array($field, $except)) {
					continue;
				}
				
                switch ($data["type"]) {
                    case 'decimal':
                    case 'float':
                        $this->_convertFloatToView($results[$key], $row, $Model->alias, $field, $data['length']);
                        break;
                    case 'date':
                        if (isset($row[$Model->alias][$field])) {
							$results[$key][$Model->alias][$field] = $this->_convertDateToView($row[$Model->alias][$field]);
						}
                        break;
                    case 'datetime':
                        if (isset($row[$Model->alias][$field])) {
							$results[$key][$Model->alias][$field] = $this->_convertDateTimeToView($row[$Model->alias][$field]);
						}
                        break;
                }
            }
        }

        return $results;
    }

    /**
     * Converters
     * float
     */
    private function _convertFloatToMysql(Model $Model, $field)
    {
        if (isset($Model->data[$Model->alias][$field])) {
			$Model->data[$Model->alias][$field] = str_replace(",", ".", str_replace(".", "", $Model->data[$Model->alias][$field]));
		}
    }

    private function _convertFloatToView(&$item, $data, $model, $field, $lenght)
    {
        $parts = explode(",", $lenght);

        if (isset($data[$model][$field])) {
			$item[$model][$field] = number_format($data[$model][$field], $parts[1], ",", ".");
		}
    }

	/**
	 * Date
	 */
    private function _convertDateToMysql(Model $Model, $field)
    {
        if (isset($Model->data[$Model->alias][$field])) {
            $parts = explode("/", $Model->data[$Model->alias][$field]);
            if (count($parts) == 3) {
				$Model->data[$Model->alias][$field] = $parts[2] . "-" . $parts[1] . "-" . $parts[0];
			}
        }
    }

    private function _convertDateToView($value)
    {
        return date('d/m/Y', strtotime($value));
    }

	/**
	 * Datetime
	 */
    private function _convertDateTimeToMysql(Model $Model, $field)
    {
        if (isset($Model->data[$Model->alias][$field])) {
            $parts = explode("/", $Model->data[$Model->alias][$field]);
            if (count($parts) == 3) {
                $year_hour = explode(' ', $parts[2]);

                $Model->data[$Model->alias][$field] = $year_hour[0] . "-" . $parts[1] . "-" . $parts[0];

                if (isset($year_hour[1])) {
					$Model->data[$Model->alias][$field] .= " " . $year_hour[1];
				}
            }
        }
    }

    private function _convertDateTimeToView($value)
    {
        return date('d/m/Y H:i:s', strtotime($value));
    }
}