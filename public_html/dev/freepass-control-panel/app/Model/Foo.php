<?php
App::uses('AppModel', 'Model');

class Foo extends AppModel
{
    const TYPE_1 = 1;
    const TYPE_2 = 2;
    const TYPE_3 = 3;

    public static $types = array(
        self::TYPE_1 => "Tipo 1",
        self::TYPE_2 => "Tipo 2",
        self::TYPE_3 => "Tipo 3"
    );

    public $belongsTo = array(
		'Bar' => array(
			'className' => 'Bar',
			'foreignKey' => 'bar_id'
		)
	);

    public $hasAndBelongsToMany = array(
		'Bar' => array(
			'className' => 'Bar',
			'foreignKey' => 'bar_id'
		)
	);

    public $actsAs = array(
        'Upload.Upload' => array(
            'upload' => array(
                'maxSize' => 2048000, # 2MB
                'rootDir' => ROOT,
                'path' => '{ROOT}{DS}uploads{DS}{model}{DS}{field}{DS}',
                'extensions' => array('jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG'),
                'fields' => array(
                    'dir' => 'id',
                    'type' => '_type',
                    'size' => '_size'
                ),
                'deleteOnUpdate' => true
            )
        ),
		'Sluggable.Sluggable' => array(
			'label' => 'name',
			'slug' => 'slug',
			'separator' => '-',
			'overwrite' => true,
			'translation' => 'utf-8'
		)
    );

    public $validate = array(
        'name' => array(
            'rule' => array('minLength', '3'),
            'message' => 'Este campo deve conter no minimo 3 caracteres'
        ),
        'bar_id' => array(
            'rule' => array("notEmpty"),
            'message' => 'Este campo é obrigatório'
        ),
        'other_bar_id' => array(
            'rule' => array("notEmpty"),
            'message' => 'Este campo é obrigatório'
        ),
        'upload' => array(
            'fileExtension' => array(
                'rule' => array('isValidExtension', array('jpeg', 'jpg', 'png', 'JPG', 'JPEG', 'PNG'), false),
                'message' => 'Extensão inválida!'
            ),
            'file' => array(
                'rule' => array('isBelowMaxSize', 2048000),
                'message' => 'Arquivo deve ser menor que 2MB'
            )
        )
    );

    public $virtualFields = array(
        'upload_path' => 'CONCAT("/uploads/foo/upload/", Foo.id, "/", Foo.upload)',
    );

    /**
     * Nome do modelo em português, usado nas views
     * @var string
     */
    public static $label = "Foo";

    /**
     * Nome dos campos em português , usados nas views (Forms,Grids)
     * @var array
     */
    public static $labels = array(
        "name" => "Nome do Foo",
        "type" => "Tipo",
        "upload" => 'Imagem',
        "phone" => "Telefone",
        "zipcode" => "CEP",
        'date' => 'Data',
        'datepicker' => 'Data',
        'money' => 'Moeda BRL',
        'cpf' => 'CPF',
        'cnpj' => 'CNPJ',
        'rate' => 'Taxa 0.0000',
        'integer' => 'Inteiro',
        'time' => 'Tempo hh:mm:ss',
        'bar_id' => 'Bar',
        'other_bar_id' => 'Outro Bar',
        'obs' => 'Observações',
        'html' => 'Html Editor'
    );

     public static $tips = array(
        "obs" => "maxlength=255 <br> Fica hide quando o tipo estiver vazio(veja: App.initHider)",
        "phone" => ".format-phone",
        "zipcode" => ".format-zipcode",
        'date' => '.format-date',
        'datepicker' => '.format-datepicker',
        'money' => '.format-money',
        'cpf' => '.format-cpf',
        'cnpj' => '.format-cnpj',
        'rate' => '.format-rate',
        'integer' => '.format-integer',
        'time' => '.format-time',
    );
}
