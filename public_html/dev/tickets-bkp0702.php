<div class="tickets-container wrapper tickets" id="ingressos">
	<div class="half-wrapper">
		<div class="row">
			<div class="col col-12">
				<h2 class="title"> Ingressos </h2>
			</div> <!-- .col .col-12 -->
		</div> <!-- .row -->

		<div class="row">
			<div class="col col-5 border-l">
				<p>
					<span class="bold subtitle">Inteira: </span> <span class="subtitle"> R$ 20,00 </span> <br/>
					<span class="bold subtitle">Meia: </span> <span class="subtitle"> R$ 10,00 </span> <br/>
					<small> (mediante apresentação de carteirinha) </small>
				</p>

				<p>
					Professores de Ensino Fundamental e Médio, de escolas públicas e particulares, têm entrada franca em todos os dias e horários, desde que comprovem atividade em 2016;
				</p>

				<p>
					<b> Quem tem direito a meia entrada: </b> <br/>
					<!-- Aposentados,  Idosos,  Estudantes, Deficientes físicos (neste caso, os acompanhantes também tem direito a meia entrada); -->
					<ul class="info-list">
						<li> Estudantes, Jovens de 15 a 29 anos pertencentes a famílias de baixa renda e Deficientes com acompanhante; </li>					
					
						<li> Diretor, Coordenador pedagógico, Supervisor e Titular do Quadro de Apoio de Escola Estadual e Municipal (Apresentar documento de identidade oficial com foto e carteira funcional da Secretaria de Educação ou Holerite que comprove a condição). (Lei Estadual SP 15.298/14) </li>
					
						<li> Aposentados (Apresentar documento de identidade oficial com foto e cartão de benefício do INSS que comprove a condição. (Lei Municipal SP nº 12.325/1997) </li>
					</ul>
				</p>

				<p>
					<b> Quem tem direito a gratuidade: </b> <br/>
					
					<ul class="info-list">
						<li> Crianças até 02 anos de idade; </li>
						
						<li> Professores de escolas da rede pública Estadual/Municipal e Escolas Particular); </li>

						<li> (Apresentar documento de identidade oficial com foto e carteira funcional da Secretaria de Educação ou Holerite que comprove a condição). </li>
					</ul>
				</p>
			</div> <!-- .col .col-5 .text-center -->

			<div class="col col-7 border-l">
				<p class="precos">
					<span class="bold subtitle"> Horário de Funcionamento/Visitação: </span> <br/>
					Aberto de terça a domingo, das 11h00 às 20h00
					Bilheteria até 19h00
				</p>

				<p class="precos">	
					Ingressos à venda no local a partir <span class="quebra"></span>
					do dia <u><b>7 de Novembro</b></u> ou
				</p>

				<div class="atencao">
					<br/><span class="bold subtitle" style="font-size: 28px; font-family: 'StagBold'"> ATENÇÃO </span> <br/>

					<p> Para Pontos de Venda e Bilheterias é necessária a comprovação do direito ao benefício da 1/2 entrada no ato da compra e no acesso ao evento. </p>
					<p> Para vendas pela Internet e Telefone é necessária a comprovação do direito ao benefício da 1/2 entrada no acesso ao evento. </p>
					<p> Caso o benefício não seja comprovado, o portador deverá complementar o valor do ingresso adquirido para o valor do ingresso integral, caso contrário o acesso ao evento não será permitido. </p>
				</div>
				
				<a href="http://expo-theartofthebrick.com.br/comprar-ingressos/">
					<img src="assets/images/bt-comprar-carimbado.png" alt="Comprar Ingresso" class="comprar" />
				</a>

				<div class="mapa">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.202426166454!2d-43.171701685079746!3d-22.90590334359806!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x997e50222e9573%3A0x54c54f452c889eb8!2sMuseu+Hist%C3%B3rico+Nacional!5e0!3m2!1spt-BR!2sbr!4v1478513501576" width="523" height="225" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>

				<p>
					<span class="bold subtitle"> MHN - Museu Histórico Nacional </span> <br/>
					<span class="endereco"> 
						Praça Marechal Âncora, s/nº
					</span>
				</p>

			</div> <!-- .col .col-7 -->
		</div> <!-- .row -->

	</div> <!-- .half-wrapper -->
</div> <!-- .tickets-container .wrapper -->
