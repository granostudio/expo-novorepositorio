<div id="banner">
	<img src="assets/images/banner.jpg" alt="" />
	<div class="bt-compre-ingresso">
		<a href="http://expo-theartofthebrick.com.br/comprar-ingressos/" class="btn btn-ingressos">
			<img src="assets/images/bt-comprar.png" alt="Comprar Ingresso" class="comprar" />
		</a>
	</div> <!-- .btn-compre-ingressos -->
</div> <!-- #banner -->

<div id="banner-mobile">
	<img src="assets/images/banner-mobile.png" alt="" />
	<div class="bt-compre-ingresso">
		<a href="http://expo-theartofthebrick.com.br/comprar-ingressos/" class="btn btn-ingressos">
			<img src="assets/images/bt-comprar.png" alt="Comprar Ingresso" class="comprar" />
		</a>
	</div> 
</div>