<div class="row">
	<div class="col col-12">
		<h2 id="nathan-sawaya" class="text-center"> Nathan Sawaya </h2>
	</div> <!-- .col-12 -->
</div> <!-- .row -->

<div class="row">
	<div class="col col-6">
		<h3>Quem é Nathan Sawaya?</h3>

		<p>Nathan Sawaya nasceu na cidade de Colville, no estado de Washington, e cresceu em Veneta, Oregon. Era uma criança alegre e criativa que amava desenhar, escrever histórias, fazer truques de mágica e, obviamente, construir seu próprio mundo usando peças LEGO®. A formação em Direito, que completou quando jovem adulto, o afastou de uma vida pautada pela criatividade e pela imaginação, levando-o a iniciar sua carreira como advogado em Nova York. Mas, tal como um leopardo não pode mudar suas manchas, após alguns anos de competição feroz Nathan Sawaya decidiu abandonar tudo e resgatar suas paixões de infância: brincar, usar toda a sua imaginação e criatividade! De repente, seu “artista interior”, tanto tempo reprimido, disse adeus às negociações de contratos e mergulhou no universo das construções com LEGO®, recuperando uma de suas primeiras paixões.</p>
	</div> <!-- .col-6 -->

	<div class="col col-6">
		<img src="assets/images/img_03.jpg" alt="" />
	</div> <!-- .col-6 -->
</div> <!-- .row -->

<div class="row">
	<div class="col col-6">
		<img src="assets/images/img_04.jpg" alt="" />
	</div> <!-- .col-6 -->

	<div class="col col-6">
		<h3>Como ele teve esta ideia?</h3>

		<p>Nathan Sawaya não escolheu esse meio por acaso. Sua história com LEGO® começou a ser traçada quando era apenas um menino, como acontece com muitos de nós. No entanto, ao contrário da maioria das pessoas, ele nunca deixou de lado os famosos blocos de montar! Ganhou seu primeiro <i>kit</i> peças LEGO® de presente de Natal quando tinha apenas cinco anos. Nessa época, ele já demonstrava ser uma criança muito criativa, e seus pais e avós estimularam-no a adotar como <i>hobby</i> a prática de construir usando as pequenas peças. Assim, Nathan passou a usar os blocos coloridos para criar uma série de casas, carros e animais, cada um mais original que o outro. Alguns anos se passaram e Nathan continuou a cultivar o prazer de brincar com as pequenas peças. Foi então que se propôs a construir uma cidade inteira, que abrangia uma área de mais de 10m²! Quando fez dez anos, e seus pais não lhe permitiram ter um animal de estimação, decidiu que teria um de qualquer jeito, à sua maneira: construiu um cachorro em tamanho real usando as peças LEGO®. Foi então que se deu conta de que os pequenos blocos poderiam ser usados para montar qualquer coisa que desejasse, e que não precisava limitar-se aos modelos apresentados nas caixas do brinquedo. Depois de trocar a mesa de trabalho do escritório de advocacia pelo chão de seu estúdio, no início dos anos 2000, Nathan Sawaya libertou sua criança interior e sua natureza artística, que, no seu caso, estavam intrinsecamente ligados. Essa atitude abriu caminho para um intenso processo criativo, pautado por experiências e práticas lúdicas que, para muitos, representam uma revolução no mundo da arte.</p>
	</div> <!-- .col-6 -->
</div> <!-- .row -->

<div class="divisao"></div> <!-- .divisao -->